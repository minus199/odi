package com.odi;

import com.odi.model.event.Event;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightDeparturedEvent;
import com.odi.model.event.FlightLndedEvent;
import com.odi.model.product.MissedConnectionProductTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MissedConnectionProductTemplateTest {

	private final Logger logger = LoggerFactory.getLogger("test");
	
	@Test
	public void contextLoads() {
	}


	@Test
	public void newTest() {
		MissedConnectionProductTemplate template = new MissedConnectionProductTemplate();
		
		assertNotNull(template);		
	}
	
	@Test
	public void isRelevantTest() {
		MissedConnectionProductTemplate template = new MissedConnectionProductTemplate();

		assertTrue(template.isIn(new FlightCanceledEvent()));
		assertTrue(template.isIn(new FlightLndedEvent()));
		assertTrue(template.isIn(new FlightDeparturedEvent()));
		assertTrue(template.isIn(new Event()) == false);
	}
	@Test
	public void isInTest() {
		MissedConnectionProductTemplate template = new MissedConnectionProductTemplate();
		FlightCanceledEvent cancel = new FlightCanceledEvent();
		FlightLndedEvent landed = new FlightLndedEvent();
		FlightDeparturedEvent depature = new FlightDeparturedEvent();
		template.setDepartureAirport("BG,LGW,CDG");
		template.setConnectionAirport("BG,LGW,CDG");
		template.setDestinationAirport("BG,LGW,CDG");

		assertTrue(template.isIn(new FlightCanceledEvent()));
		assertTrue(template.isIn(new FlightLndedEvent()));
		assertTrue(template.isIn(new FlightDeparturedEvent()));
		assertTrue(template.isIn(new Event()) == false);

		cancel.setDepartureAirportId("BG");
		assertTrue(template.isIn(cancel));
		cancel.setDepartureAirportId("LGW");
		assertTrue(template.isIn(cancel));
		cancel.setDepartureAirportId("CDG");
		assertTrue(template.isIn(cancel));
		cancel.setDepartureAirportId("NONE");
		assertTrue(template.isIn(cancel) == false);

		landed.setDepartureAirportId("BG");
		assertTrue(template.isIn(landed));
		landed.setDepartureAirportId("LGW");
		assertTrue(template.isIn(landed));
		landed.setDepartureAirportId("CDG");
		assertTrue(template.isIn(landed));
		landed.setDepartureAirportId("NONE");
		assertTrue(template.isIn(landed) == false);

		depature.setDepartureAirportId("BG");
		assertTrue(template.isIn(depature));
		depature.setDepartureAirportId("LGW");
		assertTrue(template.isIn(depature));
		depature.setDepartureAirportId("CDG");
		assertTrue(template.isIn(depature));
		depature.setDepartureAirportId("NONE");
		assertTrue(template.isIn(depature) == false);

	}

	@Test
	public void WaitTest() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
