package com.odi;

import com.odi.model.Flight;
import com.odi.model.KV;
import com.vaadin.data.Converter;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import org.junit.Test;
import org.springframework.util.StringUtils;

import javax.util.streamex.StreamEx;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.DayOfWeek;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by minus on 4/15/17.
 */
public class JunkTest {

	private void dynamic() {
		/*List<Method> methods = */
		Map<String, Method> methodsByName = StreamEx.of(Flight.class.getMethods()).toMap(Method::getName, m -> m);

		Grid<Flight> grid = new Grid<>();
		grid.getColumns().stream()
				.map(col -> {
					String colID = StringUtils.capitalize(col.getId());
					return KV.of(col, KV.of(methodsByName.get("get" + colID), methodsByName.get("set" + colID)));
				})
				.forEach(colGetSet -> {
					TextField currentEditingField = new TextField();

					Setter<Flight, String> setter = (flight, fieldvalue) -> {

					};

					ValueProvider<Flight, Object> provider = (ValueProvider<Flight, Object>) flight -> {
						try {
							return colGetSet.getV().getK().invoke(flight);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}

						return null;
					};
				});
	}

	private Optional<Class<? extends AbstractField>> matcher(Class<?> type) {

		if (Boolean.class.equals(type) || boolean.class.equals(type)) {
			return Optional.of(CheckBox.class);
		}

		Optional<? extends Class<? extends Number>> primNumType = Stream
				.of(short.class, int.class, long.class, float.class, double.class)
				.filter(c -> c.equals(type))
				.findFirst();

		if (primNumType.isPresent() || String.class.equals(type) || type.isAssignableFrom(Number.class)) {
			return Optional.of(TextField.class); // TextArea.class, TextField.class, RichTextArea.class
		}

		if (type.isAssignableFrom(DayOfWeek.class)) {
//			return;
		}

		if (type.isAssignableFrom(Date.class)) {

			return Optional.of(DateField.class);
		}

		return Optional.empty();
	}

	/*Slider (com.vaadin.ui)
	CustomField (com.vaadin.ui)

	ColorPickerSelect (com.vaadin.ui.components.colorpicker)
	ColorPickerHistory (com.vaadin.ui.components.colorpicker)
	ColorPickerGrid (com.vaadin.ui.components.colorpicker)
	AbstractColorPicker (com.vaadin.ui)
	ColorPicker (com.vaadin.ui)
	ColorPickerArea (com.vaadin.ui)
	ColorPickerGradient (com.vaadin.ui.components.colorpicker)*/

	@Test
	public void t() {

//		DateField.class,


		/*BindingInformation<DateField, LocalDateToDateConverter, ValueProvider<Flight, Date>, Setter<Flight, Date>> bindingInformation
				=*/


//		new BindingInformation<>(DateField.class, new LocalDateToDateConverter(ZoneId.systemDefault()), Flight::getFlightTime, Flight::setFlightTime);
		System.out.println();
	}

	private static class BindingInformation
			<HV extends HasValue<?>, CV extends Converter<?, ?>, VP extends ValueProvider<?, ?>, S extends Setter<?, ?>> {

		private final HV hasValue;
		private final CV converter;
		private final VP valueProvider;
		private final S setter;

		BindingInformation(Class<HV> hasValue, CV converter, VP valueProvider, S setter) {
			try {
				this.hasValue = hasValue.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new UnsupportedOperationException(hasValue.getName() + " cannot be instantiated.");
			}

			this.converter = converter;
			this.valueProvider = valueProvider;
			this.setter = setter;
		}
	}
/*
					grid.getEditor().getBinder().bind(currentEditingField, provider, setter);

					(String flight) -> {
						try {
							return colGetSet.getV().getK().invoke(flight);
						} catch (IllegalAccessException | InvocationTargetException e) {
							return null;
						}
					});






					,(flight, s) -> {
						grid.getEditor().getBinder().bind(currentEditingField, flight -> flight.getDestinationAirportId(), (flight, s) -> {
							try {
								colGetSet.getV().getV().invoke(flight, s);
							} catch (IllegalAccessException | InvocationTargetException e) {
								return null;
							}
						});

						grid.getEditor().getBinder().bind(currentEditingField, flight -> flight.getDestinationAirportId(), (flight, s) -> {
							flight.setDestinationAirportId(s);
						});
					};

					colGetSet.getK(),
							new ValueProvider<Flight, Object>() {
								@Override
								public FIELDVALUE apply(Flight flight) {
									return null;
								}
							}

							;
				} new ValueProvider<Flight, Object>() {
			@Override
			public Object apply(Flight flight) {
				try {
					return colGetSet.getV().getK().invoke(flight);
				} catch (IllegalAccessException | InvocationTargetException e) {
					return null;
				}
			}
		}, setter));*/

//		grid.getColumns().forEach(col -> col.setEditorComponent());



		/*grid.getColumns().get(1).setEditorComponent(destAirport, (Setter<Flight, ?>) (flight, o) -> {

		});


		(flight, o) -> flight.setDestinationAirportId((String) o));
*/
/*		grid.addSelectionListener(new SelectionListener<Flight>() {
			@Override
			public void selectionChange(SelectionEvent<Flight> event) {
				System.out.println();
			}
		});*/


		/*filterField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
			@Override
			public void valueChange(HasValue.ValueChangeEvent<String> event) {
				binder.setBean(event.g);
				binder.
			}
		});*/
//


/*

*/











				/*.findFirst().orElse(null))).filter(Objects::nonNull)
				.forEach(colMethod -> binder.bind(colMethod.getK(), Flight::getOriginAirportId, Flight::setAirlineId))
				.collect(Collectors.toList());*/

//

//		grid.getColumns().get(4).setEditable(true).setEditorBinding();


/*

		Column<Todo, String> column = grid.addColumn(
				todo -> String.valueOf(todo.isDone()));
		column.setWidth(75);
		column.setEditorBinding(doneBinding);

		grid.addColumn(Todo::getTask).setEditorComponent(
				taskField, Todo::setTask).setExpandRatio(1);
		grid.getEditor().setEnabled(true);
*/


	/*	TextField filteringField = filterField();
		filteringField.addValueChangeListener(event -> {
			dataProvider.withConfigurableFilter().setFilter(new Predicate<Flight>() {
				@Override
				public boolean test(Flight flight) {
					return false;
				}
			});

		});*/

//
//
//
//providerWrapper.setFilter();
//			dataProvider.sewithConfigurableFilter().setFilter();addDataProviderListener(new DataProviderListener<Flight>() {
//				@Override
//				public void onDataChange(DataChangeEvent<Flight> event) {
//					System.out.println();
//				}
//			});
//			/*dataProvider.setFilter(CompanyBudgetHistory::getCompany, company -> {
//				if (company == null) {
//					return false;
//				}
//				String companyLower = company.toLowerCase(Locale.ENGLISH);
//				String filterLower = event.getValue().toLowerCase(Locale.ENGLISH);
//				return companyLower.contains(filterLower);
//			});*/
//		});

	//
	//		grid.addContextClickListener(new ContextClickEvent.ContextClickListener() {
	//			@Override
	//			public void contextClick(ContextClickEvent event) {
	//				System.out.println("Right mouse button clicked.");
	//				/*addComponent(
	//						new ListSelect<>(
	//								"Actions",
	//								new ListDataProvider<String>(Arrays.asList("Delete", "Update"))));*/
	//			}
	//		});
}
