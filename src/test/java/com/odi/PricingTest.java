package com.odi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.odi.model.Flight;
import com.odi.service.IConnectedFlightService;
import com.odi.service.IPricingService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PricingTest {
	private final Logger logger = LoggerFactory.getLogger("test");
	
	@Autowired
	IPricingService pricingService;
	
	@Autowired
	IConnectedFlightService connectedFlightService;
	
	@Test
	public void contextLoads() {
	}


	@Test
	public void testSend() {
		try {
			String USER_AGENT = "Mozilla/5.0";
			String url = "http://www.google.com/search?q=mkyong";
			url = "http://localhost:5050/flight_price?dep_apt=TLV&arr_apt=CDG&airline=LY&flight_number=319&amount=400&threshold=60.0&date=2014-07-07";
					//URLEncoder.encode("date=2014-07-07 02:40");
	
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			// optional default is GET
			con.setRequestMethod("GET");
	
			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
	
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
			//print result
			System.out.println(response.toString());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	@Test
	public void testFlightDelay() {
		Flight flight = new Flight("06:20", "13:15", "LY", 319, "TLV", "CDG",
				  DayOfWeek.MONDAY, 400);
		
		pricingService.getFlightDelayPrice(flight, new Date(), 400, 60);
		
	}
}
