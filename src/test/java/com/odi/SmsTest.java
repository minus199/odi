package com.odi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.odi.service.utils.PlivoSms;
import com.odi.service.utils.TwilioSms;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsTest {
	private final Logger logger = LoggerFactory.getLogger("test");

	@Test
	public void plivoTest() {
		//logger.info("Plivo test");
		PlivoSms sms = new PlivoSms();
		
		//sms.Send("+972525766810", "Message from Plivo");
	}

	@Test
	public void twilioTest() {
		//logger.info("Plivo test");
		TwilioSms sms = new TwilioSms();
		
		sms.Send("+972525766810", "Message from "
				+ "twilio");
	}

}
