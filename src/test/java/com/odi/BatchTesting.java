package com.odi;

import com.google.common.primitives.Primitives;
import com.odi.model.*;
import com.odi.service.ICompanyService;
import com.odi.service.IConnectedFlightService;
import com.odi.service.batch.readers.*;
import com.odi.service.batch.writeres.IBaseWriter;
import com.odi.service.batch.writeres.IWriterFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 3/30/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {OdiApplication.class})
@ActiveProfiles("hsql")
public class BatchTesting {
	private final static List<KV<Class<? extends EntityBase>, ? extends Class<? extends IBaseReader>>> testedTypes = new ArrayList<>();
	private static final KV[] expectedKVs = {
			KV.of("Company", "address, telNum, faxNum"),
			KV.of("AirLineCompany", "code, countryId, cityId"),
			KV.of("Flight", "flightTime, flightDuration, flightLanding, airlineId, flightNumber, originAirportId, destinationAirportId, dayOfWeek, tempFlightCost, flightDuharionHours, flightDuharionMinutes, instances"),
			KV.of("Airport", "cityId, code")};

	static {
		testedTypes.add(KV.of(Company.class, CompanyReader.class));
		testedTypes.add(KV.of(AirLineCompany.class, AirLineCompanyReader.class));
		testedTypes.add(KV.of(Airport.class, AirportReader.class));
		testedTypes.add(KV.of(Flight.class, FlightReader.class));
		testedTypes.add(KV.of(Flight.class, CityReader.class));
	}

	@Value("${spring.datasource.url}")
	String url;
	//	@Autowired
	private BatchTestingUtils batchTestingUtils;
	@Autowired
	private IReaderFactory readerFactory;
	@Autowired
	private IWriterFactory writerFactory;

	@Autowired
	private JobLauncher jobLauncher;
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	@Autowired
	private ICompanyService companyService;
	@Autowired
	private IConnectedFlightService flightService;

	@Test
	@WithMockUser(username = "customUsername", roles = {"ADMIN"})
	public void t() {

//		flightService.getFlights("asd", "asd", "asd");

		String s = batchTestingUtils.generateCSV(1000, 3);
		try {
			Path file = Files.createFile(Paths.get("/tmp/companies.csv"));
			Path write = Files.write(file, s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void serviceTest() {
		List<? extends List<?>> collect = batchTestingUtils.streamOfPojoTypes(testedTypes).map(o -> {
			Constructor<?> constructor = Stream.of(o.getK().getConstructors())
					.max((o1, o2) -> {
						if (o1.getParameterCount() == o2.getParameterCount()) return 0;
						return o1.getParameterCount() > o2.getParameterCount() ? 1 : - 1;
					})
					.orElseThrow(() -> new UnsupportedOperationException("No constructor was found for " + o.getK().getSimpleName()));

			List<? extends Class<?>> collect1 = Stream.of(constructor.getParameters()).map(Parameter::getType).collect(Collectors.toList());
			return Stream.of(constructor.getParameters())
					.map(parameter -> parameter.getType().isPrimitive() ? Primitives.wrap(parameter.getType()) : parameter.getType())
					.map(aClass -> aClass.cast("Floo " + Math.floor(Math.random() * 1000)))
					.collect(Collectors.toList());
		}).collect(Collectors.toList());

		Company company = new Company("foo" + Math.random(), "bar" + Math.random(), "fuzz" + Math.random(), " buzz" + Math.random());
		Company company1 = new Company("foo1" + Math.random(), "bar1" + Math.random(), "fuzz1" + Math.random(), " buzz1" + Math.random());
		Company company2 = new Company("foo2" + Math.random(), "bar2" + Math.random(), "fuzz2" + Math.random(), " buzz2" + Math.random());

		List<Company> companies = Arrays.asList(company, company1, company2);
		List<Company> save = companyService.save(companies);
		System.out.println();
	}

	@Test
	public void testPojoParamExtract() {
		Map<String, String> collect = batchTestingUtils.streamOfPojoTypes(testedTypes).collect(Collectors.toMap(
				kv -> kv.getK().getSimpleName(),
				kv -> String.join(", ", kv.getV())));

		for (int i = 0; i < expectedKVs.length; i++) {
			KV currentKV = expectedKVs[i];
			String currentPojoName = collect.keySet().toArray()[i].toString();

			Assert.assertEquals("Pojo name mismatch", currentKV.getK(), currentPojoName);
			Assert.assertEquals("Token names mismatch", currentKV.getV(), collect.get(currentPojoName));
		}
	}

	@Test
	public void testJobDispatch() {
		ByteArrayResource csvBytesResource = new ByteArrayResource(batchTestingUtils.generateCSV(3000, 3).getBytes());
		Class<? extends EntityBase> targetClass = Company.class;
		IBaseWriter writer = writerFactory.get(targetClass);
		ItemStreamReader reader = readerFactory.get(targetClass, csvBytesResource);

		TaskletStep s1 = stepBuilderFactory.get("s1")
				.chunk(100)
				.reader(reader)
				.writer(writer)
				.build();

		Job somejob = jobBuilderFactory.get("somejob")
				.incrementer(new RunIdIncrementer())
				.start(s1)
				.listener(new JobExecutionListener() {
					@Override
					public void beforeJob(JobExecution jobExecution) {
						System.out.println("Doing job " + jobExecution.getJobId());
					}

					@Override
					public void afterJob(JobExecution jobExecution) {
						System.out.println("Done job " + jobExecution.getJobId());
					}
				})
				.build();

		try {
			JobExecution run = jobLauncher.run(somejob, new JobParameters());
			System.out.println();
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			e.printStackTrace();
		}
	}

}
