package com.odi;

import com.odi.model.*;
import com.odi.model.user.User;
import com.odi.model.user.UserRole;
import com.odi.service.*;
import com.odi.service.batch.IBatchProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by minus on 4/6/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {OdiApplication.class})
@ActiveProfiles("mysql")
public class DataTesting {
	@Autowired
	Set<? extends IServiceBase> serviceBaseMap;

	@Autowired
	ICityService cityService;

	@Autowired
	ICountryService countryService;

	@Autowired
	ICompanyService companyService;

	@Autowired
	IBatchProcessor batchProcessor;

	@Autowired
	IUserService userService;

	@Test
	public void country() {

		Stream.of(
				KV.of(AirLineCompany.class, "AirLines"),
				KV.of(Airport.class, "AirPorts"),
				KV.of(City.class, "Cities"),
				KV.of(Country.class, "Countries"),
				KV.of(Flight.class, "Flights")).forEach(kv -> process(kv));
	}

	private boolean process(KV<? extends Class<? extends EntityBase>, String> kv) {
/*		userService.save(Arrays.asList(
				new User("admin@adminest.boss", "admin", UserRole.ADMIN),
				new User("subAdmin@adminest.boss", "user", UserRole.SUB_ADMIN),
				new User("info@acme.com", "acme", UserRole.COMPANY)));*/



		ClassPathResource classPathResource = new ClassPathResource("migrations/import-data/" + kv.getV() + ".csv");
		/*java.util.Scanner s = null;
		try {
			s = new java.util.Scanner(classPathResource.getInputStream()).useDelimiter("\\A");
			String s1 = s.hasNext() ? s.next() : "";
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}*/

		try {
			return batchProcessor.process(kv.getK(), classPathResource);
		} catch (JobExecutionException e) {
			e.printStackTrace();
			return false;
		}
	}
}
