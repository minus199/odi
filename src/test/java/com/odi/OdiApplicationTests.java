package com.odi;

import com.odi.model.Flight;
import com.odi.model.FlightInstance;
import com.odi.model.event.Event;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightLndedEvent;
import com.odi.model.offer.Proposal;
import com.odi.model.product.FCProductTemplate;
import com.odi.model.product.FlightDelayProductTemplate;
import com.odi.model.product.Product;
import com.odi.model.product.Template;
import com.odi.repository.IEventRepository;
import com.odi.repository.IProductRepository;
import com.odi.repository.ITemplateRepository;
import com.odi.service.IEventService;
import com.odi.service.IFlightProposalService;
import com.odi.service.IFlightService;
import com.odi.service.utils.Mail;
import com.odi.service.utils.Sms;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OdiApplicationTests {

	@Autowired
	private IFlightService flightService;
	
	@Autowired
	private IFlightProposalService proposalService;
	
	@Autowired
	IEventService eventService;
	
	@Autowired
	IEventRepository eventRepository;
	
	@Autowired
	ITemplateRepository templateRepository;

	@Autowired
	IProductRepository productRepository;


	private final Logger logger = LoggerFactory.getLogger("test");
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void testFlights() {
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Flight flight = flightService.saveFlight("03:30", "LY", 232, "BG", "CDG", DayOfWeek.MONDAY, 333);
		flight.getInstances().add(new FlightInstance(DayOfWeek.SUNDAY, "01:00", "06:20", 360.0));
		flight.getInstances().add(new FlightInstance(DayOfWeek.MONDAY, "00:30", "05:20", 370.0));
		flight.getInstances().add(new FlightInstance(DayOfWeek.TUESDAY, "01:00", "06:50", 380.0));
		
		flightService.save(flight);
		
		Flight flight2 = flightService.get(flight.getId());
		for (FlightInstance f: flight2.getInstances())
			System.out.println(f);
		
		return;
	}
	
	@Test
	public void testMailSend() {
		Mail mail = new Mail();
		
		//mail.send("eyal@gluska.com", "Subject", "Message");
		
		return;
	}
	
	
	@Test
	public void testSMSSend() {
		Sms sms = new Sms();
		
		//sms.Send("972525766810", "Hello EG");
		
		return;
	}
	

	@Test
	public void testProductTemplate() {
		Template t1 = new FlightDelayProductTemplate();
		Template t2 = new FCProductTemplate();
		//((FlightDelayProductTemplate)template).setDelaytime("04:30");
		templateRepository.save(t2);
		templateRepository.save(t1);
		Template template2 = templateRepository.getOne(t1.getId());
		
		logger.info("Return type: " + template2.getClass().getName());
		return;
	}
	
	@Test
	public void testProducte() {
		Template template = new FlightDelayProductTemplate();
		Product product = new Product();
		
		product.setCompanyId("ElAl");
		product.setTemplate(template);
		
		productRepository.save(product);
		
		Product product2 = productRepository.findOne(product.getId());
		
		return;
	}
	
	
	@Test
	public void saveEventServiceTest() {
		Event event = null;
		
		event = new FlightCanceledEvent("TWA", 1, new Date());
		eventService.save(event);
		eventService.save(new FlightCanceledEvent("TWA", 2, new Date()));
		
		event = new FlightLndedEvent("LY", 3, new Date(), new Date(), new Date(), "bg", "cdg");
		eventService.save(event);
		eventService.save(new FlightLndedEvent("LY", 3, new Date(), new Date(), new Date(), "bg", "cdg"));
		
		List<Event> list = eventService.getAll();
		System.out.println(String.format("--- Events::: %d", list.size()));
		
		event = eventService.get(list.get(1).getId());
		list.add(event);
		for (Event ev: list)
			System.out.println(String.format("--- Type ::: %s", ev.getClass().getName()));
	}
	
	@Test
	public void saveEventRepositoryTest() {
		Event event = null;
		
		event = new FlightCanceledEvent("TWA", 1, new Date());
		eventRepository.save(event);
		eventRepository.save(new FlightCanceledEvent("TWA", 2, new Date()));
		
		event = new FlightLndedEvent("LY", 3, new Date(), new Date(), new Date(), "bg", "cdg");
		eventRepository.save(event);
		eventRepository.save(new FlightLndedEvent("LY", 3, new Date(), new Date(), new Date(), "bg", "cdg"));
		
		List<Event> list = eventRepository.findAll();
		System.out.println();
		
		event = eventRepository.findOne(list.get(1).getId());
		list.add(event);
		for (Event ev: list)
			System.out.println(String.format("--- Type ::: %s", ev.getClass().getName()));
	}
	
	
	@Test
	public void testProposalE2E() {
        DateTimeFormatter dfDate = DateTimeFormat.forPattern("dd/MM/YYYY");
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("dd/MM/YYYY kk:mm");
		Date departureTime = null;
		Date arrivalTime = null;		
		String airline = "TWA";
		int flightNumber = 651;
		double compensation = 120.0;
		String flightTime = "16:00";
		String flightDate1 = "12/1/2017";
		String flightDate2 = "18/1/2017";
		String triggerTime1 = "12/1/2017 18:00";
		String triggerTime2 = "18/1/2017 18:00";
		
		
		/*
		 * Prepare Flight
		 */
		Flight flight = flightService.saveFlight(flightTime, airline, flightNumber, "2", "3", DayOfWeek.MONDAY, 333);
		
		
		/*
		 * Prepare Proposals
		 */
		DateTime date = dfDate.parseDateTime(flightDate1);		
		Proposal proposal = proposalService.getFlightDelayProposal(
				flight.getId(),
				"ODI",
				4,
				0,
				date.toDate(),
				compensation,
				null);		
		proposalService.acceptFlightProposal(proposal.getId().toString());
		
		date = dfDate.parseDateTime(flightDate2);		
		proposal = proposalService.getFlightDelayProposal(
				flight.getId(),
				"ODI",
				4,
				0,
				date.toDate(),
				compensation,
				null);		
		proposalService.acceptFlightProposal(proposal.getId().toString());
		
		
		/*
		 * Prepare event to trigger the proposal
		 */
		eventService.flightLanded(
				flight.getId(),
				dfDate.parseDateTime(flightDate2).toDate(),
				dateTimeFormat.parseDateTime(triggerTime2).minusHours(6).toDate(),
				dateTimeFormat.parseDateTime(triggerTime2).toDate()
				);


	}

}
