package com.odi;

import com.odi.model.EntityBase;
import com.odi.model.KV;
import com.odi.service.batch.readers.IBaseReader;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 3/30/17.
 */
@Service
@Scope("prototype")
public class BatchTestingUtils {
	public <T> T generatePojos(Constructor<T> pojoConstructor, Object... args) {
		try {
			return pojoConstructor.newInstance(args);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException("Unable to create pojo");
		}
	}

	private Map<? extends Class<?>, Stream<? extends Class<?>>> extractConstructorArgs(List<KV<Class<? extends EntityBase>, ? extends Class<? extends IBaseReader>>> testedTypes) {
		return streamOfPojoTypes(testedTypes).collect(Collectors.toMap(
				KV::getK,
				o -> {
					/* For each pojo type, extract most suitable Constructor args */
					Constructor<?> constructor = Stream.of(o.getK().getConstructors())
							.max((o1, o2) -> {
								if (o1.getParameterCount() == o2.getParameterCount()) return 0;
								return o1.getParameterCount() > o2.getParameterCount() ? 1 : - 1;
							})
							.orElseThrow(() -> new UnsupportedOperationException("No constructor was found for " + o.getK().getSimpleName()));

					/* Map List<Parameter> list to List<Class> */
					return Stream.of(constructor.getParameters()).map(Parameter::getType);
				}));
	}

	Stream<? extends KV<? extends Class<?>, String[]>> streamOfPojoTypes(List<KV<Class<? extends EntityBase>, ? extends Class<? extends IBaseReader>>> testedTypes) {
		return testedTypes.stream().map(kv -> {
			try {
				return KV.of(kv.getK(), kv.getV().getConstructor(kv.getK()).newInstance(kv.getK().newInstance()).getTokenNames());
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
				e.printStackTrace();
			}

			return null;
		});
	}

	String generateCSV(int numRows, int numColumns) {

		DataFactory df = new DataFactory();
//		df.randomize((int)(Math.random() * 1000);

		char delimiter = '|';
		char newLine = '\n';

		StringBuilder stringBuffer = new StringBuilder();
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				stringBuffer.append(df.getRandomChars(5)).append(j == numColumns - 1 ? "" : delimiter);
			}

			stringBuffer.append(newLine);
		}

		return stringBuffer.toString();
	}

	private final class MiniGenerator {
		void t() {
			/*Stream<? extends Class<?>> stream = possibleArgumentTypes();
			new Callable<String>();
			new Callable<Integer>();
			new Callable<DayOfWeek>();
			new Callable<Boolean>();
			new Callable<Map>();*/
		}
	}
}
