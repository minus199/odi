package com.odi.repository;

import com.odi.model.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductRepository extends JpaRepository<Product, String> {
	List<Product> findByCompanyId(String companyId);

	long countProductsByCompanyId(String companyId);

	Page<Product> findProductByCompanyId(String companyId, Pageable pageable);

	List<Product> findProductByCompanyId(String companyId);

	long countProductByCompanyId(String companyId);
}
