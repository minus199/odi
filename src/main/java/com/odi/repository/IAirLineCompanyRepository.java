package com.odi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.odi.model.AirLineCompany;

public interface IAirLineCompanyRepository extends
		JpaRepository<AirLineCompany, String> {
    Optional<AirLineCompany> findByName(String name);
}
