package com.odi.repository;

import com.odi.model.ConnectedFlight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IConnectedFlightRepository extends JpaRepository<ConnectedFlight, String> {

}
