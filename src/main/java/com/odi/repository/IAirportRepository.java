package com.odi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.odi.model.Airport;

public interface IAirportRepository extends JpaRepository<Airport, String> {
    Optional<Airport> findByName(String name);
}
