package com.odi.repository;

import com.odi.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by magicalis on 09/03/2017.
 */
public interface IUserRepository extends JpaRepository<User, String> {
	Optional<User> findOneByEmail(String email);

	Optional<User> findOneByName(String name);

	Optional<User> findOneByPasswordHash(String hashedPassword);
}
