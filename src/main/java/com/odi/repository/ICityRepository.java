package com.odi.repository;

import com.odi.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;


@RestResource(path="city", rel="city")
//@RepositoryRestResource(collectionResourceRel = "people", path = "people")
public interface ICityRepository extends JpaRepository<City, String> {
    Optional<City> findByName(String name);
    City findByUniquenameAndCountryId(String uniquename, String countryId);
}
