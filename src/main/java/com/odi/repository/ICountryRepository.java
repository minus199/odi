package com.odi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.odi.model.Country;

public interface ICountryRepository extends JpaRepository<Country, String> {
    Optional<Country> findByName(String name);
	//@Query("SELECT p FROM Country p WHERE p.name = :name")
    //Country findByName(@Param("name") String name);
	
	//@Query("SELECT t FROM Country t WHERE t.fooIn = ?1 AND t.bar = ?2")
    //Country findByFooInAndBar(String fooIn, String bar);
	
    Country findByCode(String code);
    Country findByUniquename(String uniquename);
}