package com.odi.repository;

import com.odi.model.offer.FlightDelayProposal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface IFlightDelayProposalRepository extends JpaRepository<FlightDelayProposal, String> {
    List<FlightDelayProposal> findAcceptedState();
}
