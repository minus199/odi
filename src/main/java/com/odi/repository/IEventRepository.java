package com.odi.repository;

import com.odi.model.event.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface IEventRepository extends JpaRepository<Event, String> {
    List<Event> findWaitingStatus();
    List<Event> findProccessedStatus();
}
