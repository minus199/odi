package com.odi.repository;

import com.odi.model.ActivityLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IActivityLogRepository extends JpaRepository<ActivityLog, String> {
    List<ActivityLog> findByRelatedEntityId(String relatedEntityId);
}
