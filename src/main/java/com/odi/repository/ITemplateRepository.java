package com.odi.repository;

import com.odi.model.product.Template;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ITemplateRepository extends JpaRepository<Template, String> {
}
