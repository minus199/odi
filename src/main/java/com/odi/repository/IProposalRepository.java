package com.odi.repository;

import com.odi.model.offer.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProposalRepository extends JpaRepository<Proposal, String> {
	List<Proposal> findProposedState();

	List<Proposal> findAcceptedState();

	List<Proposal> findByCompanyId(String companyId);

	long countProposalsByCompanyId(String companyId);
}
