package com.odi.repository;

import com.odi.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

public interface IFlightRepository extends JpaRepository<Flight, String> {
	Optional<Flight> findByName(String name);

	List<Flight> findByAirlineIdAndFlightNumber(String airlineId, int flightNumber);

	List<Flight> findByOriginDest(String origin, String dest);

	List<Flight> findByoriginAirportId(String origin);

	List<Flight> findByoriginAirportIdAndDay(String origin, DayOfWeek day);

	List<Flight> findBydestinationAirportId(String destination);

	List<Flight> findBydestinationAirportIdAndDay(String destination, DayOfWeek day);

//@Query("SELECT f FROM FLIGHT order by AIRLINE_ID , ORIGIN_AIRPORT_ID , DESTINATION_AIRPORT_ID , DAY_OF_WEEK ", nativeQuery = true)
	//List<Flight> findAllOrdered();
	//List<Flight> findAllOrdered();
	//List<Flight> findAllOrderByairlineIdOriginAirportId();
	//@Query("Select f from Flight")
	//List<Flight> findOrdered(Sort sort);
}
