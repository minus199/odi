package com.odi.service;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.odi.excptions.InvalidDataException;

public class ServiceResponse {
	
	public static final String OK = "OK";
	public static final String Error = "Error";
	private String resultCode = ServiceResponse.OK;
	private String message = "";
	private String dataType = "";
	private Object data = null;
	private String fieldName = "";
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		if(data != null)
			this.setDataType(data.getClass().getSimpleName());
		else
			this.setDataType("null");
		this.data = data;
	}
	
	public void DigestException(Exception e)
	{
		setResultCode(Error);
		setMessage(e.getMessage());
		
		if(e instanceof InvalidDataException)
			setFieldName(((InvalidDataException)e).getFieldName());
	}
	
	@JsonIgnore
	@Transient
	public boolean IsSuccess() 
	{
		return resultCode.equals(ServiceResponse.OK);
	}
	
	public static ServiceResponse ErrorResponse(String message) 
	{
		ServiceResponse resp = new ServiceResponse();
		resp.setMessage(message);
		resp.setResultCode(ServiceResponse.Error);
		return resp;
	}
	
	public static ServiceResponse SuccessResponse(Object data) 
	{
		ServiceResponse resp = new ServiceResponse();
		resp.setData(data);
		resp.setResultCode(ServiceResponse.OK);
		return resp;
	}

}
