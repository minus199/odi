package com.odi.service.users.impl;

import com.odi.model.user.User;
import com.odi.service.IUserService;
import com.odi.service.users.domain.APIUser;
import com.odi.service.users.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by minus on 4/2/17.
 */

@Service
public class OdiUserDetailsService implements UserDetailsService {
	private final IUserService userService;

	@Autowired
	public OdiUserDetailsService(IUserService userService) {this.userService = userService;}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.getByName(username).orElseThrow(UserNotFoundException::new);
		return new APIUser(user);
	}
}
