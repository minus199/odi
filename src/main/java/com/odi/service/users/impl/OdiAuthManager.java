package com.odi.service.users.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Created by minus on 4/19/17.
 */
@Service
public class OdiAuthManager extends ProviderManager {
	@Autowired
	public OdiAuthManager(ApiUserAuthenticationProvider authenticationProvider) {
		super(Arrays.asList(authenticationProvider));
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		return super.authenticate(authentication);
	}
}
