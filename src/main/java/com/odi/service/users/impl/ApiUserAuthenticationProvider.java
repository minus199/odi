package com.odi.service.users.impl;

import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyAuthoritiesMapper;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ApiUserAuthenticationProvider extends DaoAuthenticationProvider {
	public ApiUserAuthenticationProvider(UserDetailsService userService, RoleHierarchy roleHierarchy) {
		super();
		setUserDetailsService(userService);
		setPasswordEncoder(new BCryptPasswordEncoder());
		setAuthoritiesMapper(new RoleHierarchyAuthoritiesMapper(roleHierarchy));
		setPreAuthenticationChecks(new AccountStatusUserDetailsChecker());
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		return super.authenticate(authentication);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
