package com.odi.service.users.exceptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by minus on 4/1/17.
 */
public class UserNotFoundException extends UsernameNotFoundException {
	public UserNotFoundException(String msg) {
		super(msg);
	}

	public UserNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}

	public UserNotFoundException() {
		super("Unable to login. User not exists or bad credentials.");
	}
}
