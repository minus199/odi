package com.odi.service.users.domain;

import com.odi.model.user.User;
import com.odi.model.user.UserRole;

/**
 * Created by minus on 4/3/17.
 */
public class GuestUser extends APIUser {
	private GuestUser(User user) {
		super(user);
	}

	public static final ISystemUser take() {
		return new GuestUser(new User("None", "Guest", UserRole.GUEST));
	}
}
