package com.odi.service.users.domain;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by minus on 4/3/17.
 */
public interface ISystemUser extends UserDetails {
	UsernamePasswordAuthenticationToken asToken();
}
