package com.odi.service;

import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by minus on 4/5/17.
 */
public class OdiResponse {
	private final HttpStatus status;
	private String message;
	private Map<String, Object> errors = new HashMap<>();
	private Object data = "";

	public OdiResponse(HttpStatus httpStatus, String message) {
		this.status = httpStatus;

		this.message = message;
	}

	public OdiResponse addError(String k, Object v) {
		errors.put(k, v);
		return this;
	}

	public Map<String, Object> getErrors() {
		return Collections.unmodifiableMap(errors);
	}

	public String getMessage() {
		return message;
	}

	public Object getData() {
		return data;
	}

	public int getStatus() {
		return status.value();
	}

	public OdiResponse setData(Object data) {
		this.data = data;
		return this;
	}

}
