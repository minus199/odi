package com.odi.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IServiceBase<T> {

	long count();

	T get(String id);

	Page<T> getAll(Pageable pageable);

	void delete(String id);

	void delete(T entity);

	void deleteInBatch(List<T> entities);

	T save(T entity);

	List<T> save(List<T> entity);

	List<T> getAll();
}
