package com.odi.service;

import com.odi.model.Flight;
import com.odi.service.model.FlightInfo;
import com.odi.service.model.FlightOption;
import com.odi.service.model.FlightPlan;

import java.io.File;
import java.time.DayOfWeek;
import java.util.List;

public interface IFlightService extends IServiceBase<Flight> {

	Flight saveFlight(String flightTime, String airlineId, int flightNumber, String originAirportId, String destinationAirportId, DayOfWeek day, int cost);

	Flight saveFlight(Flight flight);

	List<Flight> findByAirlineIdAndFlightNumber(String airlineId, int flightNumber);

	FlightPlan searchFlights(String originAirportId, String destAirportId, String departingDate, boolean isOneWay, String returnDate);

	FlightInfo flight2FlightInfo(Flight flight);

	Flight flightInfo2Flight(FlightInfo flightInfo);

	int loadFlights(String flightsFile);

	int loadFlights(File flightsFile);

	List<FlightOption> getFlightOptions(String originAirport, String destinationAirport, String departureDate, String returnDate);
}
