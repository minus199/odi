package com.odi.service.sms;

import com.odi.service.utils.model.IncomingSms;
import com.odi.service.utils.model.OutgoingSms;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Created by minus on 4/12/17.
 */
@Service
public class TwilioSmsService implements IMessageService<IncomingSms, OutgoingSms> {
	private final static Logger logger = LoggerFactory.getLogger(TwilioSmsService.class);

	public TwilioSmsService(@Value("${odi.services.sms.twilio.provider.auth_token}") String authToken,
	                        @Value("${odi.services.sms.twilio.provider.account_sid}") String accountSid) {
		Twilio.init(accountSid, authToken);
	}

	@Override
	public void handle(IncomingSms incomingMessage) {
		logger.info("Handling sms message " + incomingMessage);
	}

	@Override
	public void send(OutgoingSms outgoingMessage) {
		Assert.isTrue(! outgoingMessage.isEmpty(), "Message must be populated.");
		Message smsmessage = Message
				.creator(new PhoneNumber(outgoingMessage.getReceiver()),
				         new PhoneNumber(outgoingMessage.getSender()),
				         outgoingMessage.getContent())
				.create();

		outgoingMessage.setResponse(smsmessage);
	}
}
