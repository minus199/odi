package com.odi.service.sms;

import com.odi.service.utils.model.IncomingMessage;
import com.odi.service.utils.model.OutgoingMessage;

/**
 * Created by minus on 4/12/17.
 */
public interface IMessageService<IM extends IncomingMessage, OM extends OutgoingMessage> {

	void handle(IM incomingMessage);

	void send(OM outgoingMessage);
}
