package com.odi.service.batch.writeres.annotations;

import com.odi.service.IServiceBase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by minus on 3/31/17.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ServicedBy {
	Class<? extends IServiceBase> value();
}