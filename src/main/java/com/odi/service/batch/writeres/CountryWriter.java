package com.odi.service.batch.writeres;

import com.odi.model.Country;
import com.odi.service.ICountryService;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;

/**
 * Created by minus on 4/7/17.
 */

@ModeledBy(Country.class)
@ServicedBy(ICountryService.class)
public class CountryWriter<S extends ICountryService> extends BaseWriter<Country, S> {
	private S service;

	public CountryWriter(S service) {
		super(Country.class);
		this.service = service;
	}

	@Override
	public S getService() {
		return service;
	}
}