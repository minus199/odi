package com.odi.service.batch.writeres;

import com.odi.model.EntityBase;
import com.odi.service.IServiceBase;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by minus on 3/30/17.
 */
@Service
public class WriterFactory implements IWriterFactory {
	private static final List<Class<? extends IBaseWriter>> supportedWriters
			= Arrays.asList(
			AirLineCompanyWriter.class,
			CountryWriter.class,
			AirportWriter.class,
			CompanyWriter.class,
			FlightWriter.class,
			CityWriter.class);

	private final List<IServiceBase<? extends EntityBase>> injectedServices;

	@Autowired
	public WriterFactory(List<IServiceBase<? extends EntityBase>> injectedServices) {this.injectedServices = injectedServices;}

	@Override
	public <B extends EntityBase, T extends IBaseWriter<B, ? extends IServiceBase<B>>> T get(Class<B> baseEntity) {
		Class<? extends IBaseWriter> writerType = supportedWriters.stream()
				.filter(wc -> wc.isAnnotationPresent(ModeledBy.class)
				              && wc.isAnnotationPresent(ServicedBy.class)
				              && wc.getAnnotation(ModeledBy.class).value().isAssignableFrom(baseEntity))
				.findFirst().orElseThrow(() -> new RuntimeException(baseEntity.getName() + ": Pojo unsupported -- todo wrap in better exception"));

		IServiceBase<?> iServiceBase = injectedServices.stream()
				.filter(service -> writerType.getAnnotation(ServicedBy.class).value().isAssignableFrom(service.getClass()))
				.findFirst().orElseThrow(() -> new RuntimeException("should not happen -- remove after testing"));

		try {
			Constructor<T> constructor = (Constructor<T>) writerType.getConstructors()[0];
			return constructor.newInstance(iServiceBase);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}

		throw new RuntimeException("Not good.");
	}
}