package com.odi.service.batch.writeres;

import com.odi.model.City;
import com.odi.service.ICityService;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;

/**
 * Created by minus on 4/6/17.
 */

@ModeledBy(City.class)
@ServicedBy(ICityService.class)
public class CityWriter<S extends ICityService> extends BaseWriter<City, S> {
	private S service;

	public CityWriter(S service) {
		super(City.class);
		this.service = service;
	}

	@Override
	public S getService() {
		return service;
	}
}
