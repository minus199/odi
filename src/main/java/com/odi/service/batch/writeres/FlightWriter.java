package com.odi.service.batch.writeres;

import com.odi.model.Flight;
import com.odi.service.IFlightService;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;

@ModeledBy(Flight.class)
@ServicedBy(IFlightService.class)
public class FlightWriter<S extends IFlightService> extends BaseWriter<Flight, S> {
	private S service;

	public FlightWriter(S service) {
		super(Flight.class);
		this.service = service;
	}

	@Override
	public S getService() {
		return service;
	}
}
