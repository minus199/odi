package com.odi.service.batch.writeres;

import com.odi.model.EntityBase;
import com.odi.service.IServiceBase;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import org.springframework.batch.item.ItemWriter;

import java.util.Optional;

/**
 * Created by minus on 3/30/17.
 */
public interface IBaseWriter<T extends EntityBase, S extends IServiceBase<T>> extends ItemWriter<T> {
	default Optional<Class<? extends EntityBase>> isModeled() {
		return Optional.of(getClass().getAnnotation(ModeledBy.class).value());
	}

	S getService();

	Class<T> getPojo();
}
