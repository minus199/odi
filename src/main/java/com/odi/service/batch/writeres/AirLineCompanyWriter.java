package com.odi.service.batch.writeres;

import com.odi.model.AirLineCompany;
import com.odi.service.IAirLineCompanyService;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;

@ModeledBy(AirLineCompany.class)
@ServicedBy(IAirLineCompanyService.class)
public class AirLineCompanyWriter<S extends IAirLineCompanyService> extends BaseWriter<AirLineCompany, S> {
	private S service;

	public AirLineCompanyWriter(S service) {
		super(AirLineCompany.class);
		this.service = service;
	}

	@Override
	public S getService() {
		return service;
	}
}
