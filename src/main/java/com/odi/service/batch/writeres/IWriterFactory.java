package com.odi.service.batch.writeres;

import com.odi.model.EntityBase;
import com.odi.service.IServiceBase;

/**
 * Created by minus on 3/30/17.
 */
public interface IWriterFactory {

	<B extends EntityBase, T extends IBaseWriter<B, ? extends IServiceBase<B>>> T get(Class<B> baseEntity);
}
