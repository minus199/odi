package com.odi.service.batch.writeres;

import com.odi.model.EntityBase;
import com.odi.service.IServiceBase;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by minus on 3/30/17.
 */
abstract public class BaseWriter<T extends EntityBase, S extends IServiceBase<T>> implements IBaseWriter<T, S> {
	private final Class<T> pojo;

	public BaseWriter(Class<T> pojo) {
		this.pojo = pojo;
	}

	@Override
	public void write(List<? extends T> items) throws Exception {
		List<T> save = getService().save((List<T>) items);

		Assert.isTrue(items.size() == save.size(), "Some items failed to persist.");
	}

	@Override
	final public Class<T> getPojo() {
		return pojo;
	}
}
