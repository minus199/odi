package com.odi.service.batch.writeres;

import com.odi.model.Airport;
import com.odi.service.IAirportService;
import com.odi.service.batch.writeres.annotations.ModeledBy;
import com.odi.service.batch.writeres.annotations.ServicedBy;

@ModeledBy(Airport.class)
@ServicedBy(IAirportService.class)
public class AirportWriter<S extends IAirportService> extends BaseWriter<Airport, S> {
	private S service;

	public AirportWriter(S service) {
		super(Airport.class);
		this.service = service;
	}

	@Override
	public S getService() {
		return service;
	}
}
