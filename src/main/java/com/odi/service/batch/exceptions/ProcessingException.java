package com.odi.service.batch.exceptions;

import org.springframework.batch.core.JobExecutionException;

/**
 * Created by minus on 4/5/17.
 */
public class ProcessingException extends JobExecutionException {
	public ProcessingException(String msg) {
		super(msg);
	}

	public ProcessingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
