package com.odi.service.batch.exceptions;

import org.springframework.batch.item.file.FlatFileParseException;

/**
 * Created by minus on 4/6/17.
 */
public class ParsingFailedException extends ProcessingException {
	private final String message;
	private final int lineNumber;
	private final String lineInput;

	public ParsingFailedException(FlatFileParseException parseEx) {
		super("Failed parsing file");
		this.message = parseEx.getCause().getMessage();
		this.lineNumber = parseEx.getLineNumber();
		this.lineInput = parseEx.getInput();
	}

	@Override
	public String getMessage() {
		return message;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public String getLineInput() {
		return lineInput;
	}
}
