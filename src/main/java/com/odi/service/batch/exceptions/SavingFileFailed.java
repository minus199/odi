package com.odi.service.batch.exceptions;

/**
 * Created by minus on 4/6/17.
 */
public class SavingFileFailed extends ProcessingException {
	public SavingFileFailed(String msg) {
		super(msg);
	}

	public SavingFileFailed(String msg, Throwable cause) {
		super(msg, cause);
	}
}
