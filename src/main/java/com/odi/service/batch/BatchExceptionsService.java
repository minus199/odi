package com.odi.service.batch;

import com.odi.service.batch.exceptions.ParsingFailedException;
import com.odi.service.batch.exceptions.ProcessingException;
import com.odi.service.batch.exceptions.SavingFileFailed;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.ValidationException;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by minus on 4/6/17.
 */

@Service
public class BatchExceptionsService {

	/**
	 * @param process The processed job
	 * @return only boolean false and only when job's exit status is completed
	 * @throws ProcessingException When exit status is failed or some throwable was detected in execution steps
	 */
	public boolean handle(JobExecution process) throws ProcessingException {

		ExitStatus exitStatus = process.getExitStatus();
		if (exitStatus.equals(ExitStatus.COMPLETED)) {
			return false;
		}
		String exitCode = exitStatus.getExitCode();
		String exitDescription = exitStatus.getExitDescription();

		if (! StringUtils.isEmpty(exitDescription)) {
			throw new ProcessingException("Failed processing the file: " + exitDescription);
		}

		if (Objects.equals(ExitStatus.FAILED.getExitCode(), exitCode)) {
			Optional<Throwable> throwableOptional = Optional.ofNullable(process.getAllFailureExceptions().get(0));

			if (throwableOptional.isPresent()) {
				Throwable throwable = throwableOptional.get();
				if (throwable instanceof FlatFileParseException) {
					FlatFileParseException parseEx = (FlatFileParseException) throwable;
					throw new ParsingFailedException(parseEx);
				}

				if (throwable instanceof ValidationException) {
					ValidationException parseEx = (ValidationException) throwable;
					throw new SavingFileFailed(parseEx.getCause() + ": " + parseEx.getMessage());
				}

				throw new ProcessingException("Failed processing the file", throwable);
			}

			throw new ProcessingException("Failed processing the file");
		}

		return false;
	}
}
