package com.odi.service.batch;

import com.odi.model.EntityBase;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.core.io.Resource;

/**
 * Created by minus on 4/1/17.
 */
public interface IBatchProcessor {
	Boolean process(Class<? extends EntityBase> targetClass, Resource resource) throws JobExecutionException;
}
