package com.odi.service.batch.readers;

import com.odi.model.Country;
import org.springframework.core.io.Resource;

/**
 * Created by minus on 4/7/17.
 */
public class CountryReader extends BaseReader<Country> {
	public CountryReader(Resource resource) {
		super(Country.class, resource);
	}
}