package com.odi.service.batch.readers;

import com.odi.model.Airport;
import org.springframework.core.io.Resource;

public class AirportReader extends BaseReader<Airport> {

	public AirportReader(Resource resource) {
		super(Airport.class, resource);
	}
}
