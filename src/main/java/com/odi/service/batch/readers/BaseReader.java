package com.odi.service.batch.readers;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.Resource;

import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * Created by minus on 3/30/17.
 */
abstract public class BaseReader<T> extends FlatFileItemReader<T> implements IBaseReader<T> {
	private final Class<? extends T> pojoClass;

	public BaseReader(Class<? extends T> pojoClass, Resource resource) {
		this.pojoClass = pojoClass;
		setLinesToSkip(0);
		setResource(resource);

		setLineMapper(new DefaultLineMapper<T>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer("|") {
					{
						setNames(getTokenNames());
					}
				});

				setFieldSetMapper(new BeanWrapperFieldSetMapper<T>() {{
					setTargetType(pojoClass);
				}});
			}

			@Override
			public T mapLine(String line, int lineNumber) throws Exception {
				// this is where the pojo is created from the raw data
				return super.mapLine(line, lineNumber);
			}
		});
	}

	@Override
	protected T doRead() throws Exception {
		return super.doRead();
	}

	@Override
	public String[] getTokenNames() {
		// Default behaviour, Override with getTokenNames
		return Stream.of(pojoClass.getDeclaredFields())
				.map(Field::getName)
				.toArray(String[]::new);
	}

	@Override
	final public Class<? extends T> getPojoClass() {
		return pojoClass;
	}
}
