package com.odi.service.batch.readers;

import com.odi.model.EntityBase;
import org.springframework.core.io.Resource;

/**
 * Created by minus on 3/30/17.
 */
public interface IReaderFactory {
	<B extends EntityBase, T extends BaseReader<B>> T get(Class<B> baseEntity, Resource resource);
}
