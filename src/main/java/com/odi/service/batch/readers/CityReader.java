package com.odi.service.batch.readers;

import com.odi.model.City;
import org.springframework.core.io.Resource;

/**
 * Created by minus on 4/6/17.
 */
public class CityReader extends BaseReader<City> {
	public CityReader(Resource resource) {
		super(City.class, resource);
	}

	@Override
	public String[] getTokenNames() {
		return new String[]{"name", "country_id", "uniquename", "id"};
	}
}