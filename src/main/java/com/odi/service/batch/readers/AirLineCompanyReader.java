package com.odi.service.batch.readers;

import com.odi.model.AirLineCompany;
import org.springframework.core.io.Resource;

public class AirLineCompanyReader extends BaseReader<AirLineCompany> {
	public AirLineCompanyReader(Resource resource) {
		super(AirLineCompany.class, resource);
	}

	@Override
	public String[] getTokenNames() {
		return new String[]{"city_id", "code", "country_id", "slug"};
	}
}
