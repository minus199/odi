package com.odi.service.batch.readers;

import com.odi.model.Flight;
import org.springframework.core.io.Resource;

public class FlightReader extends BaseReader<Flight> {

	public FlightReader(Resource resource) {
		super(Flight.class, resource);
	}
}
