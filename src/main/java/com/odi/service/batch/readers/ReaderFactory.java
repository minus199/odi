package com.odi.service.batch.readers;

import com.odi.model.*;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by minus on 3/30/17.
 */
@Service
public class ReaderFactory implements IReaderFactory {

	private static final Map<
			Class<? extends EntityBase>,
			Class<? extends IBaseReader>> supportedReaders = new HashMap<>();

	static {
		supportedReaders.put(AirLineCompany.class, AirLineCompanyReader.class);
		supportedReaders.put(Airport.class, AirportReader.class);
		supportedReaders.put(Company.class, CompanyReader.class);
		supportedReaders.put(Flight.class, FlightReader.class);
		supportedReaders.put(Country.class, CountryReader.class);
		supportedReaders.put(City.class, CityReader.class);
	}

	@Override
	public <B extends EntityBase, T extends BaseReader<B>> T get(Class<B> baseEntity, Resource resource) {
		Class<? extends IBaseReader> readerType = Optional.of(supportedReaders.get(baseEntity))
				.orElseThrow(() -> new RuntimeException(baseEntity.getName() + ": Pojo unsupported -- todo wrap in better exception"));

		try {
			Constructor<T> constructor = (Constructor<T>) readerType.getConstructors()[0];
			return constructor.newInstance(resource);
		} catch (Exception e) {
			throw new RuntimeException("Not good.", e);
		}
	}
}
