package com.odi.service.batch.readers;

/**
 * Created by minus on 3/31/17.
 */
public interface IBaseReader<T> {
	String[] getTokenNames();

	Class<? extends T> getPojoClass();
}
