package com.odi.service.utils;

import com.odi.service.utils.model.IncomingMessage;
import com.odi.service.utils.model.OutgoingMessage;

/**
 * Created by minus on 4/9/17.
 */
public interface MessagingHandler<IM extends IncomingMessage, OM extends OutgoingMessage> {
	IM normalizeIncoming(Object rawPayload);

	void send(OM outgoingMessage);

	void get(IM incomingMessage);

	void send(String userContactInfo, String format);
}
