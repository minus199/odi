package com.odi.service.utils.model;

/**
 * Created by minus on 4/9/17.
 */
public class OutgoingSms implements OutgoingMessage {
	private final String receiver;
	private final String sender;
	private final String content;
	private Object response;

	public OutgoingSms(String receiver, String sender, String content) {
		this.receiver = receiver;
		this.sender = sender;
		this.content = content;
	}

	public String getReceiver() {
		return receiver;
	}

	public String getSender() {
		return sender;
	}

	public String getContent() {
		return content;
	}

	@Override
	public Object getResponse() {
		return this.response;
	}

	@Override
	public OutgoingMessage setResponse(Object response) {
		this.response = response;
		return this;
	}
}
