package com.odi.service.utils.model;

import java.util.Map;

/**
 * Created by minus on 4/9/17.
 */
abstract public class IncomingMessage {
	private final String receiver;
	private final String sender;
	private final String message;
	private final Map<String, ?> metaData;

	protected IncomingMessage(String receiver, String sender, String message) {
		this(receiver, sender, message, null);
	}

	protected IncomingMessage(String receiver, String sender, String message, Map<String, ?> metaData) {
		this.receiver = receiver;
		this.sender = sender;
		this.message = message;
		this.metaData = metaData;
	}

	public String getReceiver() {
		return receiver;
	}

	public String getSender() {
		return sender;
	}

	public String getMessage() {
		return message;
	}

	public Map<String, ?> getMetaData() {
		return metaData;
	}
}
