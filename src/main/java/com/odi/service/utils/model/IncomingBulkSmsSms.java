package com.odi.service.utils.model;

import com.google.gson.annotations.SerializedName;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Instant;
import java.util.Date;

/**
 * Created by minus on 4/9/17.
 */
public class IncomingBulkSmsSms {
	/**
	 * the number that the incoming message was sent to.
	 */
	@SerializedName("msisdn")
	private String receiver;

	/**
	 * the number that the incoming message was sent from.
	 */
	private String sender;

	/**
	 * the message sent (can be empty): 0-160 characters.
	 */
	private String message;

	/**
	 * the message content type, being one of: 7bit/8bit/16bit.
	 * For 8bit (binary SMS) and 16bit (Unicode SMS),
	 * message will be encoded as a hex string, e.g. 004200430044060006010602.
	 */
	@SerializedName("dca")
	private String encoding;

	/**
	 * an integer identifier unique to this incoming message.
	 * You must use this to eliminate possible duplicate submissions reaching you,
	 * if such duplicates would be problematic for you.
	 */
	@SerializedName("msg_id")
	private Integer msgId;

	/**
	 * if this message is in reply to a message previously sent, will be set to the source_id of the original message that was sent.
	 */
	@SerializedName("source_id")
	private String sourceId;

	/**
	 * the integer identifier that was returned by the send_sms api call at the time
	 * when the original message was submitted, if this message is in reply to another previously sent.
	 * If this incoming message is not a reply, this field will be set to zero.
	 */
	@SerializedName("referring_batch_id")
	private String referringBatchId;

	/**
	 * the mcc+mnc tuple identifying the cellular network that this message belongs to
	 * - useful for detecting ported numbers (this information is not always available
	 * - an empty string is set if not determinable).
	 */
	@SerializedName("network_id")
	private String networkId;

	/**
	 * the concatenated reference number (only present for concatenated messages). (values: 0-255)
	 */
	@SerializedName("concat_reference")
	private Integer concatReference;

	/**
	 * the total number of messages within the concatenated set (only present for concatenated messages). (values: 1-255)
	 */
	@SerializedName("concat_num_segments")
	private Integer concatNumSegments;

	/**
	 * the sequence number of this message within the concatenated set (only present for concatenated messages). (values: 1-255)
	 */
	@SerializedName("concat_seq_num")
	private Integer concatSeqNum;

	/**
	 * the time that the message was received, relative to the time zone set in your Profile. Format: yyyy-MM-dd HH:mm:ss (24 hour clock).
	 */
	@SerializedName("received_time")
	private Date receivedTime;

	public IncomingBulkSmsSms(String receiver, String sender, String message,
	                          String encoding, Integer msgId, String sourceId,
	                          String referringBatchId, String networkId,
	                          Integer concatReference, Integer concatNumSegments,
	                          Integer concatSeqNum, Date receivedTime) {
		this.receiver = receiver;
		this.sender = sender;
		this.message = message;
		this.encoding = encoding;
		this.msgId = msgId;
		this.sourceId = sourceId;
		this.referringBatchId = referringBatchId;
		this.networkId = networkId;
		this.concatReference = concatReference;
		this.concatNumSegments = concatNumSegments;
		this.concatSeqNum = concatSeqNum;
		this.receivedTime = receivedTime;
	}

	public IncomingBulkSmsSms() {
	}

	@Override
	public String toString() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("")
				.queryParam("concatReference", concatReference)
				.queryParam("concatNumSegments", concatNumSegments)
				.queryParam("concatSeqNum", concatSeqNum)
				.queryParam("encoding", encoding)
				.queryParam("message", message)
				.queryParam("msgId", msgId)
				.queryParam("networkId", networkId)
				.queryParam("receivedTime", receivedTime)
				.queryParam("receiver", receiver)
				.queryParam("referringBatchId", referringBatchId)
				.queryParam("sender", sender)
				.queryParam("sourceId", sourceId);

		return builder.buildAndExpand(builder).toUri().toString();
	}

	public String getReceiver() {
		return receiver;
	}

	public String getSender() {
		return sender;
	}

	public String getMessage() {
		return message;
	}

	public String getEncoding() {
		return encoding;
	}

	public Integer getMsgId() {
		return msgId;
	}

	public String getSourceId() {
		return sourceId;
	}

	public String getReferringBatchId() {
		return referringBatchId;
	}

	public String getNetworkId() {
		return networkId;
	}

	public Integer getConcatReference() {
		return concatReference;
	}

	public Integer getConcatNumSegments() {
		return concatNumSegments;
	}

	public Integer getConcatSeqNum() {
		return concatSeqNum;
	}

	public Date getReceivedTime() {
		return receivedTime;
	}

	public IncomingBulkSmsSms setReceiver(String receiver) {
		this.receiver = receiver;
		return this;
	}

	public IncomingBulkSmsSms setSender(String sender) {
		this.sender = sender;
		return this;
	}

	public IncomingBulkSmsSms setMessage(String message) {
		this.message = message;
		return this;
	}

	public IncomingBulkSmsSms setEncoding(String encoding) {
		this.encoding = encoding;
		return this;
	}

	public IncomingBulkSmsSms setMsgId(Integer msgId) {
		this.msgId = msgId;
		return this;
	}

	public IncomingBulkSmsSms setSourceId(String sourceId) {
		this.sourceId = sourceId;
		return this;
	}

	public IncomingBulkSmsSms setReferringBatchId(String referringBatchId) {
		this.referringBatchId = referringBatchId;
		return this;
	}

	public IncomingBulkSmsSms setNetworkId(String networkId) {
		this.networkId = networkId;
		return this;
	}

	public IncomingBulkSmsSms setConcatReference(Integer concatReference) {
		this.concatReference = concatReference;
		return this;
	}

	public IncomingBulkSmsSms setConcatNumSegments(Integer concatNumSegments) {
		this.concatNumSegments = concatNumSegments;
		return this;
	}

	public IncomingBulkSmsSms setConcatSeqNum(Integer concatSeqNum) {
		this.concatSeqNum = concatSeqNum;
		return this;
	}

	public IncomingBulkSmsSms setReceivedTime(String receivedTime) {
		this.receivedTime = Date.from(Instant.parse(receivedTime));
		return this;
	}
}
