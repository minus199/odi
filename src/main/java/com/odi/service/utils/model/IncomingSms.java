package com.odi.service.utils.model;

import java.util.Map;

/**
 * Created by minus on 4/12/17.
 */
public class IncomingSms extends IncomingMessage {
	public IncomingSms(String receiver, String sender, String message) {
		super(receiver, sender, message);
	}

	public IncomingSms(String receiver, String sender, String message, Map<String, ?> metaData) {
		super(receiver, sender, message, metaData);
	}
}
