package com.odi.service.utils.model;

/**
 * Created by minus on 4/10/17.
 */
public enum SmsSubmissionStatus {
	IN_PROGRESS(0, "In progress"),
	SCHEDULED(1, "Scheduled"),
	INTERNAL_FATAL_ERROR(22, "Internal fatal error"),
	AUTHENTICATION_FAILURE(23, "Authentication failure"),
	DATA_VALIDATION_FAILED(24, "Data validation failed"),
	YOU_DO_NOT_HAVE_SUFFICIENT_CREDITS(25, "You do not have sufficient credits"),
	UPSTREAM_CREDITS_NOT_AVAILABLE(26, "Upstream credits not available"),
	YOU_HAVE_EXCEEDED_YOUR_DAILY_QUOTA(27, "You have exceeded your daily quota"),
	UPSTREAM_QUOTA_EXCEEDED(28, "Upstream quota exceeded"),
	TEMPORARILY_UNAVAILABLE(40, "Temporarily unavailable"),
	MAXIMUM_BATCH_SIZE_EXCEEDED(201, "	Maximum batch size exceeded");

	SmsSubmissionStatus(int code, String statusMessage) {

	}
}
