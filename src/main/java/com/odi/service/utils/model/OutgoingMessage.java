package com.odi.service.utils.model;

import org.springframework.util.StringUtils;

/**
 * Created by minus on 4/9/17.
 */
public interface OutgoingMessage {
	String getReceiver();

	String getSender();

	String getContent();

	Object getResponse();

	default boolean isEmpty() {
		return StringUtils.isEmpty(getSender())
		       && StringUtils.isEmpty(getReceiver())
		       && StringUtils.isEmpty(getContent());
	}

	OutgoingMessage setResponse(Object response);
}
