package com.odi.service.utils;

import com.odi.service.utils.model.IncomingBulkSmsSms;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by minus on 4/10/17.
 */
@Service
public class IncomingSmsSerivce {
	private final String providerEntryPoint;
	private final String userName;
	private final String password;

	public IncomingSmsSerivce(@Value("odi.services.sms.bulksms.provider.entrypoint") String providerEntryPoint,
	                          @Value("odi.services.sms.bulksms.provider.username") String userName,
	                          @Value("odi.services.sms.bulksms.provider.password") String password) {

		this.providerEntryPoint = providerEntryPoint;
		this.userName = userName;
		this.password = password;
	}

	public void handle(IncomingBulkSmsSms incomingSms) {
		System.out.println(incomingSms.toString());
	}
}
