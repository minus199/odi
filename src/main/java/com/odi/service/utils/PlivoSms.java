package com.odi.service.utils;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;

public class PlivoSms {
	private final Logger logger = LoggerFactory.getLogger("service");

	public void Send(String to, String message) {
		String authId = "MAODKYNZQ0ZGUXN2IWMD"; // AUTH_ID
		String authToken = "MjkyMGYxMDI5ZWUyODljNWU4ODYwMWM1NzUzMDBk";
		RestAPI api = new RestAPI(authId, authToken, "v1");

		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", "ODI"); // Sender's phone number with country code
		parameters.put("dst", to); // Receiver's phone number with country code
		parameters.put("text", message); // Your SMS text message
		// Send Unicode text
		//parameters.put("text", "こんにちは、元気ですか？"); // Your SMS text message - Japanese
		//parameters.put("text", "Ce est texte généré aléatoirement"); // Your SMS text message - French
		parameters.put("url", "http://example.com/report/"); // The URL to which with the status of the message is sent
		parameters.put("method", "GET"); // The method used to call the url

		try {
			// Send the message
			MessageResponse msgResponse = api.sendMessage(parameters);

			// Print the response
			System.out.println(msgResponse);
			// Print the Api ID
			System.out.println("Api ID : " + msgResponse.apiId);
			// Print the Response Message
			System.out.println("Message : " + msgResponse.message);

			if (msgResponse.serverCode == 202) {
				// Print the Message UUID
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		}
	}
}
