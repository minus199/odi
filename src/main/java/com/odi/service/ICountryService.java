package com.odi.service;

import com.odi.model.Country;

public interface ICountryService extends IServiceBase<Country> {
	
	public Country findByUniquename(String uniqueName);

}
