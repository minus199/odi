package com.odi.service;

import com.odi.model.Airport;

public interface IAirportService extends IServiceBase<Airport> {

}
