package com.odi.service;

import com.odi.model.event.Event;

public interface IProbabilityService {

	double getProbability(Event event);
	
}
