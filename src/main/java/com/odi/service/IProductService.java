package com.odi.service;

import com.odi.model.product.Product;

import java.util.List;

public interface IProductService extends IServiceBase<Product> {
	List<Product> findByCompanyId(String companyId);

	String findRelevantProductId(String companyId, Class templateType, Object... args);
}
