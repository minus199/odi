package com.odi.service;

import com.odi.model.offer.Proposal;

import java.util.List;
import java.util.Map;

public interface IProposalService extends IServiceBase<Proposal> {
	Proposal getProposal(String type, Map<String, String> parameters);

	Proposal proposed(Proposal proposal, String description);

	Proposal accept(Proposal proposall, String description);

	Proposal accept(String idl, String description);

	Proposal realize(Proposal proposall, String description);

	Proposal passed(Proposal proposall, String description);

	Proposal realized(Proposal proposall, String description);

	Proposal notify(Proposal proposall, String description);

	Proposal expired(Proposal proposall, String description);

	List<Proposal> findByCompanyId(String companyId);

	List<Proposal> getAllAccepted();

	List<Proposal> getAllProposed();
}
