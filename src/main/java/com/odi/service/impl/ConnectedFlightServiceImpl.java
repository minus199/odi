package com.odi.service.impl;

import com.odi.model.ConnectedFlight;
import com.odi.model.Flight;
import com.odi.repository.IFlightRepository;
import com.odi.service.IConnectedFlightService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("connectedFlightService")
public class ConnectedFlightServiceImpl extends ServiceBaseImpl<ConnectedFlight> implements IConnectedFlightService {

	final int MAX_RESULTS = 30;
	
	@Autowired
	IFlightRepository flightRepository;
	
	@Override
	public List<ConnectedFlight> getFlights(String sourceAirport, String destinationAirport, String departureDate) {
		Instant start = Instant.now();
		
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime dt = formatter.parseDateTime(departureDate);
		DayOfWeek dayOfWeek = DayOfWeek.of(dt.getDayOfWeek());

		List<Flight> directFlights = flightRepository.findByOriginDest(sourceAirport, destinationAirport);
		System.out.println("---------------> " + Duration.between(start, Instant.now()));

		Flight referenceFlight = null;
		List<Flight> firstFlights = flightRepository.findByoriginAirportIdAndDay(sourceAirport, dayOfWeek);
		System.out.println("---------------> " + Duration.between(start, Instant.now()));
		
		List<Flight> secondFlights = flightRepository.findBydestinationAirportIdAndDay(destinationAirport, dayOfWeek);
		List<ConnectedFlight> connections = new ArrayList<ConnectedFlight>();
		System.out.println("---------------> " + Duration.between(start, Instant.now()));
		
		

		
		if (directFlights.size() > 0)
			referenceFlight = directFlights.get(0);
		
		for (Flight flight1: firstFlights) {
			for (Flight flight2: secondFlights) {
				if (flight1.getDestinationAirportId().equals(flight2.getOriginAirportId()) == false)
					continue;
				
				// Potential Connection,...
				
				// If alternatives too long
				if (referenceFlight != null) {
					if (referenceFlight.getFlightDurationHours() != 0 && referenceFlight.getFlightDurationHours() < flight1.getFlightDurationHours())
						continue;
					if (referenceFlight.getFlightDurationHours() != 0 && referenceFlight.getFlightDurationHours() < flight2.getFlightDurationHours())
						continue;
				}

				// Minimal time between flights
				int ALLOWED_DELAY = (3600 * 1000);
				Date dateWithAllowedDelay = new Date(flight1.getFlightLanding().getTime() + ALLOWED_DELAY);
				if (dateWithAllowedDelay.after(flight2.getFlightTime()))
					continue;
				
								
				// Maximal time between flights
				int MAXIMAL_DELAY = (3 * 3600 * 1000);
				Date dateWithMaximalDelay = new Date(flight1.getFlightLanding().getTime() + MAXIMAL_DELAY);
				if (dateWithMaximalDelay.before(flight2.getFlightTime()))
					continue;
				
				// Make sure it is the relevant day
				if (flight1.getDayOfWeek().equals(dayOfWeek) == false)
					continue;
				if (flight2.getDayOfWeek().equals(dayOfWeek) == false)
					continue;

//				if (referenceFlight.getFlightDurationHours() != 0)
//					if (referenceFlight.getFlightDurationHours() * 2 > (flight1.getFlightDurationHours() + flight2.getFlightDurationHours()))
//						continue;

//				// Not more expensive
//				int MINIMAL_COST_BENEFIT = 50;
//				if (referenceFlight != null) {
//					if (referenceFlight.getTempFlightCost() < 
//							flight1.getTempFlightCost() + flight2.getTempFlightCost() + MINIMAL_COST_BENEFIT)
//						continue;
//				}
				
								
				ConnectedFlight c = new ConnectedFlight(flight1, flight2);
				connections.add(c);
				
			}

		}

		java.util.Collections.sort(connections);

		logger.info("ConnectedFlights:getFlights (TLV, CDG) ::: Entries: " + String.valueOf(connections.size()));
		if (connections.size() > MAX_RESULTS)
			return connections.subList(0, MAX_RESULTS);
		return connections;
	}
	
	public int minutesBetweenFlights(Flight first, Flight second) {
		int minutes = 0;
		long firstFlightLandingTime = first.getFlightLanding().getTime();
		long secondFlightDepartureTime = second.getFlightTime().getTime();
		
		minutes = (int)((secondFlightDepartureTime/60000) - (firstFlightLandingTime/60000));
		
		return minutes;
	}

}
