/**
 * 
 */
package com.odi.service.impl;

import org.springframework.stereotype.Repository;

import com.odi.model.Country;
import com.odi.repository.ICountryRepository;
import com.odi.service.ICountryService;

/**
 * @author eyal
 *
 */
@Repository("countryService")
public class CountryServiceImpl extends ServiceBaseImpl<Country> implements ICountryService {

	public Country findByUniquename(String uniqueName) {
		return ((ICountryRepository)repository).findByUniquename(uniqueName);
	}

}
