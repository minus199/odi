package com.odi.service.impl;

import com.odi.model.Flight;
import com.odi.model.offer.FlightCanceledProposal;
import com.odi.model.offer.FlightDelayProposal;
import com.odi.model.offer.MissedConnectionProposal;
import com.odi.model.offer.Proposal;
import com.odi.model.product.FlightDelayProductTemplate;
import com.odi.model.product.MissedConnectionProductTemplate;
import com.odi.model.product.Product;
import com.odi.repository.IFlightDelayProposalRepository;
import com.odi.service.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("flightProposalService")
public class FlightProposalServiceImpl implements IFlightProposalService, ApplicationContextAware {
//public class FlightProposalServiceImpl extends ServiceBaseImpl<FlightDelayProposal> implements IFlightProposalService {

	//@Autowired
	//private ApplicationContext context;
	//private static ApplicationContext context = null;


	@Autowired
    IFlightDelayProposalRepository flightDelayProposalRepository;

	@Autowired
	IFlightService flightService;

	@Autowired
	IConnectedFlightService connectedFlightService;

	//@Autowired
	IProposalService proposalService;

	@Autowired
	IProductService productService;

	@Autowired
	IPricingService pricingService;

	public Proposal acceptFlightProposal(String proposalID) {
		Proposal proposal =	proposalService.get(proposalID);
		return proposalService.accept(proposal, "Proposal Accepted");
	}
	
	public Proposal accept(String proposalID, String userContactInfo) {
		Proposal proposal =	proposalService.get(proposalID);
		proposal.setUserContactInfo(userContactInfo);
		return proposalService.accept(proposal, "Proposal Accepted");
	}
/*
	private String _findRelevantFlightDelayProduct(String companyId, Class templateType, Flight flight) {
		String productId = "GenericFlightDelay";
		List<Product> products = productService.getAll();
		
		for (Product p: products) {
			FlightDelayProductTemplate template;
			
			if (p.getCompanyId().toLowerCase().equals(companyId.toLowerCase()) == false)
				continue;
			
			if (p.getTemplate().getClass() != templateType)
				continue;
			
			template = (FlightDelayProductTemplate)p.getTemplate();
			if (template.getAirlines().isEmpty() == false && template.getAirlines().contains(flight.getAirlineId()) == false)
				continue;
			if (template.getDepartureAirport().isEmpty() == false && template.getDepartureAirport().contains(flight.getOriginAirportId()) == false)
				continue;
			if (template.getArrivalAirport().isEmpty() == false && template.getArrivalAirport().contains(flight.getDestinationAirportId()) == false)
				continue;

			productId = p.getId();
		}
		
		return productId;
	}

	private String _findRelevantMissedConnectionProduct(String companyId, Class templateType, Flight flight1, Flight flight2) {
		String productId = "GenericMissedConnection";
		List<Product> products = productService.getAll();
		
		for (Product p: products) {
			MissedConnectionProductTemplate template;
			
			if (p.getCompanyId().toLowerCase().equals(companyId.toLowerCase()) == false)
				continue;
			
			if (p.getTemplate().getClass() != templateType)
				continue;
			
//			template = (MissedConnectionProductTemplate)p.getTemplate();
//			if (template.getAirlines().isEmpty() == false && template.getAirlines().contains(flight.getAirlineId()) == false)
//				continue;
//			if (template.getDepartureAirport().isEmpty() == false && template.getDepartureAirport().contains(flight.getOriginAirportId()) == false)
//				continue;
//			if (template.getArrivalAirport().isEmpty() == false && template.getArrivalAirport().contains(flight.getDestinationAirportId()) == false)
//				continue;
//
//			productId = p.getId();
		}
		
		return productId;
	}
*/

	@Override
	public Proposal getMissedConnectionProposal(String flight1Id, String flight2Id,
												String companyId,
												Date date, double compensation, String userContactInfo) {
		Flight flight1 = flightService.get(flight1Id);
		Flight flight2 = flightService.get(flight2Id);
		//String productId = findRelevantMissedConnectionProduct(companyId, MissedConnectionProductTemplate.class, flight1, flight2);
		String productId = productService.findRelevantProductId(companyId, MissedConnectionProductTemplate.class, flight1, flight2);

		Product product = productService.get(productId);
		MissedConnectionProposal proposal = new MissedConnectionProposal();

		proposal.setFirstFlight(flight1);
		proposal.setSecondFlight(flight2);
		proposal.setTravelDate(date);
		proposal.setProduct(product);
		proposal.setCompanyId(companyId);

		proposal.setUserContactInfo(userContactInfo);

		if (compensation == 0)
			proposal.setCompensation((flight1.getTempFlightCost()+flight2.getTempFlightCost())*1.2);

		proposal.setCost(pricingService.getCost(proposal));

		return proposalService.proposed(proposal, "");
	}


	public Proposal getFlightDelayProposal(String flightId, String companyId, int delayHours, int delayMinutes, Date date, double compensation, String userContactInfo) {
		Flight flight = flightService.get(flightId);
		if (flight == null)
			return null;

		//String productId = findRelevantFlightDelayProduct(companyId, FlightDelayProductTemplate.class, flight);
		String productId = productService.findRelevantProductId(companyId, FlightDelayProductTemplate.class, flight);
		return getFlightDelayProposal(
				flight,
				companyId,
				productId,
				delayHours, delayMinutes,
				date,
				compensation,
				userContactInfo
			);
	}
	

	private Proposal getFlightDelayProposal(Flight flight, String companyId, String productId, int delayHours, int delayMinutes, Date date, 
			double compensation, String userContactInfo) {
		FlightDelayProposal proposal = new FlightDelayProposal(
				flight.getId(),
				delayHours,
				delayMinutes,
				flight.getAirlineId(),
				flight.getFlightNumber(),
				date,
				flight.getFlightTime(),
				compensation);

		proposal.setUserContactInfo(userContactInfo);

		Product delayProduct = productService.get(productId);
		proposal.setProduct(delayProduct);
		if (compensation == 0)
			proposal.setCompensation(flight.getTempFlightCost());

		proposal.setCost(pricingService.getCost(proposal));

		Proposal p = proposalService.proposed(proposal, "");
		return p;
	}



	public Proposal getFlightCancelProposal(String flightId, Date date, double compensation) {
		Flight flight = flightService.get(flightId);
        FlightCanceledProposal proposal = new FlightCanceledProposal(flight.getId(),
				flight.getAirlineId(),
				flight.getFlightNumber(),
				date);

		proposal.setCost(pricingService.getCost(proposal));
		proposal.setCompensation(compensation);

        Product delayProduct = productService.get("GenericFlightDelay");
        proposal.setProduct(delayProduct);

        return proposalService.proposed(proposal, null);
	}



	public List<FlightDelayProposal> getAllAcceptedFlightDelayProposal() {
		return flightDelayProposalRepository.findAcceptedState();
	}

	/**
	 *
	 */
	private ApplicationContext context;

	/**
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		final FlightProposalServiceImpl cprovider = new FlightProposalServiceImpl();
		return cprovider.getContext();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		context = applicationContext;
		proposalService = (IProposalService) context.getBean("proposalService");
	}

	/**
	 * @return the context
	 */
	ApplicationContext getContext() {
		return context;
	}

}
