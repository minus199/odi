package com.odi.service.impl;

import com.odi.model.Flight;
import com.odi.model.offer.MissedConnectionProposal;
import com.odi.model.offer.Proposal;
import com.odi.model.product.FCProductTemplate;
import com.odi.model.product.FlightDelayProductTemplate;
import com.odi.model.product.MissedConnectionProductTemplate;
import com.odi.model.product.Product;
import com.odi.repository.IProductRepository;
import com.odi.service.IFlightService;
import com.odi.service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("productsService")
public class ProductServiceImpl extends ServiceBaseImpl<Product> implements IProductService {
	
	@Autowired
	IFlightService flightService;
	
    @Override
    public List<Product> findByCompanyId(String companyId) {
        return ((IProductRepository)repository).findByCompanyId(companyId);
    }
    
    
	private String findRelevantFlightDelayProduct(List<Product> products, String companyId, Class templateType, Flight flight) {
		String productId = "GenericFlightDelay";
		
		for (Product p: products) {
			FlightDelayProductTemplate template;
			
			if (p.getCompanyId().toLowerCase().equals(companyId.toLowerCase()) == false)
				continue;
			
			if (p.getTemplate().getClass() != templateType)
				continue;
			
			template = (FlightDelayProductTemplate)p.getTemplate();
			if (template.getAirlines().isEmpty() == false && template.getAirlines().contains(flight.getAirlineId()) == false)
				continue;
			if (template.getDepartureAirport().isEmpty() == false && template.getDepartureAirport().contains(flight.getOriginAirportId()) == false)
				continue;
			if (template.getArrivalAirport().isEmpty() == false && template.getArrivalAirport().contains(flight.getDestinationAirportId()) == false)
				continue;

			productId = p.getId();
		}
		
		return productId;
	}



	private String findRelevantMissedConnectionProduct(List<Product> products, String companyId, Class templateType, Flight flight1, Flight flight2) {
		String productId = "GenericMissedConnection";

		for (Product p: products) {
			MissedConnectionProductTemplate template;
			
			template = (MissedConnectionProductTemplate)p.getTemplate();
//			if (template.getAirlines().isEmpty() == false && template.getAirlines().contains(flight.getAirlineId()) == false)
//				continue;
//			if (template.getDepartureAirport().isEmpty() == false && template.getDepartureAirport().contains(flight.getOriginAirportId()) == false)
//				continue;
//			if (template.getArrivalAirport().isEmpty() == false && template.getArrivalAirport().contains(flight.getDestinationAirportId()) == false)
//				continue;
//
//			productId = p.getId();
		}
		
		return productId;
	}

	private String findRelevantFlightCancelProduct(List<Product> products, String companyId, Class templateType, Flight flight) {
		String productId = "GenericFlightCancellation";

		for (Product p: products) {
			MissedConnectionProductTemplate template;
			
			template = (MissedConnectionProductTemplate)p.getTemplate();
			/*
			 * To implement,...
			 */
		}
		
		return productId;
	}



    public String findRelevantProductId(String companyId, Class templateType, Object...args) {
		List<Product> products = this.getAll();
		List<Product> relevantProducts = new ArrayList<Product>();
		
		for (Product p: products) {
			if (p.getCompanyId().toLowerCase().equals(companyId.toLowerCase()) == false)
				continue;
			
			if (p.getTemplate().getClass() != templateType)
				continue;
			
			relevantProducts.add(p);
		}
		
		if (templateType == FlightDelayProductTemplate.class)
			return findRelevantFlightDelayProduct(relevantProducts, companyId, templateType, (Flight)args[0]);
			
		if (templateType == FCProductTemplate.class)
			return findRelevantFlightCancelProduct(relevantProducts, companyId, templateType, (Flight)args[0]);
			
		if (templateType == MissedConnectionProductTemplate.class)
			return findRelevantMissedConnectionProduct(relevantProducts, companyId, templateType, (Flight)args[0], (Flight)args[1]);
			
		return null;
    }
}
