package com.odi.service.impl;

import com.odi.model.offer.Proposal;
import com.odi.model.offer.ProposalState;
import com.odi.model.offer.Proposals;
import com.odi.repository.IActivityLogRepository;
import com.odi.repository.IFlightCalceledProposalRespository;
import com.odi.repository.IFlightDelayProposalRepository;
import com.odi.repository.IProposalRepository;
import com.odi.service.IFlightProposalService;
import com.odi.service.IProposalService;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository("proposalService")
public class ProposalServiceImpl extends ServiceBaseImpl<Proposal> implements IProposalService {

	//@Autowired
	//IFlightProposalService flightProposalService;

	@Autowired
	protected IActivityLogRepository activityLogRepository;

	@Autowired
	IFlightDelayProposalRepository flightDelayProposalRepository;

	@Autowired
	IFlightCalceledProposalRespository flightCanceledProposalRepository;

	@Autowired
	IProposalRepository proposalRepository;

	@Autowired
	private IFlightProposalService flightProposalService;

	
	final int PROPOSAL_EXPERATION_IN_SECONDS = (15*60);



	private String getParameter(Map<String, String> parameters, String parameter, String defaultValue) {
		String value = parameters.get(parameter.toLowerCase());

		if (value == null)
			return defaultValue;

		return value;
	}

	public Proposal getProposal(String type, Map<String, String> parameters) {
		DateTimeFormatter dfDate = DateTimeFormat.forPattern("yyyy-MM-dd");

		switch (Proposals.toProposal(type)) {
			case FlightCancel:
				break;
			case FlightDelay: {
				String flightid = getParameter(parameters, "flightid", null);
				String companyid = getParameter(parameters, "companyid", null);
				String productid = getParameter(parameters, "productid", null);
				String contactinfo = getParameter(parameters, "contactinfo", null);
				double compensation = Double.valueOf(getParameter(parameters, "compensation", "0.0"));
				int hours = Integer.valueOf(getParameter(parameters, "hours", "4"));
				int minutes = Integer.valueOf(getParameter(parameters, "minutes", "0"));
				Date date = dfDate.parseDateTime(getParameter(parameters, "date", "1970-1-1")).toDate();
				System.out.println("I am in Flight Delay");

				return flightProposalService.getFlightDelayProposal(flightid, companyid, hours, minutes, date, compensation, contactinfo);
			}
				//break;
			case MissedConnection: {
				String fligh1tid = getParameter(parameters, "flight1id", null);
				String fligh2tid = getParameter(parameters, "flight2id", null);
				String companyid = getParameter(parameters, "companyid", null);
				String productid = getParameter(parameters, "productid", null);
				String contactinfo = getParameter(parameters, "contactinfo", null);
				double compensation = Double.valueOf(getParameter(parameters, "compensation", "0.0"));
				Date date = dfDate.parseDateTime(getParameter(parameters, "date", "1970-1-1")).toDate();
				System.out.println("I am in Missed connection");

				return flightProposalService.getMissedConnectionProposal(fligh1tid, fligh2tid, companyid, date, compensation, contactinfo);
			}
			case NoValue:
				break;
		}
		return null;
	}

	public Proposal proposed(Proposal proposal, String description) {
		proposal.makeProposed();

		addActivityLog("Proposed",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				proposal.toString()
		);

		return save(proposal);
	}

	public Proposal accept(String id, String description) {
		Proposal proposal = get(id);
		if (proposal == null)
			return null;
		
		proposal.makeAccept();

		addActivityLog("Accepted",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal accept(Proposal proposal, String description) {
		proposal.makeAccept();

		addActivityLog("Accepted",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal realize(Proposal proposal, String description) {
		proposal.makeRealize();

		addActivityLog("Relize",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal passed(Proposal proposal, String description) {
		proposal.makePassed();

		addActivityLog("Passed",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal realized(Proposal proposal, String description) {
		proposal.makeRealized();

		addActivityLog("Realized",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal notify(Proposal proposal, String description) {
		proposal.userNotify();

		addActivityLog("Notified",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	public Proposal expired(Proposal proposal, String description) {
		proposal.makeExpired();

		addActivityLog("Expired",
				proposal.getId(),
				proposal.getClass().getSimpleName(),
				description
		);

		return save(proposal);
	}

	@Scheduled(fixedRate = 60000*1)
	public void doExpired() {
		DateTime now = DateTime.now();
		List<Proposal> proposals = proposalRepository.findAll();

		for (Proposal p: proposals) {
			DateTime proposalTime = new DateTime(p.getLastModifiedDate());
			Seconds seconds = Seconds.secondsBetween(now, proposalTime);
			if (p.getState() == ProposalState.PROPOSED && Math.abs(seconds.getSeconds()) > PROPOSAL_EXPERATION_IN_SECONDS) {
				expired(p, "");
			}
		}

	}

	public List<Proposal> getAllAccepted() {
		return proposalRepository.findAcceptedState();
	}

	public List<Proposal> getAllProposed() {
		return proposalRepository.findProposedState();
	}

	@Override
	public List<Proposal> findByCompanyId(String companyId) {
		return ((IProposalRepository)repository).findByCompanyId(companyId);
	}

	@Override
	public Proposal get(String id) {
		return proposalRepository.findOne(id);
	}


}
