package com.odi.service.impl;

import com.odi.model.Flight;
import com.odi.model.event.Event;
import com.odi.model.event.EventStatus;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightLndedEvent;
import com.odi.model.offer.Proposal;
import com.odi.repository.IActivityLogRepository;
import com.odi.repository.IEventRepository;
import com.odi.service.IEventService;
import com.odi.service.IFlightService;
import com.odi.service.IProposalService;
import com.odi.service.utils.Mail;
import com.odi.service.utils.Sms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("eventService")
public class EventServiceImpl extends ServiceBaseImpl<Event> implements IEventService {
	
	@Autowired
	IFlightService flightService;

	@Autowired
	IProposalService proposalService;
	
	@Autowired
	IEventRepository eventRepository;


	@Autowired
	protected IActivityLogRepository activityLogRepository;

	void doEvent(Event event , Proposal proposal) {

		super.logger.debug(String.format("DoEvent::: %s", event));
		// Check if the proposal support the event
		if (proposal.isRelevant(event) == false)
			// Event is not supported by this proposal
			return;

		Mail mail = new Mail();
		Sms sms = new Sms();

		/// Check if it was delayed
		if (proposal.isIn(event) == true) {

			proposalService.notify(proposal,
					"The flight arrived late. User notified to (" + proposal.getUserContactInfo() + ")"
			);

			if (proposal.getUserContactInfo() != null && proposal.getUserContactInfo().isEmpty() == false) {
				if (isValidEmailAddress(proposal.getUserContactInfo())) {
					mail.send(proposal.getUserContactInfo(),
							"Auto claim process executed",
							String.format("%s.\nYour account will be credit by %d$.",
									proposal.claimMessage(),
									(int)proposal.getCompensation())
					);
				}
				if (isValidPhoneAddress(proposal.getUserContactInfo())) {
					sms.Send(proposal.getUserContactInfo(),
							String.format("%s\nYour account will be credit by %d$.\nFor more options: http://test.gluska.com/um.html",
									proposal.claimMessage(),
									(int)proposal.getCompensation())
					);
				}
			}
			proposalService.realize(proposal, "The flight arrived late. Claim accepted. Compensation delivered");


		} else {
			if (proposal.getUserContactInfo() != null && proposal.getUserContactInfo().isEmpty() == false) {
				if (isValidEmailAddress(proposal.getUserContactInfo()))
					mail.send("eyal@sizer.me",
							"Flight landed",
							String.format(
									"%s\nCase is close.\nclick here to see why: http://test.gluska.com/why\nThank you for using ODI services.\n\nThe ODI team",
									proposal.noClaimMessage()
							)
					);
				if (isValidPhoneAddress(proposal.getUserContactInfo()))
					sms.Send(proposal.getUserContactInfo(),
							String.format(
									"%s\nCase is close.\nclick here to see why: http://test.gluska.com/why\nThank you for using ODI services.\n\nThe ODI team",
									proposal.noClaimMessage()
							)
					);
			}
			proposalService.passed(
					proposal,
					"The flight arrived on time. No claim."
			);
		}

		event.setStatus(EventStatus.PROCESSES);
		save(event);
	}
	
	void doEvent(Event event , List<Proposal> proposals) {
		for (Proposal proposal: proposals)
			doEvent(event, proposal);
	}

	@Override
	@Scheduled(fixedRate = 15000)
	public void processAcceptedFlightProposals() {
		List<Proposal> proposals = proposalService.getAllAccepted();
		List<Event> events = findWaitingStatus();
		
		for (Event event: events)
			doEvent(event, proposals);
	}

	public void flightLanded(String flightId, Date scheduledTime, Date departureTime, Date arrivalTime) {
		Flight flight = flightService.get(flightId);
		if (flight == null) {
			throw new IllegalArgumentException(String.format("Not existing flight for Airline=%s and Flight=%d",
					flight.getAirlineId(),
					flight.getFlightNumber())
			);
		}
		
		FlightLndedEvent event = new FlightLndedEvent(flight.getAirlineId(),
				flight.getFlightNumber(),
				scheduledTime,
				departureTime,
				arrivalTime,
				flight.getOriginAirportId(),
				flight.getDestinationAirportId()
				);
		this.save(event);
	}

	public void flightCanceled(String flightId, Date scheduledTime) {
		Flight flight = flightService.get(flightId);
		if (flight == null) {
			throw new IllegalArgumentException(String.format("Not existing flight for Airline=%s and Flight=%d",
					flight.getAirlineId(),
					flight.getFlightNumber())
			);
		}
		
		FlightCanceledEvent event = new FlightCanceledEvent(flight.getAirlineId(), flight.getFlightNumber(), scheduledTime);
		this.save(event);
	}
	
	@Override
	public Event get(String id) {
		return eventRepository.findOne(id);
	}



	public List<Event> findWaitingStatus() {
		return eventRepository.findWaitingStatus();
	}



	public List<Event> findProccessedStatus() {
		return eventRepository.findProccessedStatus();
	}


}

