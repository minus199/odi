package com.odi.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.odi.model.AirLineCompany;
import com.odi.model.Airport;
import com.odi.model.City;
import com.odi.model.ConnectedFlight;
import com.odi.model.Flight;
import com.odi.model.FlightInstance;
import com.odi.repository.IAirLineCompanyRepository;
import com.odi.repository.IAirportRepository;
import com.odi.repository.ICityRepository;
import com.odi.repository.IFlightRepository;
import com.odi.service.IConnectedFlightService;
import com.odi.service.IFlightService;
import com.odi.service.model.AirportModel;
import com.odi.service.model.FlightInfo;
import com.odi.service.model.FlightOption;
import com.odi.service.model.FlightPlan;
import com.odi.service.model.TimeDiff;

@Repository("flightService")
public class FlightServiceImpl extends ServiceBaseImpl<Flight> implements IFlightService {

	@Autowired
	private IFlightRepository flightRepository;

	@Autowired
	private IAirLineCompanyRepository airlineRepository;
	
	@Autowired
	private IAirportRepository airportRepository;
	
	@Autowired
	private ICityRepository cityRepository;
	
	@Autowired
	private IConnectedFlightService connectedFlightService;

	
	public Flight saveFlight(Flight flight) {
		return save(flight);
	}


	public List<Flight> findByAirlineIdAndFlightNumber(String airlineId, int flightNumber) {
		return ((IFlightRepository)repository).findByAirlineIdAndFlightNumber(airlineId, flightNumber);
	}

	public Flight saveFlight(String flightTime, String airlineId, int flightNumber, String originAirportId,
							 String destinationAirportId,
								DayOfWeek day, int cost) {
		Flight flight = new Flight("00:05", flightTime, airlineId, flightNumber, originAirportId, destinationAirportId, day, cost);
		
		return saveFlight(flight);
	}

/*
	public Flight getFlight(String airlineId, int flightNumber) {
		Flight flight = flightRepository.findByAirlineIdAndFlightNumber(airlineId, flightNumber);
		
		return flight;
	}
*/


	public FlightInfo flight2FlightInfo(Flight flight) 
	{
		FlightInfo fi = new FlightInfo();
		
		AirLineCompany airline;
		try {
			airline = airlineRepository.getOne(flight.getAirlineId());
			fi.setAirline(airline == null ? String.format("Undef(%s)", flight.getAirlineId()) : airline.getName());
			
		} catch (Exception e) {
			fi.setAirline(String.format("Undef(%s)", flight.getAirlineId()));
		}
		
		Airport origin;
		try {
			origin = airportRepository.getOne(flight.getOriginAirportId());
			fi.setOriginAirport(origin == null ? String.format("Undef(%s)", flight.getOriginAirportId()) : origin.getName());
			
			if(origin != null)
			{
				City city = cityRepository.getOne(origin.getCityId());
				String country = city == null?null:city.getCountryId();

				fi.setSrcAirport(new AirportModel(origin.getName(),origin.getCode(),origin.getCityId(),country));
			}
			
		} catch (Exception e) {
			fi.setOriginAirport(String.format("Undef(%s)", flight.getOriginAirportId()));
		}
		
		Airport dest;
		try {
			dest = airportRepository.getOne(flight.getDestinationAirportId());
			fi.setDestinationAirport(dest == null ? String.format("Undef(%s)", flight.getDestinationAirportId()) : dest.getName());
			
			if(dest != null)
			{
				City city = cityRepository.getOne(dest.getCityId());
				String country = city == null?null:city.getCountryId();
				fi.setDestAirport(new AirportModel(dest.getName(),dest.getCode(),dest.getCityId(),country));
			}
			
		} catch (Exception e) {
			fi.setDestinationAirport(String.format("Undef(%s)", flight.getDestinationAirportId()));
		}
		

		fi.setId(flight.getId());
		fi.setFlightNumber(flight.getFlightNumber());
		fi.setAirlineCode(flight.getAirlineId());
		fi.setFlightTime(flight.getFlightTime());
		fi.setFlightArrivalTime(flight.getFlightLanding());
		fi.setDay(flight.getDayOfWeek());
		fi.setFlightDuration(flight.getFlightDuration());
		return fi;
	}

	//@Override
	public Flight flightInfo2Flight(FlightInfo flightInfo) {
		Flight flight = new Flight();
		String l = "";
		
		if (flightInfo.getAirline().toLowerCase().startsWith("undef"))
			flight.setAirlineId(flightInfo.getAirline().toLowerCase().replace("undec(", "").replaceAll("\\)", ""));
		else
			flight.setAirlineId(flightInfo.getAirline());

		if (flightInfo.getDestinationAirport().toLowerCase().startsWith("undef"))
			flight.setDestinationAirportId(flightInfo.getDestinationAirport().toLowerCase().replace("undec(", "").replaceAll("\\)", ""));
		else
			flight.setDestinationAirportId(flightInfo.getDestinationAirport());

		if (flightInfo.getOriginAirport().toLowerCase().startsWith("undef"))
			flight.setOriginAirportId(flightInfo.getOriginAirport().toLowerCase().replace("undec(", "").replaceAll("\\)", ""));
		else
			flight.setOriginAirportId(flightInfo.getOriginAirport());

		flight.setFlightTime(flightInfo.getFlightTime());
		flight.setFlightNumber(flightInfo.getFlightNumber());

		return flight;
	}


	@Override
	public FlightPlan searchFlights(String originAirportId,
			String destAirportId, String departingDate, boolean isOneWay,
			String returnDate) 
	{
		Map<String,Flight> originFlights = _filterFlightsByDate(departingDate,((IFlightRepository)repository).findByOriginDest(originAirportId, destAirportId));
		Map<String,Flight> destFlights = null;
		
		
		if(!isOneWay)
		{
			destFlights = _filterFlightsByDate(returnDate,((IFlightRepository)repository).findByOriginDest(destAirportId, originAirportId));
		}
		
		return new FlightPlan(originAirportId, destAirportId, departingDate, isOneWay, returnDate, originFlights, destFlights);
	}


	private Map<String,Flight> _filterFlightsByDate(String filterDate, List<Flight> flights) 
	{
		Map<String,Flight> filterd = new LinkedHashMap<String,Flight>();
		
		final SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy");
		final SimpleDateFormat keyTimeFormat = new SimpleDateFormat("EEE dd MMM",Locale.US);

		try 
		{
			Date date = timeFormat.parse(filterDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			
			DayOfWeek dayOfWeek = DayOfWeek.of(c.get(Calendar.DAY_OF_WEEK));
			DayOfWeek dayAfter = dayOfWeek.plus(1); 
			DayOfWeek dayBefore = dayOfWeek.minus(1);
			String todayKey = keyTimeFormat.format(c.getTime());
			c.add(Calendar.DAY_OF_MONTH, 1);
			String nextDayKey = keyTimeFormat.format(c.getTime());
			c.add(Calendar.DAY_OF_MONTH, -2);
			String prevDayKey = keyTimeFormat.format(c.getTime());
			
			filterd.put(prevDayKey, null);
			filterd.put(todayKey, null);
			filterd.put(nextDayKey, null);
			
			
			
			for(Flight f : flights)
			{
				if(dayOfWeek.equals(f.getDayOfWeek()))
				{
					if(filterd.get(todayKey) == null || filterd.get(todayKey).getTempFlightCost() > f.getTempFlightCost())
						filterd.put(todayKey,f);
				}
				else if(dayAfter.equals(f.getDayOfWeek()))
				{
					if(filterd.get(nextDayKey) == null || filterd.get(nextDayKey).getTempFlightCost() > f.getTempFlightCost())
						filterd.put(nextDayKey,f);
				}
				else if(dayBefore.equals(f.getDayOfWeek()))
				{
					if(filterd.get(prevDayKey) == null || filterd.get(prevDayKey).getTempFlightCost() > f.getTempFlightCost())
						filterd.put(prevDayKey,f);
				}

				if(f.getInstances() != null && f.getInstances().size() > 0)
				{
					for(FlightInstance inst : f.getInstances())
					{
						if(dayOfWeek.equals(inst.getDay()))
						{
							if(filterd.get(todayKey) == null || filterd.get(todayKey).getTempFlightCost() > inst.getCost())
								filterd.put(todayKey,f);
						}
						else if(dayAfter.equals(inst.getDay()))
						{
							if(filterd.get(nextDayKey) == null || filterd.get(nextDayKey).getTempFlightCost() > inst.getCost())
								filterd.put(nextDayKey,f);
						}
						else if(dayBefore.equals(inst.getDay()))
						{
							if(filterd.get(prevDayKey) == null || filterd.get(prevDayKey).getTempFlightCost() > inst.getCost())
								filterd.put(prevDayKey,f);
						}
					}
				}
			}
			
		} catch (ParseException e) {
			
		}
		
		return filterd;
	}

	@Override
	public int loadFlights(String flightsFile)
	{
		return loadFlights(new File(flightsFile));
	}

	@Override
	public int loadFlights(File flightsFile) 
	{
		int linesCount = 0;
		List<Flight> flights = new ArrayList<Flight>();
		
		try {
			FileInputStream fis = new FileInputStream(flightsFile);
			 
			//Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
 
			String line = null;
			while ((line = br.readLine()) != null) {
				linesCount++;
				String[] parts = line.split(",");
				try {
					Flight flight = new Flight(
							parts[0].trim(), //flightTime,
							parts[1].trim(), //flightLanding,
							parts[2].trim().toUpperCase(), //airlineId,
							Integer.valueOf(parts[3].trim()), //flightNumber,
							parts[4].trim().toUpperCase(), //originAirportId,
							parts[5].trim().toUpperCase(), //destinationAirportId,
							DayOfWeek.valueOf(parts[6].trim().toUpperCase()), //day,
							Integer.valueOf(parts[7].trim())  //cost
							);
					flight.setId(String.format("%s@%d@%s", flight.getAirlineId(), flight.getFlightNumber(), flight.getDayOfWeek().toString()));
					// Flight Length
					if (parts.length > 8) {
						String[] timeTokens = parts[8].split(":");
						flight.setFlightDurationHours(Integer.valueOf(timeTokens[0]));
						flight.setFlightDurationHours(Integer.valueOf(timeTokens[1]));
					}
					flights.add(flight);
					//this.save(flight);
				} catch (Exception e) {
					
				}
			}
 
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.save(flights);
		return linesCount;
	}
	
	public List<FlightOption> getFlightOptions(String originAirport, String destinationAirport, String departureDate, String returnDate) {
		List<FlightOption> list = new ArrayList<FlightOption>();
		List<ConnectedFlight> connections = connectedFlightService.getFlights(originAirport, destinationAirport, departureDate);

		for (ConnectedFlight c: connections) {
			FlightOption flight = new FlightOption();
			flight.setGoFirst(flight2FlightInfo(c.getFirstFlight()));
			flight.setGoSecond(flight2FlightInfo(c.getSecondFlight()));

			flight.setGoLayover(TimeDiff.createFromDates(c.getFirstFlight().getFlightLanding(),c.getSecondFlight().getFlightTime()));
			
			flight.setGoCost(c.getFirstFlight().getTempFlightCost() + c.getSecondFlight().getTempFlightCost());

			Date startTime = c.getFirstFlight().getFlightTime();
			Date endTime   = c.getSecondFlight().getFlightLanding();
			
			flight.setTripGoDuration(TimeDiff.createFromDates(startTime, endTime));
			
			flight.setGoConnected(true);
			flight.setOneway(true);
			flight.setTwoways(false);

			list.add(flight);
		}

		if (returnDate == null) {
			return list;
		}

		/*
		 * Do Return
		 */

		connections = connectedFlightService.getFlights(destinationAirport, originAirport, returnDate);

		int currentIndex = 0;
		ConnectedFlight lastconn = null;
		
		for (ConnectedFlight c: connections) {
			if (list.size() <= currentIndex)
				break;

			// Update flight with return op[tion

			list.get(currentIndex).setReturnFirst(flight2FlightInfo(c.getFirstFlight()));
			list.get(currentIndex).setReturnSecond(flight2FlightInfo(c.getSecondFlight()));
			
			list.get(currentIndex).setReturnLayover(TimeDiff.createFromDates(c.getFirstFlight().getFlightLanding(),c.getSecondFlight().getFlightTime()));
			
			list.get(currentIndex).setReturnCost(c.getFirstFlight().getTempFlightCost() + c.getSecondFlight().getTempFlightCost());

			
			Date startTime = c.getFirstFlight().getFlightTime();
			Date endTime   = c.getSecondFlight().getFlightLanding();
			
			list.get(currentIndex).setTripReturnDuration(TimeDiff.createFromDates(startTime, endTime));
			
			list.get(currentIndex).setReturnConnected(true);
			list.get(currentIndex).setOneway(false);
			list.get(currentIndex).setTwoways(true);

			currentIndex++;
			lastconn = c;
		}
		
		if(currentIndex < list.size() && lastconn != null)
		{
			ConnectedFlight c = lastconn;
			
			for(;currentIndex<list.size();currentIndex++)
			{
				list.get(currentIndex).setReturnFirst(flight2FlightInfo(c.getFirstFlight()));
				list.get(currentIndex).setReturnSecond(flight2FlightInfo(c.getSecondFlight()));
				
				list.get(currentIndex).setReturnLayover(TimeDiff.createFromDates(c.getFirstFlight().getFlightLanding(),c.getSecondFlight().getFlightTime()));
				
				list.get(currentIndex).setReturnCost(c.getFirstFlight().getTempFlightCost() + c.getSecondFlight().getTempFlightCost());

				
				Date startTime = c.getFirstFlight().getFlightTime();
				Date endTime   = c.getSecondFlight().getFlightLanding();
				
				list.get(currentIndex).setTripReturnDuration(TimeDiff.createFromDates(startTime, endTime));
				
				list.get(currentIndex).setReturnConnected(true);
				list.get(currentIndex).setOneway(false);
				list.get(currentIndex).setTwoways(true);

				lastconn = c;
			}
		}

		return list;
	}

//	@Override
//	public Flight getFlight(String id) {
//		return flightRepository.getOne(id);
//	}

}
