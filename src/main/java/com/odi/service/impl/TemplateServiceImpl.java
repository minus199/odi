package com.odi.service.impl;

import com.odi.model.product.Template;
import com.odi.service.ITemplateService;
import org.springframework.stereotype.Repository;

@Repository("templateService")
public class TemplateServiceImpl extends ServiceBaseImpl<Template> implements ITemplateService {
}
