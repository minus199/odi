package com.odi.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.odi.model.AirLineCompany;
import com.odi.model.Airport;
import com.odi.service.IAirLineCompanyService;
import com.odi.service.IAirportService;
import com.odi.service.impl.ServiceBaseImpl;

@Repository("airportService")
public class AirportServiceImpl extends ServiceBaseImpl<Airport> implements IAirportService {

}
