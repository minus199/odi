package com.odi.service.impl;

import com.google.gson.Gson;
import com.odi.model.Flight;
import com.odi.model.event.Event;
import com.odi.model.offer.FlightCanceledProposal;
import com.odi.model.offer.FlightDelayProposal;
import com.odi.model.offer.MissedConnectionProposal;
import com.odi.model.offer.Proposal;
import com.odi.model.product.FlightDelayProductTemplate;
import com.odi.model.product.PaymentMethod;
import com.odi.model.product.Product;
import com.odi.service.IConnectedFlightService;
import com.odi.service.IFlightService;
import com.odi.service.IPricingService;

import com.odi.service.model.PricingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by eyal on 24/03/2017.
 */
@Repository("pricingService")
public class PricingService implements IPricingService {
    protected final Logger logger = LoggerFactory.getLogger("service");

    @Value("${odi.pricing.url:http://localhost:5050/flight_price}")
    private String pricingurl;

	@Autowired
	IFlightService flightService;

	@Autowired
	IConnectedFlightService connectedFlightService;



     private PricingResult executeHTTPRequest(String requestURL) {
		try {
			String USER_AGENT = "Mozilla/5.0";
			String url = requestURL;

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			// optional default is GET
			con.setRequestMethod("GET");
	
			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
	
			int responseCode = con.getResponseCode();
			//System.out.println("\nSending 'GET' request to URL : " + url);
			//System.out.println("Response Code : " + responseCode);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
            Gson gson = new Gson();
            PricingResult price = gson.fromJson(response.toString(), PricingResult.class);

            //print result
            //System.out.println(price.toString());

            return price;
		} catch (Exception e) {
			System.out.println(e.toString());
			return null;
		}
    }

    public double getPricing(Product product) {
        return 0;
    }


    /*
     * At the moment, pricing works for:
     *     30,60,90,120,180 minutes
     */
    private int getDelayInMinutes(int delay) {
    	if (delay >= 180)
    		return 180;
    	if (delay >= 120)
    		return 120;
    	if (delay >= 90)
    		return 90;
    	if (delay >= 60)
    		return 60;
    	if (delay >= 30)
    		return 30;
    	return 0;
    }
    
    public double getFlightDelayPrice(Flight flight, Date date, double amount, int delay) {
        //SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        String pricingDate = dateParser.format(date);;

        delay = getDelayInMinutes(delay);
        
        String requestUrl = String.format(
                "%s?dep_apt=%s&arr_apt=%s&airline=%s&flight_number=%d&amount=%d&threshold=%d&date=%s",
                pricingurl,
                flight.getOriginAirportId(),
                flight.getDestinationAirportId(),
                flight.getAirlineId(),
                flight.getFlightNumber(),
                (int)amount,
                delay,
                pricingDate
        );

        PricingResult result = executeHTTPRequest(requestUrl);
        logger.info(result == null? "No Pricing" : result.toString());

        return result == null ? 0 : result.getPrice();
    }
    
	public Double getCost(Proposal proposal) {
		double price = 0;
		
		if (proposal.getProduct().getPaymentMethod() == PaymentMethod.Fixed)
			return proposal.getProduct().getPaymentAmount();
		
		if (proposal.getProduct().getPaymentMethod() == PaymentMethod.Included)
			return 0.0;
		
		if (proposal.getProduct().getPaymentMethod() == PaymentMethod.PercentageOfTotal)
			return proposal.getCost() * proposal.getProduct().getPaymentAmount() / 100;
		
		
		if (proposal.getClass() == FlightDelayProposal.class) {
			Flight flight = flightService.get( ((FlightDelayProposal)proposal).getFlightId() );
			if (flight == null)
				return 10.0;
			price = this.getFlightDelayPrice(flight,
					((FlightDelayProposal) proposal).getFlightDate(),
					flight.getTempFlightCost(),
					((FlightDelayProposal) proposal).getDelayHours() * 60 + ((FlightDelayProposal) proposal).getDelayMinutes()
					);
			if (price > 0)
				return price;
			return ((double)flight.getTempFlightCost()) / 100 * 4;
		}

		if (proposal.getClass() == FlightCanceledProposal.class) {
			Flight flight = flightService.get( ((FlightCanceledProposal)proposal).getFlightId() );
			if (flight == null)
				return 5.0;
			return ((double)flight.getTempFlightCost()) / 100 * 2;
		}


		if (proposal.getClass() == MissedConnectionProposal.class) {
			Flight flight = ((MissedConnectionProposal)proposal).getFirstFlight();
			if (flight == null)
				return 10.0;
			/*
			 * This part should be replaced with something that calculates the risk in each specific airport
			 */
			int minutes = connectedFlightService.minutesBetweenFlights(
					((MissedConnectionProposal)proposal).getFirstFlight(),
					((MissedConnectionProposal)proposal).getSecondFlight());
			int minutesForPricing = 0;
			if (minutes > 60)
				minutesForPricing = minutes / 30 * 30 - 30;
			else
				minutesForPricing = 30;
			
			price = this.getFlightDelayPrice(flight,
					((MissedConnectionProposal) proposal).getTravelDate(),
					flight.getTempFlightCost(),
					minutesForPricing
			);
			if (price > 0)
				return price;
			return ((double)proposal.getCompensation()) / 100 * 4;
		}


		return 8.0;
    }

}
