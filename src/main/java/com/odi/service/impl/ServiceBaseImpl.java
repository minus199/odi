package com.odi.service.impl;

import com.odi.model.ActivityLog;
import com.odi.model.EntityBase;
import com.odi.repository.IActivityLogRepository;
import com.odi.service.IServiceBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.reflect.Field;
import java.util.List;

public class ServiceBaseImpl<T> implements IServiceBase<T> {
	protected final Logger logger = LoggerFactory.getLogger("service");

	@Autowired
	protected JpaRepository<T, String> repository;

	@Autowired
	protected IActivityLogRepository activityLogRepository;



	protected void addActivityLog(String activity, String relatedEntityId, String relatedEntityName, String description) {
		ActivityLog activityLog = new ActivityLog(activity, relatedEntityId, relatedEntityName, description);
		activityLogRepository.save(activityLog);
	}

	protected boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	protected boolean isValidPhoneAddress(String phone) {
		if (phone.startsWith("00"))
			phone = "+" + phone.substring(2);
		String pattern = "^\\+[0-9]{1,3}\\.[0-9]{4,14}(?:x.+)?$";

		pattern = "^\\+(?:[0-9] ?){6,14}[0-9]$";

		java.util.regex.Pattern p1 = java.util.regex.Pattern.compile(pattern);

		java.util.regex.Matcher m1 = p1.matcher(phone);

		return m1.matches();
	}

	@Override
	public void delete(String id) {
		addActivityLog("Delete",
		               id,
		               "", // Class Name
		               ""
		              );
		repository.delete(id);
	}

	@Override
	public T get(String id) {
		return repository.getOne(id);
	}

	final protected Class getSuperClass(Class c) {
		if (c.getSimpleName().contains("EntityBase") == true)
			return c;
		if (c.getSuperclass() != null && c.getSuperclass() != Object.class)
			return getSuperClass(c.getSuperclass());
		return c;
	}

	@Override
	public T save(T entity) {
		Field field = null;
		Object value = null;
		try {
			Class c = getSuperClass(entity.getClass());
			field = c.getDeclaredField("id");
			field.setAccessible(true);
			value = field.get(entity);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String id = (String) value;
		addActivityLog("Save",
		               id, //((EntityBase)entity).getId(),
		               entity.getClass().getSimpleName(),
		               ""
		              );
		return repository.save(entity);
	}

	@Override
	public List<T> save(List<T> list) {
		return repository.save(list);
	}

	@Override
	public Page<T> getAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public void delete(T entity) {
		addActivityLog("Delete",
		               ((EntityBase) entity).getId(),
		               entity.getClass().getSimpleName(),
		               ""
		              );
		repository.delete(entity);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public void deleteInBatch(List<T> entities) {
		repository.deleteInBatch(entities);
	}

	@Override
	public List<T> getAll() {
		return repository.findAll();
	}
}
