package com.odi.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.odi.model.AirLineCompany;
import com.odi.service.IAirLineCompanyService;
import com.odi.service.impl.ServiceBaseImpl;

@Repository("airLineCompanyService")
public class AirLineCompanyServiceImpl extends ServiceBaseImpl<AirLineCompany> implements IAirLineCompanyService {

}
