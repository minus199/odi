package com.odi.service.impl;

import com.odi.model.Company;
import com.odi.repository.ICompanyRepository;
import com.odi.service.ICompanyService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("companyService")
public class CompanyServiceImpl extends ServiceBaseImpl<Company> implements ICompanyService {

	@Override
	public Optional<Company> findOneByName(String name) {
		return ((ICompanyRepository) repository).findByName(name);
	}
}
