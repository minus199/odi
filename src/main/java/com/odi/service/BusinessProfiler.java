package com.odi.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Aspect
public class BusinessProfiler {

    @Around("execution(* com.odi.service.IConnectedFlightService.*(..))")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        long start = System.currentTimeMillis();

        //System.out.println("Going to call: " + method.getName());
        Object output = pjp.proceed();
        //System.out.println("Method execution completed.");

        long elapsedTime = System.currentTimeMillis() - start;
        System.out.println(String.format("Method %s :: %d milliseconds", method.getName(), elapsedTime));
        return output;
    }

    @Around("execution(* com.odi.service.IEventService.*(..))")
    public Object event_profile(ProceedingJoinPoint pjp) throws Throwable {
        return profile(pjp);
    }

}
