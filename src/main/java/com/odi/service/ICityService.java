package com.odi.service;

import com.odi.model.City;

public interface ICityService extends IServiceBase<City> {
	City findByUniquenameAndCountryId(String uniqueName, String countryId);
}
