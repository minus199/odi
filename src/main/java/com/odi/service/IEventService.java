package com.odi.service;

import com.odi.model.event.Event;

import java.util.Date;
import java.util.List;

public interface IEventService  extends IServiceBase<Event> {
	
	public void flightLanded(String flighId, Date scheduledTime, Date departureTime, Date arrivalTime);
	public void flightCanceled(String flightId, Date scheduledTime);
	public List<Event> findWaitingStatus();
	public List<Event> findProccessedStatus();
	void processAcceptedFlightProposals();
}
