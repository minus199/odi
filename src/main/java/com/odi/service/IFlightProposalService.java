package com.odi.service;

import com.odi.model.offer.FlightDelayProposal;
import com.odi.model.offer.Proposal;

import java.util.Date;
import java.util.List;

public interface IFlightProposalService  {
//public interface IFlightProposalService extends IServiceBase<FlightDelayProposal> {

    public Proposal getMissedConnectionProposal(String fligh1Id, String flight2Id,
			String companyId, Date date, double compensation, String userContactInfo);
    
    public Proposal getFlightDelayProposal(String flightId, String companyId, int delayHours, int delayMinutes, Date date, double compensation, String userContactInfo);
    public Proposal getFlightCancelProposal(String flightId, Date date, double compensation);
    public Proposal acceptFlightProposal(String proposalID);
    public Proposal accept(String proposalID, String userContactInfo);
    public List<FlightDelayProposal> getAllAcceptedFlightDelayProposal();
}
