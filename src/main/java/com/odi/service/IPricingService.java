package com.odi.service;

import com.odi.model.Flight;
import com.odi.model.event.Event;
import com.odi.model.offer.Proposal;
import com.odi.model.product.Product;

import java.util.Date;

public interface IPricingService {
	
	double getPricing(Product product);
	double getFlightDelayPrice(Flight flight, Date date, double amount, int delay);
	public Double getCost(Proposal proposal);

}
