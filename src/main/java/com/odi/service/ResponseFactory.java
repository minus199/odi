package com.odi.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by minus on 4/5/17.
 */
@Service
public class ResponseFactory {
	public ResponseEntity<OdiResponse> make() {
		return make("");
	}

	public ResponseEntity<OdiResponse> make(String message) {
		return make(HttpStatus.OK, message);
	}

	public ResponseEntity<OdiResponse> make(HttpStatus httpStatus, String message) {
		return make("", httpStatus, message);
	}

	public ResponseEntity<OdiResponse> make(Object data, HttpStatus httpStatus, String message) {
		OdiResponse response = new OdiResponse(httpStatus, message);
		response.setData(data);
		return new ResponseEntity<>(response, httpStatus);
	}

	public ResponseEntity<OdiResponse> make(Optional<Throwable> throwableOptional, HttpStatus httpStatus) {
		httpStatus = httpStatus == null ? HttpStatus.INTERNAL_SERVER_ERROR : httpStatus;
		OdiResponse odiResponse = new OdiResponse(httpStatus, throwableOptional.get().getMessage());
		return new ResponseEntity<>(odiResponse, httpStatus);
	}
}
