package com.odi.service;

import com.odi.model.user.User;

import java.util.Optional;

/**
 * Created by minus on 4/4/17.
 */
public interface IUserService extends IServiceBase<User> {
	Optional<User> getByName(String name);
}
