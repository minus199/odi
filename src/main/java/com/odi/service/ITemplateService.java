package com.odi.service;

import com.odi.model.product.Template;


public interface ITemplateService extends IServiceBase<Template> {
}
