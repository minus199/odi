package com.odi.service.model;

import com.odi.model.offer.ProposalState;

public class FlightProposalParams {

	private String id;
	private String flight;
    private String airline;
    private String date;
    private int delayHours = 4;
    private int delayMinutes = 0;
	private double cost = 300;
	private String proposalID;
	private ProposalState state;
	private String userMobile = "+972525766810";
	private String description;
	private String flightId;

	public String getProposalID() {
		return proposalID;
	}

	public void setProposalID(String proposalID) {
		this.proposalID = proposalID;
	}

	public double getCost() {

		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}

	public int getDelayHours() {
		return delayHours;
	}

	public void setDelayHours(int delayHours) {
		this.delayHours = delayHours;
	}

	public int getDelayMinutes() {
		return delayMinutes;
	}

	public void setDelayMinutes(int delayMinutes) {
		this.delayMinutes = delayMinutes;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public ProposalState getState() {
		return state;
	}

	public void setState(ProposalState state) {
		this.state = state;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
}
