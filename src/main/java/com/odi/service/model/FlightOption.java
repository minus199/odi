package com.odi.service.model;

import com.odi.model.Airport;

public class FlightOption 
{
	private Boolean oneway = true;
	private Boolean twoways = false;

	private Airport originAirport;
	private Airport destinationAirport;

	private boolean goConnected = true;
	private FlightInfo goFirst;
	private FlightInfo goSecond;
	
	private int goCost = 0;

	private boolean returnConnected = true;
	private FlightInfo returnFirst;
	private FlightInfo returnSecond;
	private int returnCost = 0;

	public Boolean getOneway() {
		return oneway;
	}

	public void setOneway(Boolean oneway) {
		this.oneway = oneway;
	}

	public Boolean getTwoways() {
		return twoways;
	}

	public void setTwoways(Boolean twoways) {
		this.twoways = twoways;
	}


	public Airport getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(Airport originAirport) {
		this.originAirport = originAirport;
	}

	public Airport getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(Airport destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public FlightInfo getGoFirst() {
		return goFirst;
	}

	public void setGoFirst(FlightInfo goFirst) {
		this.goFirst = goFirst;
	}

	public FlightInfo getGoSecond() {
		return goSecond;
	}

	public void setGoSecond(FlightInfo goSecond) {
		this.goSecond = goSecond;
	}

	public FlightInfo getReturnFirst() {
		return returnFirst;
	}

	public void setReturnFirst(FlightInfo returnFirst) {
		this.returnFirst = returnFirst;
	}

	public FlightInfo getReturnSecond() {
		return returnSecond;
	}

	public void setReturnSecond(FlightInfo returnSecond) {
		this.returnSecond = returnSecond;
	}

	public int getGoCost() {
		return goCost;
	}

	public void setGoCost(int goCost) {
		this.goCost = goCost;
	}

	public int getReturnCost() {
		return returnCost;
	}

	public void setReturnCost(int returnCost) {
		this.returnCost = returnCost;
	}

	public boolean isGoConnected() {
		return goConnected;
	}

	public void setGoConnected(boolean goConnected) {
		this.goConnected = goConnected;
	}

	public boolean isReturnConnected() {
		return returnConnected;
	}

	public void setReturnConnected(boolean returnConnected) {
		this.returnConnected = returnConnected;
	}

	private TimeDiff tripGoDuration = null;
	private TimeDiff tripReturnDuration = null;
	private TimeDiff goLayover = null;
	private TimeDiff returnLayover = null;

	public TimeDiff getTripGoDuration() {
		return tripGoDuration;
	}

	public void setTripGoDuration(TimeDiff tripGoDuration) {
		this.tripGoDuration = tripGoDuration;
	}

	public TimeDiff getTripReturnDuration() {
		return tripReturnDuration;
	}

	public void setTripReturnDuration(TimeDiff tripReturnDuration) {
		this.tripReturnDuration = tripReturnDuration;
	}

	public TimeDiff getGoLayover() {
		return goLayover;
	}

	public void setGoLayover(TimeDiff goLayover) {
		this.goLayover = goLayover;
	}

	public TimeDiff getReturnLayover() {
		return returnLayover;
	}

	public void setReturnLayover(TimeDiff returnLayover) {
		this.returnLayover = returnLayover;
	}

	
}
