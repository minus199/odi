package com.odi.service.model;

public class OtaFlight {
    private String id;
    private String proposalId;
    private String airline;
    private String departureAirport;
    private String desctinationAirport;
    private int flightNumber;
    private String flightTime;
    private String dateOfFlight = "2017-2-20";
    private String name = "John Daw";
    private String phone = "+972525766810";
    private String email = "eyal@gluska.com";
    private String oda = null;

    private int cost;
    private double odicost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getDesctinationAirport() {
        return desctinationAirport;
    }

    public void setDesctinationAirport(String desctinationAirport) {
        this.desctinationAirport = desctinationAirport;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public String getDateOfFlight() {
        return dateOfFlight;
    }

    public void setDateOfFlight(String dateOfFlight) {
        this.dateOfFlight = dateOfFlight;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getProposalId() {
        return proposalId;
    }

    public void setProposalId(String proposalId) {
        this.proposalId = proposalId;
    }

	public double getOdicost() {
		return odicost;
	}

	public void setOdicost(double odicost) {
		this.odicost = odicost;
	}

    public String getOda() {
        return oda;
    }

    public void setOda(String oda) {
        this.oda = oda;
    }
}
