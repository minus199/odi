package com.odi.service.model;

/**
 * Created by eyal on 24/03/2017.
 */
public class PricingResult {
    private double price;
    private String text;

    public double getPrice() {
        return ((double)Math.round(price * 10)) / 10.0;
    }

    public void setPrice(double price) {
        // make it one digits...
        this.price = price;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "PricingResult{" +
                "price=" + price +
                ", text='" + text + '\'' +
                '}';
    }
}
