package com.odi.service.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class TimeDiff 
{
	private int hours = 0;
	private int minutes = 0;
	private int seconds = 0;
	
	public int getHours() {
		return hours;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	public int getMinutes() {
		return minutes;
	}
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	public int getSeconds() {
		return seconds;
	}
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
	
	private void calcTotalDuration(Date startTime, Date endTime) {
		long timeDiffSeconds  = getDateDiff(startTime, endTime, TimeUnit.SECONDS);
		
		int timeDiffMinutes = (int) (timeDiffSeconds / 60);
		
		seconds = (int) (timeDiffSeconds % 60);
		
		int hourDiff = (int) (timeDiffMinutes / 60);

		hours = hourDiff;
		minutes = (timeDiffMinutes % 60);;
	}
	
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	public static TimeDiff createFromDates(Date startTime, Date endTime)
	{
		TimeDiff diff = new TimeDiff();
		diff.calcTotalDuration(startTime, endTime);
		
		return diff;
	}
	
	public static TimeDiff fromSingleDate(Date date)
	{
		TimeDiff diff = new TimeDiff();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);  
		
		diff.hours = calendar.get(Calendar.HOUR_OF_DAY);
		diff.minutes = calendar.get(Calendar.MINUTE);
		diff.seconds = calendar.get(Calendar.SECOND); 
		
		return diff;
	}
	
	
}
