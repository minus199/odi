package com.odi.service.model;

public class AirportModel 
{
	private String city;
	private String code = null;
	private String name = null;
	private String country = null;
	
	public AirportModel(String name,String code,String city, String country) 
	{
		this.city = city;
		this.country = country;
		this.name = name;
		this.code = code;
	}
	
	public AirportModel() {
		// TODO Auto-generated constructor stub
	}
	
	public String getCityId() {
		return city;
	}
	public void setCityId(String cityId) {
		this.city = cityId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
