package com.odi.service.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.odi.model.Airport;
import com.odi.model.City;
import com.odi.model.Country;

public class ViewGlobalData 
{
	private HashMap<String, List<Airport>> airports = null;
	private HashMap<String, List<City>> cities = null;
	private HashMap<String, Country> countries = null;
	
	
	public HashMap<String, List<Airport>> getAirports() {
		return airports;
	}

	public void setAirports(HashMap<String, List<Airport>> airports) {
		this.airports = airports;
	}

	public HashMap<String, List<City>> getCities() {
		return cities;
	}

	public void setCities(HashMap<String, List<City>> cities) {
		this.cities = cities;
	}

	public HashMap<String, Country> getCountries() {
		return countries;
	}

	public void setCountries(HashMap<String, Country> countries) {
		this.countries = countries;
	}

	public ViewGlobalData() {
		// TODO Auto-generated constructor stub
	}
	
	public ViewGlobalData(List<Country> countries,List<City> cities,List<Airport> airports) 
	{
		this.init_all(countries,cities,airports);
	}

	private void init_all(List<Country> counts, List<City> cits,
			List<Airport> airp) 
	{
		this.cities  = new HashMap<>();
		this.countries = new HashMap<>();
		this.airports = new HashMap<>();
		
		
		for(Country c : counts)
		{
			this.cities.put(c.getId(), new ArrayList<City>());
			this.countries.put(c.getId(),c);
		}
		
		for(City c : cits)
		{
			this.airports.put(c.getId(), new ArrayList<Airport>());
			this.cities.get(c.getCountryId()).add(c);
		}
		
		for(Airport a : airp)
		{
			this.airports.get(a.getCityId()).add(a);
		}
	}
	
	
}
