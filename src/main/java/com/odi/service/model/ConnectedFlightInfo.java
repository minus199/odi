package com.odi.service.model;

import com.odi.model.ConnectedFlight;
import com.odi.model.Flight;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class ConnectedFlightInfo {
    private String id;
    private String proposalId;

    private int cost;
    private double odicost;

    private String flight1id;
    private String airline1;
    private int flight1Number;
    private String flight1Time;
    private String flight1LandingTime;
    private String originAirport1;
    private String destinationAirport1;

    private String flight2id;
    private String airline2;
    private int flight2Number;
    private String flight2Time;
    private String flight2LandingTime;
    private String originAirport2;
    private String destinationAirport2;

    private String dateOfFlight = "2017-2-20";
    private String name = "John Daw";
    private String phone = "+972525766810";
    private String email = "eyal@gluska.com";
    private String oda = null;


    public ConnectedFlightInfo() {
    }

    public ConnectedFlightInfo(ConnectedFlight flight) {
        setData(flight.getId(), flight.getFirstFlight(), flight.getSecondFlight());
    }

    public ConnectedFlightInfo(String id, Flight flight1, Flight flight2) {
        setData(id, flight1, flight2);
    }

    private void setData(String id, Flight flight1, Flight flight2) {
        DateTimeFormatter dfTime = DateTimeFormat.forPattern("kk:mm");
        DateTime flight1time = new DateTime(flight1.getFlightTime());
        DateTime flight2time = new DateTime(flight2.getFlightTime());
        DateTime flight1LandingTime = new DateTime(flight1.getFlightLanding());
        DateTime flight2LandingTime = new DateTime(flight2.getFlightLanding());

        this.setId(id);

        this.setFlight1id(flight1.getId());
        this.setAirline1(flight1.getAirlineId());
        this.setFlight1Number(flight1.getFlightNumber());
        this.setFlight1Time(flight1time.toString(dfTime));
        this.setFlight1LandingTime(flight1LandingTime.toString(dfTime));
        this.setOriginAirport1(flight1.getOriginAirportId());
        this.setDestinationAirport1(flight1.getDestinationAirportId());

        this.setFlight2id(flight2.getId());
        this.setAirline2(flight2.getAirlineId());
        this.setFlight2Number(flight2.getFlightNumber());
        this.setFlight2Time(flight2time.toString(dfTime));
        this.setFlight2LandingTime(flight2LandingTime.toString(dfTime));
        this.setOriginAirport2(flight2.getOriginAirportId());
        this.setDestinationAirport2(flight2.getDestinationAirportId());

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAirline1() {
        return airline1;
    }

    public void setAirline1(String airline1) {
        this.airline1 = airline1;
    }

    public String getFlight1Time() {
        return flight1Time;
    }

    public void setFlight1Time(String flight1Time) {
        this.flight1Time = flight1Time;
    }

    public String getOriginAirport1() {
        return originAirport1;
    }

    public void setOriginAirport1(String originAirport1) {
        this.originAirport1 = originAirport1;
    }

    public String getDestinationAirport1() {
        return destinationAirport1;
    }

    public void setDestinationAirport1(String destinationAirport1) {
        this.destinationAirport1 = destinationAirport1;
    }

    public String getAirline2() {
        return airline2;
    }

    public void setAirline2(String airline2) {
        this.airline2 = airline2;
    }

    public String getFlight2Time() {
        return flight2Time;
    }

    public void setFlight2Time(String flight2Time) {
        this.flight2Time = flight2Time;
    }

    public String getOriginAirport2() {
        return originAirport2;
    }

    public void setOriginAirport2(String originAirport2) {
        this.originAirport2 = originAirport2;
    }

    public String getDestinationAirport2() {
        return destinationAirport2;
    }

    public void setDestinationAirport2(String destinationAirport2) {
        this.destinationAirport2 = destinationAirport2;
    }

    public String getDateOfFlight() {
        return dateOfFlight;
    }

    public void setDateOfFlight(String dateOfFlight) {
        this.dateOfFlight = dateOfFlight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOda() {
        return oda;
    }

    public void setOda(String oda) {
        this.oda = oda;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public double getOdicost() {
        return odicost;
    }

    public void setOdicost(double odicost) {
        this.odicost = odicost;
    }

    public String getProposalId() {
        return proposalId;
    }

    public void setProposalId(String proposalId) {
        this.proposalId = proposalId;
    }

    public int getFlight1Number() {
        return flight1Number;
    }

    public void setFlight1Number(int flight1Number) {
        this.flight1Number = flight1Number;
    }

    public int getFlight2Number() {
        return flight2Number;
    }

    public void setFlight2Number(int flight2Number) {
        this.flight2Number = flight2Number;
    }

    public String getFlight1LandingTime() {
        return flight1LandingTime;
    }

    public void setFlight1LandingTime(String flight1LandingTime) {
        this.flight1LandingTime = flight1LandingTime;
    }

    public String getFlight2LandingTime() {
        return flight2LandingTime;
    }

    public void setFlight2LandingTime(String flight2LandingTime) {
        this.flight2LandingTime = flight2LandingTime;
    }


    public String getFlight1id() {
        return flight1id;
    }

    public void setFlight1id(String flight1id) {
        this.flight1id = flight1id;
    }

    public String getFlight2id() {
        return flight2id;
    }

    public void setFlight2id(String flight2id) {
        this.flight2id = flight2id;
    }
}
