package com.odi.service.model;

import java.util.List;
import java.util.Map;

import com.odi.model.Flight;

public class FlightPlan 
{
	private String originAirportId = null;
	private String destAirportId = null;
	private String departingDate = null;
	private boolean isOneWay = false;
	private String returnDate = null;
	
	private Map<String,Flight> goingFlights = null;
	private Map<String,Flight> returningFlights = null;
	
	public String getOriginAirportId() {
		return originAirportId;
	}

	public void setOriginAirportId(String originAirportId) {
		this.originAirportId = originAirportId;
	}

	public String getDestAirportId() {
		return destAirportId;
	}

	public void setDestAirportId(String destAirportId) {
		this.destAirportId = destAirportId;
	}

	public String getDepartingDate() {
		return departingDate;
	}

	public void setDepartingDate(String departingDate) {
		this.departingDate = departingDate;
	}

	public boolean isOneWay() {
		return isOneWay;
	}

	public void setOneWay(boolean isOneWay) {
		this.isOneWay = isOneWay;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public Map<String,Flight> getGoingFlights() {
		return goingFlights;
	}

	public void setGoingFlights(Map<String,Flight> goingFlights) {
		this.goingFlights = goingFlights;
	}

	public Map<String,Flight> getReturningFlights() {
		return returningFlights;
	}

	public void setReturningFlights(Map<String,Flight> returningFlights) {
		this.returningFlights = returningFlights;
	}

	public FlightPlan() {
		// TODO Auto-generated constructor stub
	}
	
	public FlightPlan(String originAirportId,
			String destAirportId, String departingDate, boolean isOneWay,
			String returnDate,Map<String,Flight> goingFlights,Map<String,Flight> returningFlights) 
	{
		this.originAirportId = originAirportId;
		this.destAirportId = destAirportId;
		this.departingDate = departingDate;
		this.returnDate = returnDate;
		this.isOneWay = isOneWay;
		this.goingFlights = goingFlights;
		this.returningFlights = returningFlights;
	}
}
