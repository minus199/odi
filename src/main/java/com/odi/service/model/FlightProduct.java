package com.odi.service.model;


public class FlightProduct {
    private String id;
    private String name;
    private String companyId;
    private String airlines;
    private String delayTime;
    private String monetary;
    private String assistance;
    private String departureAirport = "";
    private String arrivalAirport = "";
    private Boolean included = false;
    private Boolean fixed = false;
    private Boolean percentageofcost = true;
    private Boolean percentageoftotal = false;



    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
	public String getAssistance() {
		return assistance;
	}

	public void setAssistance(String assistance) {
		this.assistance = assistance;
	}

	public String getMonetary() {
		return monetary;
	}

	public void setMonetary(String monetary) {
		this.monetary = monetary;
	}


    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Boolean getIncluded() {
		return included;
	}

	public void setIncluded(Boolean included) {
		this.included = included;
	}

	public Boolean getFixed() {
		return fixed;
	}

	public void setFixed(Boolean fixed) {
		this.fixed = fixed;
	}

	public Boolean getPercentageofcost() {
		return percentageofcost;
	}

	public void setPercentageofcost(Boolean percentageofcost) {
		this.percentageofcost = percentageofcost;
	}

	public Boolean getPercentageoftotal() {
		return percentageoftotal;
	}

	public void setPercentageoftotal(Boolean percentageoftotal) {
		this.percentageoftotal = percentageoftotal;
	}

}
