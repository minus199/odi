package com.odi.model;

import org.hibernate.annotations.Proxy;


import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Proxy(lazy = false)
@org.hibernate.annotations.NamedQueries(
{
	@org.hibernate.annotations.NamedQuery(name = "Flight.findByAirlineIdAndFlightNumber", query = "SELECT f FROM Flight f WHERE f.airlineId = ?1 AND f.flightNumber = ?2"),
	@org.hibernate.annotations.NamedQuery(name = "Flight.findByOriginDest", query = "SELECT f FROM Flight f WHERE f.originAirportId = ?1 AND f.destinationAirportId = ?2"),
	@org.hibernate.annotations.NamedQuery(name = "Flight.findByoriginAirportId", query = "SELECT f FROM Flight f WHERE f.originAirportId = ?1"),
	@org.hibernate.annotations.NamedQuery(name = "Flight.findByoriginAirportIdAndDay", query = "SELECT f FROM Flight f WHERE f.originAirportId = ?1 AND f.dayOfWeek = ?2"),
	@org.hibernate.annotations.NamedQuery(name = "Flight.findBydestinationAirportId", query = "SELECT f FROM Flight f WHERE f.destinationAirportId = ?1"),
	@org.hibernate.annotations.NamedQuery(name = "Flight.findBydestinationAirportIdAndDay", query = "SELECT f FROM Flight f WHERE f.destinationAirportId = ?1 AND f.dayOfWeek = ?2"),
})
@Cacheable(true)
// SELECT * FROM FLIGHT order by AIRLINE_ID , ORIGIN_AIRPORT_ID , DESTINATION_AIRPORT_ID , DAY_OF_WEEK 
final public class Flight extends EntityBase {
	private Date flightTime = null;
	private Date flightLanding = null;
	private String airlineId;
	private int flightNumber;
	private String originAirportId;
	private String destinationAirportId;
	private DayOfWeek dayOfWeek;
	private int tempFlightCost = 400;
	private String flightDuration;

	private int flightDurationHours = 0;
	private int flightDurationMinutes = 0;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "flight_instances",
			joinColumns = @JoinColumn(name = "flight_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "flight_instance_id", referencedColumnName = "id"))
	private List<FlightInstance> instances = new ArrayList<FlightInstance>();
	
	public Flight() {	
	}
	
	public Flight(Date flightTime, Date flightLanding, String airlineId, int flightNumber, String originAirportId, String destinationAirportId,
				  DayOfWeek day, int cost) {
		this.setFlightTime(flightTime);
		this.setFlightLanding(flightLanding);
		this.setAirlineId(airlineId);
		this.setFlightNumber(flightNumber);
		this.setOriginAirportId(originAirportId);
		this.setDestinationAirportId(destinationAirportId);
		this.setDayOfWeek(day);
		this.setTempFlightCost(cost);
	}
	
	public Flight(String flightTime, String flightLanding, String airlineId, int flightNumber, String originAirportId, String destinationAirportId,
				  DayOfWeek day, int cost) {
		this.setFlightTime(flightTime);
		this.setFlightLanding(flightLanding);
		this.setAirlineId(airlineId);
		this.setFlightNumber(flightNumber);
		this.setOriginAirportId(originAirportId);
		this.setDestinationAirportId(destinationAirportId);
		this.setDayOfWeek(day);
		this.setTempFlightCost(cost);
	}

	public Date getFlightTime() {
		return flightTime;
	}
	
	public void setFlightTime(Date flightTime) {
		this.flightTime = flightTime;
	}

	// hh:mm
	public void setFlightTime(String flightTime) {
		final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");
		try {
			this.setFlightTime(timeFormat.parse(flightTime));
		} catch (ParseException e) {
			return;
		}
	}
	
	public String getAirlineId() {
		return airlineId;
	}
	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}
	public String getOriginAirportId() {
		return originAirportId;
	}
	public void setOriginAirportId(String originAirportId) {
		this.originAirportId = originAirportId;
	}
	public String getDestinationAirportId() {
		return destinationAirportId;
	}
	public void setDestinationAirportId(String destinationAirportId) {
		this.destinationAirportId = destinationAirportId;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}


	@Override
	public void validate() {
		super.validate();
	}

	public int getTempFlightCost() {
		return tempFlightCost;
	}

	public void setTempFlightCost(int tempFlightCost) {
		this.tempFlightCost = tempFlightCost;
	}

	public List<FlightInstance> getInstances() {
		return instances;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public Date getFlightLanding() {
		return flightLanding;
	}

	public void setFlightLanding(Date flightLanding) {
		this.flightLanding = flightLanding;
	}

	// hh:mm
	public void setFlightLanding(String flightLanding) {
		final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");
		try {
			this.setFlightLanding(timeFormat.parse(flightLanding));
		} catch (ParseException e) {
			return;
		}
	}

	public int getFlightDurationHours() {
		return flightDurationHours;
	}

	public void setFlightDurationHours(int flightDurationHours) {
		this.flightDurationHours = flightDurationHours;
	}

	public int getFlightDurationMinutes() {
		return flightDurationMinutes;
	}

	public void setFlightDurationMinutes(int flightDurationMinutes) {
		this.flightDurationMinutes = flightDurationMinutes;
	}

	@Override
	public String toString() {
		return String.format("%s %d %d:%d", this.getAirlineId(),
				this.getFlightNumber(),
				this.getFlightTime().getHours(),
				this.getFlightTime().getMinutes()
				);
	}



}
