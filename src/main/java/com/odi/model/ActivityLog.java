package com.odi.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@org.hibernate.annotations.NamedQuery(name = "ActivityLog.findByRelatedEntityId", query = "SELECT p FROM ActivityLog p WHERE p.relatedEntityId = ?1")
public class ActivityLog {
    @Id
    @Column(name="id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String activity;
    private String relatedEntityId;
    private String relatedEntityName;

    private String description = "";

    private Date createdDate;

    public ActivityLog() {

    }

    public ActivityLog(String activity, String relatedEntityId, String relatedEntityName, String description) {
        this.activity = activity;
        this.relatedEntityId = relatedEntityId;
        this.relatedEntityName = relatedEntityName;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getRelatedEntityId() {
        return relatedEntityId;
    }

    public void setRelatedEntityId(String relatedEntityId) {
        this.relatedEntityId = relatedEntityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRelatedEntityName() {
        return relatedEntityName;
    }

    public void setRelatedEntityName(String relatedEntityName) {
        this.relatedEntityName = relatedEntityName;
    }

    public void setRelatedEntityName(Class c) {
        setRelatedEntityName(c.getSimpleName());
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @PrePersist
    public void prePersist() {
        this.setCreatedDate(new Date());
    }

     @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        if (description != null && description.isEmpty() == false)
            return String.format("%s ::: %s %s (%s)",
                    dateFormat.format(getCreatedDate()),
                    getRelatedEntityName(),
                    getActivity(),
                    getDescription()
            );
         return String.format("%s ::: %s %s",
                 dateFormat.format(getCreatedDate()),
                 getRelatedEntityName(),
                 getActivity()
         );
     }

}
