package com.odi.model;

import com.odi.view.Currency;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by minus on 4/17/17.
 */


/*@org.hibernate.annotations.NamedQueries(
		{
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findByAirlineIdAndFlightNumber", query = "SELECT f FROM Flight2 f WHERE f.airline = ?1 AND f.flightNumber = ?2"),
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findByOriginDest", query = "SELECT f FROM Flight2 f WHERE f.originAirportId = ?1 AND f.destinationAirportId = ?2"),
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findByoriginAirportId", query = "SELECT f FROM Flight2 f WHERE f.originAirportId = ?1"),
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findByoriginAirportIdAndDay", query = "SELECT f FROM Flight2 f WHERE f.originAirportId = ?1 AND f.dayOfWeek = ?2"),
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findBydestinationAirportId", query = "SELECT f FROM Flight2 f WHERE f.destinationAirportId = ?1"),
				@org.hibernate.annotations.NamedQuery(name = "Flight2.findBydestinationAirportIdAndDay", query = "SELECT f FROM Flight2 f WHERE f.destinationAirportId = ?1 AND f.dayOfWeek = ?2"),
		})*/

@Entity
@Proxy(lazy = false)
@Cacheable
public class Flight2 extends EntityBase {

	private int flightNumber;

	@ManyToOne
	private AirLineCompany airline;

	@ManyToOne
	private Airport originAirport;
	private Date takeoffDate;

	@ManyToOne
	private Airport destinationAirport;
	private Date landingDate;

	private Time flightDuration;

	private Double cost = 400.00;
	private Currency currency;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "flight_instances",
			joinColumns = @JoinColumn(name = "flight_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "flight_instance_id", referencedColumnName = "id"))
	private List<FlightInstance> instances = new ArrayList<FlightInstance>();

	public Flight2() {
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public AirLineCompany getAirline() {
		return airline;
	}

	public Airport getOriginAirport() {
		return originAirport;
	}

	public Date getTakeoffDate() {
		return takeoffDate;
	}

	public Airport getDestinationAirport() {
		return destinationAirport;
	}

	public Date getLandingDate() {
		return landingDate;
	}

	public Time getFlightDuration() {
		return flightDuration;
	}

	public Double getCost() {
		return cost;
	}

	public Currency getCurrency() {
		return currency;
	}

	public List<FlightInstance> getInstances() {
		return instances;
	}

	public Flight2 setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
		return this;
	}

	public Flight2 setAirline(AirLineCompany airline) {
		this.airline = airline;
		return this;
	}

	public Flight2 setOriginAirport(Airport originAirport) {
		this.originAirport = originAirport;
		return this;
	}

	public Flight2 setTakeoffDate(Date takeoffDate) {
		this.takeoffDate = takeoffDate;
		return this;
	}

	public Flight2 setDestinationAirport(Airport destinationAirport) {
		this.destinationAirport = destinationAirport;
		return this;
	}

	public Flight2 setLandingDate(Date landingDate) {
		this.landingDate = landingDate;
		return this;
	}

	public Flight2 setFlightDuration(Time flightDuration) {
		this.flightDuration = flightDuration;
		return this;
	}

	public Flight2 setCost(Double cost) {
		this.cost = cost;
		return this;
	}

	public Flight2 setCurrency(Currency currency) {
		this.currency = currency;
		return this;
	}

	public Flight2 setInstances(List<FlightInstance> instances) {
		this.instances = instances;
		return this;
	}
}
