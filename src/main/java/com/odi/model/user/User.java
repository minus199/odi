package com.odi.model.user;

import com.odi.model.EntityBase;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class User extends EntityBase {

	@Column(name = "email", nullable = false, unique = true)
	private String email;

	@Column(name = "password_hash", nullable = false)
	private String passwordHash;

	@Column(name = "role", nullable = false)
	@Enumerated(EnumType.STRING)
	private UserRole role;

	public User() {

	}

	public User(String email, String password, UserRole userRole) {
		this.email = email;
		setPasswordHash(password);
		role = userRole;
	}

	public Boolean isPasswordMatches(String rawPassword) {
		return BCrypt.checkpw(rawPassword, passwordHash);
	}

	public String getEmail() {
		return email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public UserRole getRole() {
		return role;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPasswordHash(String rawPassword) {
		this.passwordHash = BCrypt.hashpw(rawPassword, BCrypt.gensalt());
	}

	public void setRole(UserRole role) {
		this.role = role;
	}
}
