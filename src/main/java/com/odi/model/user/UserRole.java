package com.odi.model.user;

/**
 * Created by magicalis on 09/03/2017.
 */

import javafx.util.Pair;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.util.streamex.StreamEx;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum UserRole {

	ADMIN("READ", "WRITE"),
	SUB_ADMIN(),
	COMPANY(),
	COMPANY_USER(),
	GUEST();

	public final static String defaultRolePrefix = "ROLE_";
	public final static String roleHierarchy = StreamEx.of(values())
			.map(userRole -> defaultRolePrefix + userRole.name())
			.pairMap(Pair::new)
			.map(pair -> pair.getKey() + " > " + pair.getValue())
			.joining(" ");

	private final List<GrantedAuthority> privileges;

	UserRole(String... privileges) {

		this.privileges = Stream.of(privileges)
				.sorted() // By the order they appear in this enum
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}

	public List<GrantedAuthority> grantedAuthorities() {
		GrantedAuthority role = new SimpleGrantedAuthority(defaultRolePrefix + name());
		if (! this.privileges.contains(role)) this.privileges.add(role);

		return privileges;
	}

}
