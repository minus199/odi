package com.odi.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Proxy(lazy=false)
public class ConnectedFlight extends EntityBase implements Comparable<ConnectedFlight> {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "firstFlightId")
    Flight firstFlight = null;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "secondFlightId")
    Flight secondFlight = null;

    public ConnectedFlight() {
    }

    public ConnectedFlight(Flight firstFlight, Flight secondFlight) {
        this.setFirstFlight(firstFlight);
        this.setSecondFlight(secondFlight);
    }


    public Flight getFirstFlight() {
        return firstFlight;
    }

    public void setFirstFlight(Flight firstFlight) {
        this.firstFlight = firstFlight;
    }

    public Flight getSecondFlight() {
        return secondFlight;
    }

    public void setSecondFlight(Flight secondFlight) {
        this.secondFlight = secondFlight;
    }

    public Date getFlightTime() {
        return getFirstFlight().getFlightTime();
    }

    public Date getFlightLanding() {
        return getFirstFlight().getFlightLanding();
    }

    @Override
    public String toString() {
        return String.format("Connection flight: %s -> %s -> %s",
                getFirstFlight().getOriginAirportId(),
                getFirstFlight().getDestinationAirportId(),
                getSecondFlight().getDestinationAirportId()
        );
    }

    @Override
    public int compareTo(ConnectedFlight o) {
        long o1diff = o.getFirstFlight().getFlightLanding().getTime() - o.getSecondFlight().getFlightTime().getTime();
        long o2diff = this.getFirstFlight().getFlightLanding().getTime() - this.getSecondFlight().getFlightTime().getTime();

        return (int)(o1diff - o2diff);
     }
}
