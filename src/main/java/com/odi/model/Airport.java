package com.odi.model;

import javax.persistence.Entity;

@Entity
final public class Airport extends EntityBase {
	private String cityId;
	private String code = null;

	public Airport() {
	}

	public Airport(String name, String code, String cityId) {
		super(name);
		this.setCode(code);
		this.setCityId(cityId);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.toUpperCase();
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	@Override
	public void validate() {
		super.validate();
	}


}
