package com.odi.model;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;


@Entity
@Proxy(lazy = false)
@NamedQueries({
		@NamedQuery(name = "Country.findByCode", query = "SELECT c FROM Country c WHERE c.code = ?1"),
		@NamedQuery(name = "Country.findByUniquename", query = "SELECT c FROM Country c WHERE c.uniquename = ?1")
	})
//@NamedQuery(name = "Country.findByUnique", query = "SELECT p FROM Country p WHERE p.uniquename = ?1")
public class Country extends EntityBase  {

	public static String toUnique(String name) {
		return name.toLowerCase().replace(" ", "").replaceAll("-", "");
	}
	private String uniquename = "";

	@Override
	public void setName(String name) {
		this.setUniquename(Country.toUnique(name));
		super.setName(name);
	}

	private String code = "";

	public Country() {		
	}

	public Country(String name) {
		this.setName(name);
	}

	public Country(String name, String code) {
		this.setName(name);
		this.setCode(code);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.toUpperCase();
	}

	public String getUniquename() {
		return uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}
	
	
	@Override
	public void validate() {
		super.validate();
	}


}
