package com.odi.model;

/**
 * Created by minus on 3/30/17.
 */
public class KV<K, V> {
	private final K k;
	private final V v;

	private KV(K k, V v) {
		this.k = k;
		this.v = v;
	}

	public static <K, V> KV<K, V> of(K k, V v) {
		return new KV<>(k, v);
	}

	public static KV<Object, Object> empty() {
		return KV.of(null, null);
	}

	public K getK() {
		return k;
	}

	public V getV() {
		return v;
	}
}
