package com.odi.model;

import org.hibernate.annotations.NamedQuery;

import javax.persistence.Entity;
@Entity
@NamedQuery(name = "City.findByUniquenameAndCountryId", query = "SELECT p FROM City p WHERE p.uniquename = ?1 AND p.countryId = ?2")
public class City extends EntityBase {
	public static String toUnique(String name) {
		return name.toLowerCase().replace(" ", "").replaceAll("-", "");
	}

	private String countryId;
	private String uniquename = "";

	@Override
	public void setName(String name) {
		this.setUniquename(Country.toUnique(name));
		super.setName(name);
	}
	
	public City() {};
	
	public City(String name, String countryId) {
		this.setName(name);
		this.setCountryId(countryId);
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getUniquename() {
		return uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	@Override
	public void validate() {
		super.validate();
	}


}
