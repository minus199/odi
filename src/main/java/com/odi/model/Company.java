package com.odi.model;

import javax.persistence.Entity;


@Entity
public class Company extends EntityBase {
	private String address;
	private String telNum;
	private String faxNum;
	

	public Company() {
	}

	public Company(final String name,
			final String address, final String telNum, final String faxNum) {
		this.setName(name);
		this.setAddress(address);
		this.setTelNum(telNum);
		this.setFaxNum(faxNum);
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getTelNum() {
		return this.telNum;
	}

	public void setTelNum(final String telNum) {
		this.telNum = telNum;
	}

	public String getFaxNum() {
		return this.faxNum;
	}

	public void setFaxNum(final String faxNum) {
		this.faxNum = faxNum;
	}

	@Override
	public void validate() {
		super.validate();
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
