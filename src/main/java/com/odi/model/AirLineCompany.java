package com.odi.model;

import javax.persistence.Entity;

@Entity
public class AirLineCompany extends EntityBase {

	private String code = "";
	private String countryId;
	private String cityId;

	public AirLineCompany() {	
	}
	
	public AirLineCompany(String name, String code) {
		super(name);
		this.setCode(code);
	}
	
	public AirLineCompany(String name, String code, String countryId, String cityId) {	
		super(name);
		this.setCode(code);
		this.setCountryId(countryId);
		this.setCityId(cityId);
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.toUpperCase();
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	@Override
	public void validate() {
		super.validate();
	}

}
