package com.odi.model.offer;

import com.odi.model.event.Event;
import com.odi.model.event.FlightLndedEvent;
import org.hibernate.annotations.Proxy;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@NamedQuery(name = "FlightDelayProposal.findAcceptedState", query = "SELECT p FROM FlightDelayProposal p WHERE p.state = 3")
@Proxy(lazy=false) // Dont know why, but it helped...
@DiscriminatorValue("FlightDelayProposal")
@Table(name="FlightDelayProposal")
public class FlightDelayProposal extends Proposal {

	// Dynamic parameters -> The delay
	private int delayHours = 0;
	private int delayMinutes = 0;

	// The Flight
	private String airline = null;
	private int flightNumber = 0;

	// The date of the flight
	private Date flightDate = null;
	
	// Copy from Flight
	private Date flightTime;

	private String flightId;

	/*
	 * Calculated -> The combination of flight planned time + flight date
	 * Hour is defined as a part of  'flight' definition
	 */
	private Date flightPlannedSchedule;
	
	
	public FlightDelayProposal() {
		setSupportedEvent(FlightLndedEvent.class.getSimpleName());
	}
	
	public FlightDelayProposal(String flightId, int delayHours, int delayMinutes, String airline, int flightNumber, Date flightDate, Date flightTime, double compensation) {
		this.setFlightId(flightId);
		this.setDelayHours(delayHours);
		this.setDelayMinutes(delayMinutes);
		this.setAirline(airline);
		this.setFlightNumber(flightNumber);
		this.setFlightDate(flightDate);
		this.setFlightTime(flightTime);
		this.setCompensation(compensation);
		setSupportedEvent(FlightLndedEvent.class.getSimpleName());
	}
	
	public int getDelayHours() {
		return delayHours;
	}

	public void setDelayHours(int delayHours) {
		this.delayHours = delayHours;
	}

	public int getDelayMinutes() {
		return delayMinutes;
	}

	public void setDelayMinutes(int delayMinutes) {
		this.delayMinutes = delayMinutes;
	}

	public String getAirline() {
		return airline.toUpperCase();
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
		calcPlannedSchedule();
	}
	
	public void setFlightDate(String flightDate) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			this.setFlightDate(dateFormat.parse(flightDate));
		} catch (ParseException e) {
			return;
		}
	}

	public Date getFlightTime() {
		return flightTime;
	}

	private void calcPlannedSchedule() {
		if (getFlightDate() == null || getFlightTime() == null)
			return;
		Date plannedDate = getFlightDate();
		plannedDate.setHours(getFlightTime().getHours());
		plannedDate.setMinutes(getFlightTime().getMinutes());
		setFlightPlannedSchedule(plannedDate);
	}
	
	public void setFlightTime(Date flightTime) {
		this.flightTime = flightTime;
		calcPlannedSchedule();
	}

	public Date getFlightPlannedSchedule() {
		return flightPlannedSchedule;
	}

	public void setFlightPlannedSchedule(Date flightPlannedSchedule) {
		this.flightPlannedSchedule = flightPlannedSchedule;
	}


	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	@Override
	public boolean isRelevant(Event event) {
		if (!getProduct().getTemplate().isRelevant(event))
			return false;

		if (event.getClass() == FlightLndedEvent.class) {
			if (((FlightLndedEvent) event).getAirline().equals(getAirline()) == false)
				return false;

			if (((FlightLndedEvent) event).getFlightNumber() != getFlightNumber())
				return false;

			if (((FlightLndedEvent) event).getScheduledDate().getYear() != getFlightPlannedSchedule().getYear())
				return false;

			if (((FlightLndedEvent) event).getScheduledDate().getMonth() != getFlightPlannedSchedule().getMonth())
				return false;

			if (((FlightLndedEvent) event).getScheduledDate().getDate() != getFlightPlannedSchedule().getDate())
				return false;
		}

		return true;
	}
	
	@Override
	public boolean isIn(Event event) {
		if (event.getClass() == FlightLndedEvent.class) {
			int proposalDelayInSeconds = getDelayHours() * 3600 + getDelayMinutes() * 60;
			int actualDelayInSeconds = (int) ((((FlightLndedEvent) event).getArrivalTime().getTime() - getFlightPlannedSchedule().getTime()) / 1000);

			if (actualDelayInSeconds - proposalDelayInSeconds < 0)
				return false;
		}

		if (getProduct().getTemplate().isIn(event) == false)
			return false;
		
		return super.isIn(event);
	}
	
	@Override
	public void validate() {
		// TODO Auto-generated method stub
		super.validate();
	}

	@Override
	public String claimMessage() {
		DateTime date = new DateTime(this.getFlightDate());
		return String.format("Flight %s%d on %s arrived late",
				this.getAirline(),
				this.getFlightNumber(),
				date.toString(DateTimeFormat.forPattern("yyyy-MM-dd"))
				);
	}

	@Override
	public String noClaimMessage() {
		DateTime date = new DateTime(this.getFlightDate());
		return String.format("Flight %s%d on %s arrived on agreed schedule",
				this.getAirline(),
				this.getFlightNumber(),
				date.toString(DateTimeFormat.forPattern("yyyy-MM-dd"))
		);
	}


	@Override
	public String toString() {
		return String.format("(%s) Flight %s%d DELAY on %s, payment %.2f$ to get %.2f$",
				this.getState().toString(),
				this.getAirline(),
				this.getFlightNumber(),
				this.getFlightDate().toString(),
				this.getCost(),
				this.getCompensation()
				);
	}

}
