package com.odi.model.offer;

public enum ProposalState {
	UNDEF(0),
	PROPOSED(100),
	EXPIRED(200),
	ACCEPTED(300),
	REALIZE(400),
	PASSED(4001),
	USERNOTIFIED(500),
	REALIZED(900);
	

	private int value;    

	private ProposalState(int value) {
	    this.value = value;
	}

	public int getValue() {
	    return value;
	}

}
