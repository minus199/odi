package com.odi.model.offer;

import com.odi.model.BaseBase;
import com.odi.model.event.Event;
import com.odi.model.product.Product;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="PROPOSAL_TYPE")
@Table(name="PROPOSAL")
@org.hibernate.annotations.NamedQueries({
		@org.hibernate.annotations.NamedQuery(name = "Proposal.findProposedState", query = "SELECT p FROM Proposal p WHERE p.state = 1"),
		@org.hibernate.annotations.NamedQuery(name = "Proposal.findAcceptedState", query = "SELECT p FROM Proposal p WHERE p.state = 3"),
		@org.hibernate.annotations.NamedQuery(name = "Proposal.findByCompany", query = "SELECT p FROM Proposal p WHERE p.companyId = ?1")
})
public class Proposal extends BaseBase {

	private ProposalState state = ProposalState.UNDEF;
	
	private double cost;
	private double odicost;
	private double compensation;
	
	private Date notifiedTime = null;
	private int notifiedCount = 0;
	
	private String userContactInfo = null;
	
	private String supportedEvent = null;
	
	private String companyId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "theproduct")
	private Product product;


	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getCompensation() {
		return compensation;
	}

	public void setCompensation(double compensation) {
		this.compensation = compensation;
	}

	public Date getNotifiedTime() {
		return notifiedTime;
	}

	public void setNotifiedTime(Date notifiedTime) {
		this.notifiedTime = notifiedTime;
	}

	public int getNotifiedCount() {
		return notifiedCount;
	}

	public void setNotifiedCount(int notifiedCount) {
		this.notifiedCount = notifiedCount;
	}

	public ProposalState getState() {
		return state;
	}

	public void setState(ProposalState state) {
		this.state = state;
	}
	
	public String getUserContactInfo() {
		return userContactInfo;
	}

	public void setUserContactInfo(String userContactInfo) {
		this.userContactInfo = userContactInfo;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}


	public String getSupportedEvent() {
		return supportedEvent;
	}

	public void setSupportedEvent(String supportedEvent) {
		this.supportedEvent = supportedEvent;
	}

	public void makeProposed() {
		setState(ProposalState.PROPOSED);
		validate();
	}
	
	public void makeAccept() {
		setState(ProposalState.ACCEPTED);
		validate();
	}

	public void makeRealize() {
		setState(ProposalState.REALIZE);
		validate();
	}

	public void makePassed() {
		setState(ProposalState.PASSED);
		validate();
	}

	public void makeRealized() {
		setState(ProposalState.REALIZED);
		validate();
	}

	public void makeExpired() {
		setState(ProposalState.EXPIRED);
	}

	public void makeNotified() {
		setState(ProposalState.USERNOTIFIED);
	}

	public void userNotify() {
		setState(ProposalState.USERNOTIFIED);
		setNotifiedCount(getNotifiedCount()+1);
		validate();
	}


	public boolean isEventTypeSupported(Object event) {
		return isEventTypeSupported(event.getClass());
	}
	
	public boolean isEventTypeSupported(Class event) {
		return isEventTypeSupported(event.getSimpleName());
	}
	
	public boolean isEventTypeSupported(String eventType) {
		if (getProduct() == null)
			return false;
		if (getProduct().getTemplate() == null)
			return false;
		return getProduct().getTemplate().isEventTypeSupported(eventType);
		//return getSupportedEvent().toLowerCase().contains(eventType.toLowerCase());
	}

	public boolean isRelevant(Event event) {
		return isEventTypeSupported(event);
	}

	public boolean isIn(Event event) {
		return isRelevant(event);
	}
	
	@Override
	public void validate() {
		super.validate();
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String claimMessage() {
		return "Claim Accepted";
	}

	public String noClaimMessage() {
		return "No Claim";
	}

	public double getOdicost() {
		return odicost;
	}

	public void setOdicost(double odicost) {
		this.odicost = odicost;
	}
}
