package com.odi.model.offer;

import com.odi.model.event.Event;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightDeparturedEvent;
import com.odi.model.event.FlightLndedEvent;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.Date;

@Entity
@NamedQuery(name = "FlightCanceledProposal.findAcceptedState", query = "SELECT p FROM FlightCanceledProposal p WHERE p.state = 3")
@Proxy(lazy=false) // Dont know why, but it helped...
@DiscriminatorValue("FlightCanceledProposal")
@Table(name="FlightCanceledProposal")
public class FlightCanceledProposal extends Proposal {

    // The Flight
    private String airline = null;
    private int flightNumber = 0;

    // The date of the flight
    private Date flightDate = null;

    private String flightId;


    public FlightCanceledProposal() {
        setSupportedEvent(FlightCanceledEvent.class.getSimpleName());
    }

    public FlightCanceledProposal(String flightId, String airline, int flightNumber, Date flightDate) {
        setSupportedEvent(FlightCanceledEvent.class.getSimpleName() +
                "," +
                FlightDeparturedEvent.class.getSimpleName() +
                "," +
                FlightLndedEvent.class.getSimpleName()
                );
        setFlightId(flightId);
        setAirline(airline);
        setFlightNumber(flightNumber);
        setFlightDate(flightDate);
    }


    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }




    @Override
    public boolean isRelevant(Event event) {
        if (isEventTypeSupported(event) == false)
            return false;

        if (event.getClass().getSimpleName().equalsIgnoreCase(FlightCanceledEvent.class.getSimpleName())) {
            if (((FlightCanceledEvent)event).getAirline().equals(getAirline()) == false)
                return false;

            if (((FlightCanceledEvent)event).getFlightNumber() != getFlightNumber())
                return false;

            if (((FlightCanceledEvent)event).getScheduledDate().getYear() != getFlightDate().getYear())
                return false;

            if (((FlightCanceledEvent)event).getScheduledDate().getMonth() != getFlightDate().getMonth())
                return false;

            if (((FlightCanceledEvent)event).getScheduledDate().getDate() != getFlightDate().getDate())
                return false;
        }
        
        if (event.getClass().getSimpleName().equalsIgnoreCase(FlightDeparturedEvent.class.getSimpleName())) {
            if (((FlightDeparturedEvent)event).getAirline().equals(getAirline()) == false)
                return false;

            if (((FlightDeparturedEvent)event).getFlightNumber() != getFlightNumber())
                return false;

            if (((FlightDeparturedEvent)event).getScheduledDate().getYear() != getFlightDate().getYear())
                return false;

            if (((FlightDeparturedEvent)event).getScheduledDate().getMonth() != getFlightDate().getMonth())
                return false;

            if (((FlightDeparturedEvent)event).getScheduledDate().getDate() != getFlightDate().getDate())
                return false;
        }
        
        if (event.getClass().getSimpleName().equalsIgnoreCase(FlightLndedEvent.class.getSimpleName())) {
            if (((FlightLndedEvent)event).getAirline().equals(getAirline()) == false)
                return false;

            if (((FlightLndedEvent)event).getFlightNumber() != getFlightNumber())
                return false;

            if (((FlightLndedEvent)event).getScheduledDate().getYear() != getFlightDate().getYear())
                return false;

            if (((FlightLndedEvent)event).getScheduledDate().getMonth() != getFlightDate().getMonth())
                return false;

            if (((FlightLndedEvent)event).getScheduledDate().getDate() != getFlightDate().getDate())
                return false;
        }
        
 
        return true;
    }
    @Override
    public boolean isIn(Event event) {
        if (isRelevant(event))
        	if (event.getClass().getSimpleName().equalsIgnoreCase(FlightCanceledEvent.class.getSimpleName()))
        		return true;

       return false;
    }

    @Override
    public void validate() {
        super.validate();
    }

    @Override
    public String claimMessage() {
        return String.format("Flight %s%s on %s cancelled",
                this.getFlightDate().toString(),
                this.getAirline(),
                this.getFlightNumber()
        );
    }

    @Override
    public String noClaimMessage() {
        return String.format("Flight %s%s on %s departure",
                this.getFlightDate().toString(),
                this.getAirline(),
                this.getFlightNumber()
        );
    }



    @Override
    public String toString() {
    	DecimalFormat formatter = new DecimalFormat("#0.00");
        return String.format("Flight %s%d CANCELATION on %s, payment %s$ to get %s$",
                this.getAirline(),
                this.getFlightNumber(),
                this.getFlightDate().toString(),
                formatter.format(this.getCost()),
                formatter.format(this.getCompensation())
        );
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }
}
