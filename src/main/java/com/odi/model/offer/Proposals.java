package com.odi.model.offer;


public enum Proposals {
    FlightCancel,
    FlightDelay,
    MissedConnection,
    NoValue;

    public static Proposals toProposal(String proposal)
    {
        try {
            return valueOf(proposal);
        }
        catch (Exception ex) {
            return NoValue;
        }
    }
}
