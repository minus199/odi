package com.odi.model.product;

import com.odi.model.event.*;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;

@Entity
//@NamedQuery(name = "FlightDelayProductTemplate.findAcceptedState", query = "SELECT p FROM FlightDelayProposal p WHERE p.state = 3")
@Proxy(lazy=false) // Dont know why, but it helped...
public class MissedConnectionProductTemplate extends Template {
    final String MISSED_CONNECTION_TEMPLATE_NAME = "Missed Connection";

    private String departureAirport = "";
    private String connectionAirport = "";
    private String destinationAirport = "";

    private String airlines = "";


    public MissedConnectionProductTemplate() {
        this.addEventSupport(FlightCanceledEvent.class.getSimpleName());
        this.addEventSupport(FlightDeparturedEvent.class.getSimpleName());
        this.addEventSupport(FlightLndedEvent.class.getSimpleName());
        setName(MISSED_CONNECTION_TEMPLATE_NAME);
    }


    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getConnectionAirport() {
        return connectionAirport;
    }

    public void setConnectionAirport(String connectionAirport) {
        this.connectionAirport = connectionAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }


    @Override
    public boolean isRelevant(Event event) {
        return super.isRelevant(event);
    }

    @Override
    public boolean isIn(Event event) {
        switch (Events.fromClass(event.getClass())) {
            case FlightCanceledEvent:
                if (getDepartureAirport() != null && getDepartureAirport().isEmpty() == false)
                    if (getDepartureAirport().contains( ((FlightCanceledEvent)event).getDepartureAirportId()) == false)
                        return false;
                if (getDestinationAirport() != null && getDestinationAirport().isEmpty() == false)
                    if (getDestinationAirport().contains( ((FlightCanceledEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getConnectionAirport() != null && getConnectionAirport().isEmpty() == false)
                    if (getConnectionAirport().contains( ((FlightCanceledEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getAirlines() != null && getAirlines().isEmpty() == false)
                    if (getAirlines().contains( ((FlightCanceledEvent)event).getAirline()) == false)
                        return false;
                break;
            case FlightDeparturedEvent:
                if (getDepartureAirport() != null && getDepartureAirport().isEmpty() == false)
                    if (getDepartureAirport().contains( ((FlightDeparturedEvent)event).getDepartureAirportId()) == false)
                        return false;
                if (getDestinationAirport() != null && getDestinationAirport().isEmpty() == false)
                    if (getDestinationAirport().contains( ((FlightDeparturedEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getConnectionAirport() != null && getConnectionAirport().isEmpty() == false)
                    if (getConnectionAirport().contains( ((FlightDeparturedEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getAirlines() != null && getAirlines().isEmpty() == false)
                    if (getAirlines().contains( ((FlightDeparturedEvent)event).getAirline()) == false)
                        return false;
                break;
            case FlightLndedEvent:
                if (getDepartureAirport() != null && getDepartureAirport().isEmpty() == false)
                    if (getDepartureAirport().contains( ((FlightLndedEvent)event).getDepartureAirportId()) == false)
                        return false;
                if (getDestinationAirport() != null && getDestinationAirport().isEmpty() == false)
                    if (getDestinationAirport().contains( ((FlightLndedEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getConnectionAirport() != null && getConnectionAirport().isEmpty() == false)
                    if (getConnectionAirport().contains( ((FlightLndedEvent)event).getArrivalAirportId()) == false)
                        return false;
                if (getAirlines() != null && getAirlines().isEmpty() == false)
                    if (getAirlines().contains( ((FlightLndedEvent)event).getAirline()) == false)
                        return false;
                break;
            case UNKNOWN:
                return false;
        }

        return super.isIn(event);
    }


}
