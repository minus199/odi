package com.odi.model.product;


import com.odi.model.event.Event;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightDeparturedEvent;
import com.odi.model.event.FlightLndedEvent;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;


@Entity
//@NamedQuery(name = "FlightDelayProductTemplate.findAcceptedState", query = "SELECT p FROM FlightDelayProposal p WHERE p.state = 3")
@Proxy(lazy=false) // Dont know why, but it helped...
public class FlightDelayProductTemplate extends Template {
    final String FLIGHT_DELAY_TEMPLATE_NAME = "Flight Delay";

	//LocalTime delaytime = new LocalTime(12, 0);
    private String delayTime = "";
    private String airlines = "";
    private String departureAirport = "";
    private String arrivalAirport = "";

    public FlightDelayProductTemplate() {
        this.addEventSupport(FlightCanceledEvent.class.getSimpleName());
        this.addEventSupport(FlightDeparturedEvent.class.getSimpleName());
        this.addEventSupport(FlightLndedEvent.class.getSimpleName());
        setName(FLIGHT_DELAY_TEMPLATE_NAME);
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public boolean isAirlineIncluded(String airlineCode) {
        if (this.getAirlines() == null || this.getAirlines().isEmpty() == true)
            return true;
        if (this.getAirlines().contains(airlineCode) == true)
            return true;
        return false;
    }

    @Override
	public boolean isRelevant(Event event) {
		return super.isRelevant(event);
	}

	@Override
	public boolean isIn(Event event) {
        if (event.getClass() == FlightLndedEvent.class) {
            if (getDepartureAirport() != null && getDepartureAirport().isEmpty() == false)
                if (getDepartureAirport().contains( ((FlightLndedEvent)event).getDepartureAirportId()) == false)
                    return false;
            if (getArrivalAirport() != null && getArrivalAirport().isEmpty() == false)
                if (getArrivalAirport().contains( ((FlightLndedEvent)event).getArrivalAirportId()) == false)
                    return false;
        }
		return super.isIn(event);
	}

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }


    //public void setDelaytime(String delaytime) {
    //    this.delaytime = LocalTime.parse(delaytime);
    //}

    //public void setDelaytime(int hours, int minutes) {
    //    setDelaytime(this.delaytime.hourOfDay().setCopy(hours).minuteOfHour().setCopy(minutes));
    //}

    //public void setDelaytime(LocalTime delaytime) {
    //    this.delaytime = delaytime;
    //}

    //public LocalTime getDelaytime() {
    //    return this.delaytime;
    //}
}
