package com.odi.model.product;

public enum PaymentMethod {
	Included(1),
	Fixed(2),
	PercentageOfCost(3),
	PercentageOfTotal(4),
	Last(99);
	
	private int value ;
	
	PaymentMethod(int value) {
        this.setValue(value);
    }

	public int getValue() {
		return value;
	}

	private void setValue(int value) {
		this.value = value;
	}
}
