package com.odi.model.product;

import com.odi.model.BaseBase;
import com.odi.model.event.Event;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="Template")
@Table(name="Template")
@Proxy(lazy=false) 
public class Template extends BaseBase {

    private String supportedEvent = "";

    public String getSupportedEvent() {
        return supportedEvent;
    }

    public void setSupportedEvent(String supportedEvent) {
        this.supportedEvent = supportedEvent;
    }

    public void addEventSupport(String eventType) {
        setSupportedEvent(getSupportedEvent() + ";" + eventType);
    }

    public void addEventSupport(Class event) {
        setSupportedEvent(event.getSimpleName());
    }

    public void removeEventSupport(String eventType) {
        setSupportedEvent(getSupportedEvent().replace(eventType + ";",""));
    }

    public void removeEventSupport(Class event) {
        setSupportedEvent(event.getSimpleName());
    }

    public boolean isEventTypeSupported(String eventType) {
        return getSupportedEvent().toLowerCase().contains(eventType.toLowerCase());
    }

    public boolean isEventTypeSupported(Class event) {
        return isEventTypeSupported(event.getSimpleName());
    }

    public boolean isEventTypeSupported(Object event) {
        return isEventTypeSupported(event.getClass());
    }
    
	public boolean isRelevant(Event event) {
    	if (isEventTypeSupported(event) == false)
    		return false;
		
		return true;
	}

	public boolean isIn(Event event) {
		if (isRelevant(event) == false)
			return true;
		
		return true;
	}
}
