package com.odi.model.product;

import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightDeparturedEvent;
import com.odi.model.event.FlightLndedEvent;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;

@Entity
@Proxy(lazy=false) // Dont know why, but it helped...
public class FCProductTemplate  extends Template {

    final String FLIGHT_CANCEL_TEMPLATE_NAME = "Flight Cancellation";

    private String airlines = "";

    public FCProductTemplate() {
        this.addEventSupport(FlightCanceledEvent.class.getSimpleName());
        this.addEventSupport(FlightDeparturedEvent.class.getSimpleName());
        this.addEventSupport(FlightLndedEvent.class.getSimpleName());
        setName(FLIGHT_CANCEL_TEMPLATE_NAME);
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public boolean isAirlineIncluded(String airlineCode) {
        if (this.getAirlines() == null || this.getAirlines().isEmpty() == true)
            return true;
        if (this.getAirlines().contains(airlineCode) == true)
            return true;
        return false;
    }
}
