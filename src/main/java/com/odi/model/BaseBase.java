package com.odi.model;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

//@Entity
@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class BaseBase extends EntityBase {
//    @Id
//	@Column(name="id")
//	//@GeneratedValue(generator = "uuid")
//	//@GenericGenerator(name = "uuid", strategy = "uuid2")
//	private String id = UUID.randomUUID().toString();
//
//	@Column(name="name")
//	private String name;
//    private Date createdDate;
//	private Date lastModifiedDate;
//    
//
//	public BaseBase() {
//		
//	}
//	
//	public BaseBase(String name) {
//		this.setName(name);
//	}
//	
//	public String getStatus() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//    public Date getCreatedDate() {
//		return createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public Date getLastModifiedDate() {
//		return lastModifiedDate;
//	}
//
//	public void setLastModifiedDate(Date lastModifiedDate) {
//		this.lastModifiedDate = lastModifiedDate;
//	}
//
//	public void validate() {
//		return;
//	}
//
//    @PrePersist
//    public void prePersist() {
//        this.setCreatedDate(new Date());
//        this.setLastModifiedDate(this.createdDate);
//		this.validate();
//    }
//     
//    @PreUpdate
//    public void preUpdate() {
//        this.setLastModifiedDate(new Date());
//		this.validate();
//    }
//
//	@Override
//	public String toString() {
//		return getClass().getName() + ": " + getName();
//	}
//
}
