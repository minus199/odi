package com.odi.model;


import javax.persistence.*;
import java.util.Date;
import java.util.UUID;


//@Entity
@MappedSuperclass
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class EntityBase  {
    @Id
	@Column(name="id")
	//@GeneratedValue(generator = "uuid")
	//@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id = UUID.randomUUID().toString();

	@Column(name="name")
	private String name;
    private Date createdDate;
	private Date lastModifiedDate;
    

	public EntityBase() {
		
	}
	
	public EntityBase(String name) {
		this.setName(name);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public void validate() {
		return;
	}

    @PrePersist
    public void prePersist() {
        this.setCreatedDate(new Date());
        this.setLastModifiedDate(this.createdDate);
		this.validate();
    }
     
    @PreUpdate
    public void preUpdate() {
        this.setLastModifiedDate(new Date());
		this.validate();
    }

	@Override
	public String toString() {
		return getClass().getName() + ": " + getName();
	}

}
