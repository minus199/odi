package com.odi.model.event;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("FlightDeparturedEvent")
//@Table(name="FlightDeparturedEvent")
public class FlightDeparturedEvent extends Event {
    private String airline;
    private int flightNumber;

    /*
     * Scheduled date is the date dd/MM/yyyy that the flight is planned to go
     *     - Ignoring the time part of the date. Not relevant for this variable
     *     - This is because there is no 'Date' only field type in Java
     */
    private Date scheduledDate;

    private Date departuredDate;

    private String departureAirportId = "";
    private String arrivalAirportId = "";


    public FlightDeparturedEvent() {

    }

    public FlightDeparturedEvent(String airline, int flightNumber, Date scheduledDate,
                                 Date departuredDate, String departureAirportId, String arrivalAirportId) {
        this.setAirline(airline);
        this.setFlightNumber(flightNumber);
        this.setScheduledDate(scheduledDate);
        this.setDeparturedDate(departuredDate);
    }


    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    @Override
    public void validate() {
        // TODO Auto-generated method stub
        super.validate();
    }


    public Date getDeparturedDate() {
        return departuredDate;
    }

    public void setDeparturedDate(Date departuredDate) {
        this.departuredDate = departuredDate;
    }

     @Override
    public String toString() {
        return "FlightDeparturedEvent{" +
                "airline='" + airline + '\'' +
                ", flightNumber=" + flightNumber +
                ", scheduledDate=" + scheduledDate +
                ", departuredDate=" + departuredDate +
                '}';
    }

    public String getDepartureAirportId() {
        return departureAirportId;
    }

    public void setDepartureAirportId(String departureAirportId) {
        this.departureAirportId = departureAirportId;
    }

    public String getArrivalAirportId() {
        return arrivalAirportId;
    }

    public void setArrivalAirportId(String arrivalAirportId) {
        this.arrivalAirportId = arrivalAirportId;
    }
}
