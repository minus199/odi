package com.odi.model.event;

import com.odi.model.BaseBase;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="EVENT_TYPE")
@Table(name="EVENT")
@Proxy(lazy=false) // Dont know why, but it helped...
@org.hibernate.annotations.NamedQueries({
	@org.hibernate.annotations.NamedQuery(name = "Event.findWaitingStatus", query = "SELECT p FROM Event p WHERE p.status = 1"),
	@org.hibernate.annotations.NamedQuery(name = "Event.findProccessedStatus", query = "SELECT p FROM Event p WHERE p.status = 2")
})
public class Event extends BaseBase {
	private EventSource source = EventSource.UNDEF;
	private EventStatus status = EventStatus.WAITING;
    private Date efectiveDate;
 
    public EventSource getSource() {
		return source;
	}
	public void setSource(EventSource source) {
		this.source = source;
	}
	public Date getEfectiveDate() {
		return efectiveDate;
	}
	public void setEfectiveDate(Date efectiveDate) {
		this.efectiveDate = efectiveDate;
	}
	
	@Override
	public void validate() {
		// TODO Auto-generated method stub
		super.validate();
	}
	public EventStatus getStatus() {
		return status;
	}
	public void setStatus(EventStatus status) {
		this.status = status;
	}
	
	


}
