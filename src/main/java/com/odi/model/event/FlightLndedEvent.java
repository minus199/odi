package com.odi.model.event;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("FlightLndedEvent")
//@Table(name="FlightLndedEvent")
public class FlightLndedEvent extends Event {
	private String airline;
	private int flightNumber;
	
	/*
	 * Scheduled date is the date dd/MM/yyyy that the flight is planned to go
	 *     - Ignoring the time part of the date. Not relevant for this variable
	 *     - This is because there is no 'Date' only field type in Java 
	 */
	private Date scheduledDate;

	private Date departureTime;
	private Date arrivalTime;

	private String departureAirportId = "";
	private String arrivalAirportId = "";


	public FlightLndedEvent() {
	}
	
	public FlightLndedEvent(String airline, int flightNumber, Date scheduledDate, Date departureTime,
			Date arrivalTime, String departureAirportId, String arrivalAirportId) {
		this.setAirline(airline);
		this.setFlightNumber(flightNumber);
		this.setScheduledDate(scheduledDate);
		this.setDepartureTime(departureTime);
		this.setArrivalTime(arrivalTime);
		this.setDepartureAirportId(departureAirportId);
		this.setArrivalAirportId(arrivalAirportId);
	}
	
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public int getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Date getScheduledDate() {
		return scheduledDate;
	}
	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}


	@Override
	public String toString() {
		return "FlightLndedEvent{" +
				"airline='" + airline + '\'' +
				", flightNumber=" + flightNumber +
				", scheduledDate=" + scheduledDate +
				", departureTime=" + departureTime +
				", arrivalTime=" + arrivalTime +
				'}';
	}

	public String getDepartureAirportId() {
		return departureAirportId;
	}

	public void setDepartureAirportId(String departureAirportId) {
		this.departureAirportId = departureAirportId;
	}

	public String getArrivalAirportId() {
		return arrivalAirportId;
	}

	public void setArrivalAirportId(String arrivalAirportId) {
		this.arrivalAirportId = arrivalAirportId;
	}
}
