package com.odi.model.event;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("FlightCanceledEvent")
//@Table(name="FlightCanceledEvent")
public class FlightCanceledEvent extends Event {
    private String airline;
    private int flightNumber;

    /*
     * Scheduled date is the date dd/MM/yyyy that the flight is planned to go
     *     - Ignoring the time part of the date. Not relevant for this variable
     *     - This is because there is no 'Date' only field type in Java
     */
    private Date scheduledDate;

    private String departureAirportId = "";
    private String arrivalAirportId = "";


    public FlightCanceledEvent() {

    }

    public FlightCanceledEvent(String airline, int flightNumber, Date scheduledDate) {
        setAirline(airline);
        setFlightNumber(flightNumber);
        setScheduledDate(scheduledDate);
    }


    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    @Override
    public void validate() {
        // TODO Auto-generated method stub
        super.validate();
    }

    @Override
    public String toString() {
        return String.format("%s%d on %s canceled",
                getAirline(),
                getFlightNumber(),
                LocalDate.fromDateFields(getScheduledDate()).toString(DateTimeFormat.forPattern("yyyy-MM-dd"))
        );
    }

    public String getDepartureAirportId() {
        return departureAirportId;
    }

    public void setDepartureAirportId(String departureAirportId) {
        this.departureAirportId = departureAirportId;
    }

    public String getArrivalAirportId() {
        return arrivalAirportId;
    }

    public void setArrivalAirportId(String arrivalAirportId) {
        this.arrivalAirportId = arrivalAirportId;
    }
}
