package com.odi.model.event;

public enum EventStatus {
	UNDEF(0),
	WAITING(100),
	PROCESSES(200);

	private int value;    

	private EventStatus(int value) {
	    this.value = value;
	}

	public int getValue() {
	    return value;
	}
}
