package com.odi.model.event;

public enum EventSource {
	UNDEF(0),
	RT(100),
	OFFLINE(200);

	private int value;    

	private EventSource(int value) {
	    this.value = value;
	}

	public int getValue() {
	    return value;
	}
}
