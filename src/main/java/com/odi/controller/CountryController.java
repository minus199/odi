package com.odi.controller;

import com.odi.model.Country;
import com.odi.repository.ICountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicLong;


@Controller
@RequestMapping("/country")
public class CountryController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
	@Autowired
	private ICountryRepository countryRepository;

	@ExceptionHandler(IllegalArgumentException.class)
	void handleBadRequests(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.BAD_REQUEST.value(), "Please try again and with a non empty string as 'name'");
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	void handleBadRequests2(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.NOT_FOUND.value(), "Requested item not exist");
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Country>> getAll(@RequestParam(value = "offset", defaultValue = "0") int index,
			@RequestParam(value = "numberOfRecord", defaultValue = "10") int numberOfRecord) {
		System.out.println(String.format("Getting all fruits with index: %d, and count: %d", index, numberOfRecord));

    	List<Country> countries = countryRepository.findAll();
 
		if (countries == null || countries.isEmpty()) {
			return new ResponseEntity<List<Country>>(HttpStatus.NO_CONTENT);
		}
 
		return new ResponseEntity<List<Country>>(countries, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
    public @ResponseBody Country get(@PathVariable("id") String id,
    		@RequestParam(value="name", required=false, defaultValue="A Country") String name) {
		Country country = countryRepository.findOne(id);
    	List<Country> countries = countryRepository.findAll();
    	 
		if (country == null) {
			NoSuchElementException e = new NoSuchElementException();
			throw e;
		}
 
		return country;
    }


    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody Country create(@RequestParam(value="name", required=false, defaultValue="A Country") String name) {
    	Country country = countryRepository.save(new Country(name));
        return country;
    } 
}
