package com.odi.controller;

import com.odi.service.sms.IMessageService;
import com.odi.service.utils.model.IncomingBulkSmsSms;
import com.odi.service.utils.IncomingSmsSerivce;
import com.odi.service.utils.model.IncomingSms;
import com.odi.service.utils.model.OutgoingSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by minus on 4/9/17.
 */
@RestController
//@PreAuthorize("hasIpAddress")
public class SmsController {
	private static final Logger logger = LoggerFactory.getLogger(SmsController.class);
	private final IncomingSmsSerivce smsSerivce;
	private final IMessageService<IncomingSms, OutgoingSms> messageService;

	@Autowired
	public SmsController(IncomingSmsSerivce smsSerivce, IMessageService<IncomingSms, OutgoingSms> messageService) {this.smsSerivce = smsSerivce;
		this.messageService = messageService;
	}

	@PostMapping(value = "/sms/handle",
			consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Map handleIncoming(@RequestBody Map map) {
		logger.debug("check if map is populated ", map);
		HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
		HashMap objectObjectHashMap2 = new HashMap(){{
			put("message", "hello");
		}};
		objectObjectHashMap.put("response", objectObjectHashMap2);
		/*new IncomingSms()
		messageService.handle();*/
		throw new UnsupportedOperationException("sms handling not supported ");
		//new MessagingResponse.Builder().redirect();
	}

	//	private final
	@RequestMapping("/api/sms/callback/incoming")
	@ResponseBody
	public ResponseEntity handleIncomingSMS(@RequestParam(defaultValue = "default") String msisdn,
	                                        @RequestParam(defaultValue = "default") String sender,
	                                        @RequestParam(defaultValue = "default") String message,
	                                        @RequestParam(defaultValue = "default") String dca,
	                                        @RequestParam(defaultValue = "0") Integer msg_id,
	                                        @RequestParam(defaultValue = "default") String source_id,
	                                        @RequestParam(defaultValue = "default") String referring_batch_id,
	                                        @RequestParam(defaultValue = "default") String network_id,
	                                        @RequestParam(defaultValue = "0") Integer concat_reference,
	                                        @RequestParam(defaultValue = "0") Integer concat_num_segments,
	                                        @RequestParam(defaultValue = "0") Integer concat_seq_num,
	                                        @RequestParam(value = "date", required = false) Date received_time) {
		IncomingBulkSmsSms incomingSms = new IncomingBulkSmsSms(msisdn, sender, message, dca, msg_id, source_id, referring_batch_id, network_id,
		                                                        concat_reference, concat_num_segments, concat_seq_num, received_time);

		smsSerivce.handle(incomingSms);
		return new ResponseEntity(HttpStatus.OK);
	}
}
