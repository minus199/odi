package com.odi.controller;

import com.google.gson.Gson;
import com.odi.model.*;
import com.odi.model.event.FlightCanceledEvent;
import com.odi.model.event.FlightLndedEvent;
import com.odi.model.offer.FlightCanceledProposal;
import com.odi.model.offer.FlightDelayProposal;
import com.odi.model.offer.MissedConnectionProposal;
import com.odi.model.offer.Proposal;
import com.odi.model.product.FCProductTemplate;
import com.odi.model.product.FlightDelayProductTemplate;
import com.odi.model.product.Product;
import com.odi.repository.IActivityLogRepository;
import com.odi.service.*;
import com.odi.service.model.*;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
//@RequestMapping("/view")
public class ViewController {

	final private IActivityLogRepository activityLogRepository;
	final private IFlightProposalService flightProposalService;
	final private IFlightService flightService;
	final private IConnectedFlightService connectedFlightService;
	final private IAirLineCompanyService airLineCompanyService;
	final private IAirportService airportService;
	final private IEventService eventService;
	final private IProposalService proposalService;
	final private ICompanyService companyService;
	final private IProductService productService;

	@Autowired
	public ViewController(IActivityLogRepository activityLogRepository, IFlightProposalService flightProposalService,
	                      IFlightService flightService, IConnectedFlightService connectedFlightService,
	                      IAirLineCompanyService airLineCompanyService, IAirportService airportService,
	                      IEventService eventService, IProposalService proposalService,
	                      ICompanyService companyService, IProductService productService) {
		this.activityLogRepository = activityLogRepository;
		this.flightProposalService = flightProposalService;
		this.flightService = flightService;
		this.connectedFlightService = connectedFlightService;
		this.airLineCompanyService = airLineCompanyService;
		this.airportService = airportService;
		this.eventService = eventService;
		this.proposalService = proposalService;
		this.companyService = companyService;
		this.productService = productService;
	}

    @GetMapping("/view/statusproposal")
    public String proposalstatus(Model model, @RequestParam String id) {
        List<ActivityLog> activities = activityLogRepository.findByRelatedEntityId(id);

        model.addAttribute("activities", activities);

        return "proposalstatus";
    }
    
   
    private FlightProposalParams flightDelayProposal2FlightProposalParams(Proposal p) {
        FlightProposalParams params = new FlightProposalParams();
        FlightDelayProposal proposal = (FlightDelayProposal) p;

        params.setProposalID(proposal.getId());
        params.setAirline(proposal.getAirline());
        params.setFlight(String.valueOf(proposal.getFlightNumber()));
        params.setCost(proposal.getCost());
        params.setDelayHours(proposal.getDelayHours());
        params.setDelayMinutes(proposal.getDelayMinutes());
        params.setUserMobile(proposal.getUserContactInfo());
        params.setState(proposal.getState());
        params.setDescription(proposal.toString());
        params.setDescription(proposal.toString());

        DateTime date = new DateTime(proposal.getFlightDate());
        params.setDate(date.toString(DateTimeFormat.forPattern("yyyy-MM-dd")));

        return params;
    }

    private FlightProposalParams flightCancelledProposal2FlightProposalParams(Proposal p) {
        FlightProposalParams params = new FlightProposalParams();
        System.out.println(p.getClass().getSimpleName());
        FlightCanceledProposal proposal = (FlightCanceledProposal) p;

        params.setProposalID(proposal.getId());
        params.setAirline(proposal.getAirline());
        params.setFlight(String.valueOf(proposal.getFlightNumber()));
        params.setCost(proposal.getCost());
        params.setUserMobile(proposal.getUserContactInfo());
        params.setState(proposal.getState());
        params.setDescription(proposal.toString());
        params.setDescription(proposal.toString());

        DateTime date = new DateTime(proposal.getFlightDate());
        params.setDate(date.toString(DateTimeFormat.forPattern("yyyy-MM-dd")));

        return params;
    }

    @GetMapping("/view/proposals")
    public String proposals(Model model) {
        List<Proposal> proposals = proposalService.getAll();

        List<FlightProposalParams> fdList = new ArrayList<FlightProposalParams>();


        for (Proposal proposal : proposals) {
            FlightProposalParams params = new FlightProposalParams();
            params.setProposalID(proposal.getId());
            params.setState(proposal.getState());
            params.setDescription(proposal.toString());
            //FlightProposalParams params = flightDelayProposal2FlightProposalParams(proposal);
            fdList.add(params);
        }

        model.addAttribute("proposals", fdList);
        return "proposals";
    }

    @GetMapping("/view/addproposal")
    public String addproposal(Model model, @RequestParam String flightid) {
        FlightProposalParams proposal = new FlightProposalParams();
        Flight flight = flightService.get(flightid);

        if (flight != null) {
            proposal.setAirline(flight.getAirlineId());
            proposal.setFlight(String.valueOf(flight.getFlightNumber()));
            proposal.setFlightId(flight.getId());
        }

        model.addAttribute("proposal", proposal);

        return "proposal";
    }

    @GetMapping("/view/addconnectionproposal")
    public String addconnectionproposal(Model model, @RequestParam String flights) {
        FlightProposalParams proposal = new FlightProposalParams();
        String[] ids = flights.split(";");
        String flight1id = ids[0];
        String flight2id = ids[1];
        Flight flight = flightService.get(flight1id);

        if (flight != null) {
            proposal.setAirline(flight.getAirlineId());
            proposal.setFlight(String.valueOf(flight.getFlightNumber()));
            proposal.setFlightId(flight.getId());
            proposal.setId(flights);
        }

        model.addAttribute("proposal", proposal);

        return "connectionproposal";
    }

    @PostMapping("/view/connectedproposal")
    public String connectedproposalSubmit(@ModelAttribute FlightProposalParams proposal, Model model) {
        String[] ids = proposal.getId().split(";");
        String flight1id = ids[0];
        String flight2id = ids[1];
        
        Flight flight1 = flightService.get( flight1id);
        Flight flight2 = flightService.get( flight2id);

         
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = df.parse(proposal.getDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MissedConnectionProposal fp = (MissedConnectionProposal) flightProposalService.getMissedConnectionProposal(
        		flight1id,
        		flight2id,
        		"ODI",
                date,
                proposal.getCost(),
                proposal.getUserMobile()
        );
        fp.setProduct(productService.get("GenericFlightCancellation"));

        return "redirect:/view/proposals";

    }


    @GetMapping("/view/addcancel")
    public String addcancelproposal(Model model, @RequestParam String flightid) {
        FlightProposalParams proposal = new FlightProposalParams();
        Flight flight = flightService.get(flightid);

        if (flight != null) {
            proposal.setAirline(flight.getAirlineId());
            proposal.setFlight(String.valueOf(flight.getFlightNumber()));
            proposal.setFlightId(flight.getId());
        }

        model.addAttribute("proposal", proposal);

        return "cancelproposal";
    }


    @GetMapping("/view/editproposal")
    public String editproposal(Model model, @RequestParam String id) {
        FlightProposalParams params = null;
        Proposal proposal = proposalService.get(id);

        if (proposal != null) {
            if (proposal.getClass() == FlightCanceledProposal.class)
                params = flightCancelledProposal2FlightProposalParams(proposal);
            if (proposal.getClass() == FlightDelayProposal.class)
                params = flightDelayProposal2FlightProposalParams(proposal);
            proposal.setId(id);
        }
        model.addAttribute("proposal", params);

        return "proposal";
    }

    @GetMapping("/view/proposal")
    public String proposal(Model model) {
        model.addAttribute("proposal", new FlightProposalParams());
        return "proposal";
    }

    @PostMapping("/view/proposal")
    public String proposalSubmit(@ModelAttribute FlightProposalParams proposal, Model model) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = df.parse(proposal.getDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        FlightDelayProposal fp = (FlightDelayProposal) flightProposalService.getFlightDelayProposal(
                proposal.getFlightId(),
                "DOI", // Eyal, to make it dynamic
                proposal.getDelayHours(),
                proposal.getDelayMinutes(),
                date,
                proposal.getCost(),
                proposal.getUserMobile()
        );
        fp.setProduct(productService.get("GenericFlightDelay"));

        return "redirect:/view/proposals";
    }

    @PostMapping("/view/cancelproposal")
    public String cancelProposalSubmit(@ModelAttribute FlightProposalParams proposal, Model model) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = df.parse(proposal.getDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        FlightCanceledProposal fc = (FlightCanceledProposal) flightProposalService.getFlightCancelProposal(
                proposal.getFlight(),
                date,
                proposal.getCost()
        );

        return "redirect:/view/proposals";
    }

    @GetMapping("/view/deleteproposal")
    public String deleteProposal(Model model, @RequestParam String id) {
        proposalService.delete(id);

        return "redirect:/view/proposals";
    }


    @PostMapping("/view/proposal/accept")
    public String proposalPostAccept(@RequestParam String id, Model model) {
        Proposal p = proposalService.accept(id, "Proposal Accepted");

        return "redirect:/view/proposals";
    }


    /*
     * This is needed in order to get here from proposal lists
     */
    @GetMapping("/view/proposal/accept")
    public String proposalGetAccept(@RequestParam String id, Model model) {
        Proposal p = proposalService.accept(id, "Proposal Accepted");

        return "redirect:/view/proposals";
    }


    @GetMapping("/view/makelandedevent/{id}")
    public String proposalMakeLandedEvent(@PathVariable String id,
                                          @RequestParam(defaultValue = "false") boolean delay,
                                          Model model) {
        Proposal p = proposalService.get(id);

        if (p.isEventTypeSupported(FlightLndedEvent.class) == false)
            return "redirect:/view/proposals";

        Flight flight = null;
        DateTime landeddate = null;
        int delayHours = 0;
        int delayMinutes = 0;

        if (p instanceof MissedConnectionProposal) {
            flight = ((MissedConnectionProposal) p).getFirstFlight();
            landeddate = new DateTime(((MissedConnectionProposal) p).getTravelDate());
            delayHours = 3;
            delayMinutes = 0;
        }
        if (p instanceof FlightDelayProposal) {
            flight = flightService.get(((FlightDelayProposal) p).getFlightId());
            landeddate = new DateTime(((FlightDelayProposal) p).getFlightDate());
            delayHours = ((FlightDelayProposal) p).getDelayHours();
            delayMinutes = ((FlightDelayProposal) p).getDelayMinutes();
        }

        FlightLanded fl = new FlightLanded();
        DateTimeFormatter dfDate = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd kk:mm");


        if (flight != null) {
            fl.setAirline(flight.getAirlineId());
            fl.setFlight(flight.getFlightNumber());

            fl.setDate(landeddate.toString(dfDate));

            // Get expected landing time
            landeddate = landeddate.hourOfDay().setCopy(flight.getFlightTime().getHours());
            landeddate = landeddate.minuteOfHour().setCopy(flight.getFlightTime().getMinutes());

            // Add the delay
            // Make it exactly on the border of the threshold
            landeddate = landeddate.plusHours(delayHours);
            landeddate = landeddate.plusMinutes(delayMinutes);

            if (delay == false)
                // Make it on time
                landeddate = landeddate.minusHours(1);
            else
                // Make it late
                landeddate = landeddate.plusHours(1);

            fl.setArrivaltime(landeddate.toString(dateTimeFormat));
        }

        model.addAttribute("params", fl);
        return "flightlanded";
    }


    @GetMapping("/view/makecancelevent/{id}")
    public String proposalMakeCancelEvent(@PathVariable String id,
                                          @RequestParam(defaultValue = "false") boolean delay,
                                          Model model) {
        Proposal p = proposalService.get(id);
        FlightCanceled fc = new FlightCanceled();

        if (p.isEventTypeSupported(FlightCanceledEvent.class) == false)
            return "redirect:/view/proposals";

        DateTimeFormatter dfDate = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime planneddate = new DateTime(((FlightCanceledProposal) p).getFlightDate());

        fc.setAirline(((FlightCanceledProposal) p).getAirline());
        fc.setFlight(((FlightCanceledProposal) p).getFlightNumber());
        fc.setDate(planneddate.toString(dfDate));

        model.addAttribute("params", fc);
        return "flightcanceled";
    }


///////////////////////////////////////////////////////////////////

    @GetMapping("/view/flights")
    public String flights(Model model) {
    	String[] airlines = { "LY", "AF", "LH" , "U2" };
        List<Flight> flights = flightService.getAll();

        List<FlightInfo> fiList = new ArrayList<FlightInfo>();


        for (Flight flight : flights) {
        	if (Arrays.asList(airlines).contains(flight.getAirlineId()))
        		fiList.add(flightService.flight2FlightInfo(flight));
        }

        Collections.sort(fiList);
        
        model.addAttribute("flights", fiList);
        return "flights";
    }

    @GetMapping("/view/deleteflight")
    public String deleteFlight(Model model, @RequestParam String id) {
        Flight flight = flightService.get(id);
        flightService.delete(id);

        return "redirect:/view/flights";
    }

    @GetMapping("/view/editflight")
    public String editFlight(Model model, @RequestParam String id) {
        Flight flight = flightService.get(id);
        FlightInfo flightInfo = flightService.flight2FlightInfo(flight);

        flightInfo.setAirline(String.valueOf(flight.getAirlineId()));
        flightInfo.setDestinationAirport(String.valueOf(flight.getDestinationAirportId()));
        flightInfo.setOriginAirport(String.valueOf(flight.getOriginAirportId()));

        model.addAttribute("flight", flightInfo);
        return "flight";
    }

    @GetMapping("/view/flight")
    public String flight(Model model) {
        FlightInfo flight = new FlightInfo();
        List<AirLineCompany> airlines = airLineCompanyService.getAll();
        List<Airport> airports = airportService.getAll();

        model.addAttribute("flight", flight);
        model.addAttribute("airports", airports);
        model.addAttribute("airlines", airlines);
        return "flight";
    }


    @PostMapping("/view/flight")
    public String flightSubmit(@ModelAttribute FlightInfo flight, Model model) {
        Flight f = flightService.saveFlight(flightService.flightInfo2Flight(flight));

        return "redirect:/view/flights";
        //return flights(model);
    }


///////////////////////////////////////////////////////////////////

    @GetMapping("/view/flightlanded")
    public String flightlanded(Model model) {
        model.addAttribute("params", new FlightLanded());
        return "flightlanded";
    }


    @PostMapping("/view/flightlanded")
    public String flightLandedSubmit(@ModelAttribute FlightLanded params, Model model) {
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd kk:mm");

        DateTime date = df.parseDateTime(params.getDate());
        DateTime arrival = dateTimeFormat.parseDateTime(params.getArrivaltime());

        String flightId = null;

        List<Flight> flights = flightService.findByAirlineIdAndFlightNumber(params.getAirline(), params.getFlight());
        // Patch, to make sure there is always flight since there is no error handling yet...
        if (flights.size() > 0)
            flightId = flights.get(0).getId();
        for (Flight f : flights) {
            if (date.getDayOfWeek() == f.getDayOfWeek().getValue())
                flightId = f.getId();
        }
        eventService.flightLanded(
                flightId,
                date.toDate(),
                null,
                arrival.toDate()
        );

        return "redirect:/view/proposals";
    }


    @PostMapping("/view/flightcanceled")
    public String flightCanceledSubmit(@ModelAttribute FlightCanceled params, Model model) {
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");

        DateTime date = df.parseDateTime(params.getDate());

        eventService.flightCanceled(
                "", // Eyal, to set the flight id
                date.toDate()
        );

        return "redirect:/view/proposals";
    }

    /////////////////////////////   Company   ///////////////////////////////

    @GetMapping("/view/companies")
    public String companies(Model model) {
        List<Company> companies = companyService.getAll();

        model.addAttribute("companies", companies);
        return "companies";
    }


    @GetMapping("/view/company")
    public String company_get(Model model) {
        Company company = new Company();

        model.addAttribute("company", company);
        return "company";
    }


    @PostMapping("/view/company")
    public String company_post(@ModelAttribute Company company, Model model) {
        companyService.save(company);

        return "redirect:/view/companies";
    }

    @GetMapping("/view/editcompany")
    public String editCompany(Model model, @RequestParam String id) {
        Company company = companyService.get(id);

        model.addAttribute("company", company);
        return "company";
    }

    @GetMapping("/view/deletecompany")
    public String deleteCompany(Model model, @RequestParam String id) {
        Company company = companyService.get(id);
        companyService.delete(id);

        return "redirect:/view/companies";
    }

    @GetMapping("/view/companydashboard")
    public String companyDashboard(String id, Model model) {
        //id = "ODI";
        Company company = companyService.get(id);
        List<Product> products = productService.findByCompanyId(id);
        List<Proposal> proposals = proposalService.findByCompanyId(id);

        //products = productService.getAll();
        proposals = proposalService.getAll();
        model.addAttribute("products", products);
        model.addAttribute("proposals", proposals);
        model.addAttribute("company", company);

        return "companydashboard";
    }


    @GetMapping("/view/companyflights")
    public String companyFlights(String id, Model model) {
        List<Flight> flights = flightService.getAll();
        Company company = companyService.get(id);

        List<FlightInfo> fiList = new ArrayList<FlightInfo>();


        for (Flight flight : flights)
            fiList.add(flightService.flight2FlightInfo(flight));

        Collections.sort(fiList);

        model.addAttribute("flights", fiList);
        model.addAttribute("company", company);

        return "companyflights";
    }

    @GetMapping("/view/companydelayproduct")
    public String companydelayproduct(String id, Model model) {
        FlightProduct product = new FlightProduct();
        List<Company> companies = companyService.getAll();

        product.setCompanyId(id);
        model.addAttribute("companies", companies);
        model.addAttribute("product", product);
        return "fdelayproduct";
    }


    /////////////////////////////   Company   ///////////////////////////////

    @GetMapping("/view/products")
    public String products(Model model) {
        List<Product> products = productService.getAll();

        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/view/deleteproduct")
    public String deleteProduct(Model model, @RequestParam String id) {
        productService.delete(id);

        return "redirect:/view/companies";
    }


    @GetMapping("/view/flightdelayproduct")
    public String flightDelayProducts(Model model) {
        FlightProduct product = new FlightProduct();
        List<Company> companies = companyService.getAll();

        model.addAttribute("companies", companies);
        model.addAttribute("product", product);
        return "fdelayproduct";
    }

    @PostMapping("/view/flightdelayproduct")
    public String flightDelayProducts_post(FlightProduct prd, Model model) {
        Product product = new Product();
        FlightDelayProductTemplate template = new FlightDelayProductTemplate();

        product.setCompanyId(prd.getCompanyId());
        template.setAirlines(prd.getAirlines());
        template.setDelayTime(prd.getDelayTime());
        template.setArrivalAirport(prd.getArrivalAirport());
        template.setDepartureAirport(prd.getDepartureAirport());
        product.setTemplate(template);
        product.setName(prd.getName());

        // Make it readable ID (for Demo purpose)
        if (product.getName() != null && product.getName().isEmpty() == false)
            product.setId(WordUtils.capitalize(prd.getName()).replace(" ", ""));

        if (prd.getId() != null && prd.getId().isEmpty() == false)
            product.setId(prd.getId());

        productService.save(product);

        return "redirect:/view/products";
    }

    @GetMapping("/view/flightcancelproduct")
    public String flightCancelProducts(Model model) {
        FlightProduct product = new FlightProduct();
        List<Company> companies = companyService.getAll();

        model.addAttribute("companies", companies);
        model.addAttribute("product", product);
        return "fcalcelproduct";
    }

    @PostMapping("/view/flightcalcelproduct")
    public String flightCancelProducts_post(FlightProduct prd, Model model) {
        Product product = new Product();
        FCProductTemplate template = new FCProductTemplate();

        product.setCompanyId(prd.getCompanyId());
        template.setAirlines(prd.getAirlines());
        product.setTemplate(template);

        if (prd.getId() != null && prd.getId().isEmpty() == false)
            product.setId(prd.getId());

        productService.save(product);

        return "redirect:/view/products";
    }

    @GetMapping("/view/editproduct")
    public String editProduct(Model model, @RequestParam String id) {
        Product product = productService.get(id);
        if (product.getTemplate().getClass() == FlightDelayProductTemplate.class) {
            List<Company> companies = companyService.getAll();

            FlightProduct fp = new FlightProduct();
            fp.setId(product.getId());
            fp.setCompanyId(product.getCompanyId());
            fp.setDelayTime(((FlightDelayProductTemplate) product.getTemplate()).getDelayTime());
            fp.setAirlines(((FlightDelayProductTemplate) product.getTemplate()).getAirlines());
            fp.setArrivalAirport(((FlightDelayProductTemplate) product.getTemplate()).getArrivalAirport());
            fp.setDepartureAirport(((FlightDelayProductTemplate) product.getTemplate()).getDepartureAirport());
            model.addAttribute("companies", companies);
            model.addAttribute("product", fp);
            return "fdelayproduct";
        }

        if (product.getTemplate().getClass() == FCProductTemplate.class) {
            List<Company> companies = companyService.getAll();

            FlightProduct fp = new FlightProduct();
            fp.setId(product.getId());
            fp.setCompanyId(product.getCompanyId());
            fp.setAirlines(((FCProductTemplate) product.getTemplate()).getAirlines());
            model.addAttribute("companies", companies);
            model.addAttribute("product", fp);
            return "fdelayproduct";
        }

        return "redirect:/view/products";
    }

    @GetMapping("/view/addproductproposal")
    public String addProductProposal(Model model, @RequestParam String productid) {
        FlightProposalParams proposal = new FlightProposalParams();
        Product product = productService.get(productid);
        if (product != null) {
            if (((FlightDelayProductTemplate) product.getTemplate()).getDelayTime() != null &&
                    ((FlightDelayProductTemplate) product.getTemplate()).getDelayTime().isEmpty() == false) {
                LocalTime time = LocalTime.parse(
                        ((FlightDelayProductTemplate) product.getTemplate()).getDelayTime(),
                        DateTimeFormat.forPattern("HH:mm")
                );
                proposal.setDelayHours(time.getHourOfDay());
                proposal.setDelayMinutes(time.getMinuteOfHour());
                proposal.setDate(LocalDate.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd")));
            }
        }
        model.addAttribute("proposal", proposal);

        return "proposal";
    }

    /////////////////////////////   OTA   ///////////////////////////////

    @GetMapping("/view/ota")
    public String ota(Model model, @RequestParam String flightid) {
        Flight flight = flightService.get(flightid);
        OtaFlight otaFlight = new OtaFlight();


        otaFlight.setId(flightid);
        otaFlight.setAirline(flight.getAirlineId());
        otaFlight.setDepartureAirport(flight.getOriginAirportId());
        otaFlight.setDesctinationAirport(flight.getDestinationAirportId());
        otaFlight.setFlightNumber(flight.getFlightNumber());

        DateTime dateTime = new DateTime(flight.getFlightTime());
        otaFlight.setFlightTime(dateTime.toString(DateTimeFormat.forPattern("HH:mm")));

        model.addAttribute("flight", otaFlight);

        return "ota";
    }

    @PostMapping("/view/ota")
    public String ota(Model model, @ModelAttribute OtaFlight flight) {
        int cost = 400;
        DateTime dateTime = DateTime.parse(flight.getDateOfFlight(),
                DateTimeFormat.forPattern("yyyy-MM-dd"));
        Proposal proposal = flightProposalService.getFlightDelayProposal(
                flight.getId(),
                "ODI", // Eyal, to make dynamic
                4,
                0,
                dateTime.toDate(),
                cost,
                flight.getPhone()
        );
        flight.setProposalId(proposal.getId());
        flight.setCost(cost);
        flight.setOdicost(proposal.getCost());
        flight.setOda("yes");

        model.addAttribute("flight", flight);

        return "flightorder";
    }

    @PostMapping("/view/otaflight")
    public String otaflight(Model model, @ModelAttribute OtaFlight flight) {
    	if (flight.getOda() != null && flight.getOda().equals("yes"))
    		proposalService.accept(flight.getProposalId(), "OTA proposal accepted");
        return "redirect:/view/proposals";
    }

    
    @PostMapping("/book_flight")
    public String book_flight(Model model, HttpServletRequest request)
    {
    	model.addAttribute("json_data", request.getParameter("json_data")); 
    	
    	return "book";
    }
    
    
    @PostMapping("/edreams_book")
    public String edreams_book_flight(Model model, HttpServletRequest request)
    {
    	Map<String, String> parameters = new HashMap<String, String>();

		Map map = request.getParameterMap();
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			System.out.println(key + "=" + ((String[])map.get(key))[0] );
			parameters.put(key, ((String[])map.get(key))[0]);
		}
		
    	model.addAttribute("form_data", new Gson().toJson(map)); 
    	
    	return "edreams_book";
    }
    
    
    @GetMapping("/edreams_book")
    public String edreams_book_flight_get()
    {
    	return "redirect:edreams";
    }
    
    
    @GetMapping("/edreams")
    public String edreams_index(Model model, HttpServletRequest request)
    {
    	return "edreams";
    }


    ////////////////// Connections /////////////////////
    @PostMapping("/view/connections")
    public String connections_post(Model model, String src, String dest, String thedate) {
        //List<ConnectedFlight> flights = connectedFlightService.getAll();
        List<ConnectedFlight> flights = connectedFlightService.getFlights(src, dest, thedate);

        List<ConnectedFlightInfo> fiList = new ArrayList<ConnectedFlightInfo>();


        for (ConnectedFlight flight : flights) {
            ConnectedFlightInfo fi = new ConnectedFlightInfo(flight.getId(), flight.getFirstFlight(), flight.getSecondFlight());

            fiList.add(fi);
        }

        model.addAttribute("flights", fiList);
        return "connectionflights";
    }

    @GetMapping("/view/connections")
    public String connections(Model model, String src, String dest, String thedate) {
        //List<ConnectedFlight> flights = connectedFlightService.getAll();
        List<ConnectedFlight> flights = connectedFlightService.getFlights(src, dest, thedate);

        List<ConnectedFlightInfo> fiList = new ArrayList<ConnectedFlightInfo>();


        for (ConnectedFlight flight : flights) {
            ConnectedFlightInfo fi = new ConnectedFlightInfo(flight.getId(), flight.getFirstFlight(), flight.getSecondFlight());

            fiList.add(fi);
        }

        model.addAttribute("flights", fiList);
        return "connectionflights";
    }

    @GetMapping("/view/connectionota")
    public String connectionota(Model model, @RequestParam String flightid) {
        ConnectedFlight flight = connectedFlightService.get(flightid);
        ConnectedFlightInfo flightInfo = new ConnectedFlightInfo(flight);

        //DateTime dateTime = new DateTime(flight.getFlightTime());
        //otaFlight.setFlightTime(dateTime.toString(DateTimeFormat.forPattern("HH:mm")));

        model.addAttribute("flight", flightInfo);

        return "connectionota";
    }

    @PostMapping("/view/connectionota")
    public String connectionota(Model model, @ModelAttribute ConnectedFlightInfo flight) {
        DateTime dateTime = DateTime.parse(flight.getDateOfFlight(),
                DateTimeFormat.forPattern("yyyy-MM-dd"));

        Proposal proposal = flightProposalService.getMissedConnectionProposal(
                flight.getId(),
                "ODI",
                "GenericMissedConnection",
                dateTime.toDate(),
                0,
                flight.getPhone()
        );
        OtaFlight otaFlight = new OtaFlight();
        otaFlight.setAirline(String.format("%s->%s",
                ((MissedConnectionProposal)proposal).getFirstFlight().getAirlineId(),
                ((MissedConnectionProposal)proposal).getSecondFlight().getAirlineId()
        ));
        otaFlight.setDepartureAirport( ((MissedConnectionProposal)proposal).getFirstFlight().getOriginAirportId());
        otaFlight.setDesctinationAirport( ((MissedConnectionProposal)proposal).getSecondFlight().getDestinationAirportId());
        otaFlight.setFlightNumber( ((MissedConnectionProposal)proposal).getFirstFlight().getFlightNumber());
        otaFlight.setDateOfFlight(flight.getDateOfFlight());

        otaFlight.setFlightTime( flight.getFlight1Time() );

        otaFlight.setProposalId(proposal.getId());
        otaFlight.setCost(600);
        otaFlight.setOdicost(proposal.getCost());
        otaFlight.setOda("yes");

        model.addAttribute("flight", otaFlight);

        return "flightorder";
    }


}