package com.odi.controller;

import com.odi.service.IFlightService;
import com.odi.service.model.FlightOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/flight")
public class FlightController {
	protected final Logger logger = LoggerFactory.getLogger("controller");

    @Autowired
    IFlightService flightService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/options", method = RequestMethod.GET)
    public ResponseEntity<List<FlightOption>> getOptions(HttpServletRequest request, 
    									@RequestParam(defaultValue = "TLV") String source,
                                        @RequestParam(defaultValue = "CDG") String dest,
                                        @RequestParam(defaultValue = "2017-03-20") String godate,
                                        @RequestParam(defaultValue = "2017-03-30") String returndate
                                        ) {
		logger.info(request.getRequestURI() + "?" + request.getQueryString());
        return new ResponseEntity<List<FlightOption>>(
                flightService.getFlightOptions(source, dest, godate, returndate),
                HttpStatus.OK);

    }

}
