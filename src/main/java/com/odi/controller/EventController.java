package com.odi.controller;

import com.odi.service.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
@RequestMapping("/event")
public class EventController {

	@Autowired
	private IEventService eventService;

	
	@RequestMapping(value = "/flightlanded", method = RequestMethod.POST)
	public ResponseEntity<String> flightLanded(
			// Air line code
			@RequestParam String airline,
			// Flight number
			@RequestParam int flightno,
			// Flight schedule date
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date date,
			// Departure airport code
			@RequestParam String fromairport,
			// Country Id for departure airport - 2-Char Country Codes
			@RequestParam String fromcountry,
			// City Id for departure airport
			@RequestParam String fromcity,
			// Arrival airport code
			@RequestParam String toairport,
			// Country Id for arrival airport - 2-Char Country Codes
			@RequestParam String tocountry,
			// City Id for arrival airport
			@RequestParam String tocity,
			// Schedule departure time (local)
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd hh:mm") Date departue,
			//Schedule arrival time (local)
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd hh:mm") Date arival,
			// Leave gate departure time (local)
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd hh:mm") Date outgate,
			// Connected to gate arrival time (local)
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd hh:mm") Date ingate
			
			) {
		// Eyal, to fix eventService.flightLanded(airline, flightno, date, departue, arival);
		return new ResponseEntity<String>("OK", null, HttpStatus.OK);
	}

}
