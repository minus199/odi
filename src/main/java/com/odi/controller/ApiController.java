package com.odi.controller;

import com.odi.model.Airport;
import com.odi.model.City;
import com.odi.model.Country;
import com.odi.repository.IAirportRepository;
import com.odi.repository.ICityRepository;
import com.odi.repository.ICountryRepository;
import com.odi.service.IFlightService;
import com.odi.service.model.FlightPlan;
import com.odi.service.model.ViewGlobalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/wapi")
public class ApiController {
	private static final String template = "Hello, %s!";
	protected final Logger logger = LoggerFactory.getLogger("controller");
	private final AtomicLong counter = new AtomicLong();

	private final ICountryRepository countryRepository;

	private final ICityRepository cityRepository;

	private final IAirportRepository airportRepository;

	private final IFlightService flightService;

	@Autowired
	public ApiController(ICountryRepository countryRepository, ICityRepository cityRepository,
	                     IAirportRepository airportRepository, IFlightService flightService) {
		this.countryRepository = countryRepository;
		this.cityRepository = cityRepository;
		this.airportRepository = airportRepository;
		this.flightService = flightService;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/search_flights", method = RequestMethod.POST)
	public ResponseEntity<FlightPlan> search_flights(HttpServletRequest request, @RequestParam(value = "origin") String originAirportId,
	                                                 @RequestParam(value = "destination") String destAirportId, @RequestParam(value = "departing") String departingDate, @RequestParam(value = "oneway", required = false, defaultValue = "false") boolean isOneWay,
	                                                 @RequestParam(value = "return", required = false) String returnDate) {
		System.out.println("Searching flights");
		logger.info(request.getRequestURI());

		return new ResponseEntity<FlightPlan>(flightService.searchFlights(originAirportId, destAirportId, departingDate, isOneWay, returnDate), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost")
	@RequestMapping(value = "/get_global_data", method = RequestMethod.GET)
	public ResponseEntity<ViewGlobalData> get_ui_config() {
		System.out.println("Getting ui global config");

		List<Country> countries = countryRepository.findAll();
		List<City> cities = cityRepository.findAll();
		List<Airport> airports = airportRepository.findAll();

		return new ResponseEntity<ViewGlobalData>(new ViewGlobalData(countries, cities, airports), HttpStatus.OK);
	}
}
