package com.odi.controller;

import com.odi.model.offer.Proposal;
import com.odi.model.offer.Proposals;
import com.odi.service.IFlightProposalService;
import com.odi.service.IProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
@RequestMapping("/proposal")
public class ProposalController {

	@Autowired
	private IFlightProposalService flightProposalService;

	@Autowired
	private IProposalService proposalService;


	@CrossOrigin(origins = "*")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<Proposal> getItFlightDelayProposal(
			HttpServletRequest request,
			@RequestParam(defaultValue = "FlightDelay") String template)
	{
		Map<String, String> parameters = new HashMap<String, String>();

		Map map = request.getParameterMap();
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			System.out.println(key + "=" + ((String[])map.get(key))[0] );
			parameters.put(key, ((String[])map.get(key))[0]);
		}

		return new ResponseEntity<Proposal>(proposalService.getProposal(template, parameters), null, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ResponseEntity<Proposal> getFlightDelayProposal(
			HttpServletRequest request,
			@RequestParam String flightid,
			@RequestParam String companyid,
			@RequestParam String productid,
			@RequestParam(defaultValue="0.0") double compensation,
			@RequestParam(defaultValue="4") int hours,
			@RequestParam(defaultValue="0") int minutes,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date date)
	{
		// Note: The proposal should be build by the product, we should receive:
		// flightId - the flight id (airport, airline and price could be retrieved from the flight)
		// companyId
		// flightDate - the flight date and time so we could create a unique flight
		// numberOfTickets - the number of tickets purchased (This should multiply the amount and the compensation)
		//  -> Maybe we should add numberOfTickets to the Proposal model along with cost, totalCost, compensation and totalCompensation
		// hours/minutes can be optional
		/*
		 * todo: return ProposalResult
		 * {
		 * 		Proposal - the proposal,
		 * 		DisplayText - the text to display the user,
		 * 		Yes text - For example "Yes I want it"
		 * 		No text -  For example "No I don'csvJob wan'csvJob it"
		 * }
		 */

		Map<String, String> parameters = new HashMap<String, String>();

		Map map = request.getParameterMap();
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			System.out.println(key + "=" + ((String[])map.get(key))[0] );
			parameters.put(key, ((String[])map.get(key))[0]);
		}

		Proposal p = proposalService.getProposal(Proposals.FlightDelay.toString(), parameters);
		//Proposal p = flightProposalService.getFlightDelayProposal(flightid, hours, minutes, date, compensation, null);

		return new ResponseEntity<Proposal>(p, null, HttpStatus.OK);
	}

	@RequestMapping(value = "/flight", method = RequestMethod.GET)
	public ResponseEntity<String> getFlightProposal(
			@RequestParam String flightid,
			@RequestParam int flight,
			@RequestParam String airline,
			@RequestParam(defaultValue="120.0") double compensation,
			@RequestParam(defaultValue="4") int hours,
			@RequestParam(defaultValue="0") int minutes,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date date) 
	{
		// Note: The proposal should be build by the product, we should receive:
		// flightId - the flight id (airport, airline and price could be retrieved from the flight)
		// companyId
		// flightDate - the flight date and time so we could create a unique flight
		// numberOfTickets - the number of tickets purchased (This should multiply the amount and the compensation)
		//  -> Maybe we should add numberOfTickets to the Proposal model along with cost, totalCost, compensation and totalCompensation
		// hours/minutes can be optional
		/*
		 * todo: return ProposalResult 
		 * {
		 * 		Proposal - the proposal,
		 * 		DisplayText - the text to display the user,
		 * 		Yes text - For example "Yes I want it"
		 * 		No text -  For example "No I don'csvJob wan'csvJob it"
		 * }
		 */
		return new ResponseEntity<String>(flightProposalService.getFlightDelayProposal(flightid, "ODI", hours, minutes, date, compensation, null).getId().toString(), null, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/accept", method = RequestMethod.POST)
	public ResponseEntity<String> acceptFlightProposal(
			@RequestParam String proposalId,
			@RequestParam String user,
			@RequestParam String userphone) 
	{
		// todo: accept the proposal and save the user details on service
		// todo: return the proposal id (or some identifier so 3rd parties could have an assurence that the proposal was accepted)
		return new ResponseEntity<String>(flightProposalService.accept(proposalId, userphone).getId(), HttpStatus.OK);
	}

}
