package com.odi.controller;

import com.odi.model.user.User;
import com.odi.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicLong;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @ExceptionHandler(IllegalArgumentException.class)
    void handleBadRequests(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Please try again and with a non empty string as 'name'");
    }

    @ExceptionHandler(NoSuchElementException.class)
    void handleBadRequests2(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), "Requested item not exist");
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAll() {
        List<User> users = userService.getAll();

        if (users == null || users.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public @ResponseBody
    User get(@PathVariable("id") String id) {
        User user = userService.get(id);

        if (user == null) {
            NoSuchElementException e = new NoSuchElementException();
            throw e;
        }

        return user;
    }

    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody User create(@RequestParam String name,
                                     @RequestParam String password,
                                     @RequestParam String email) {
        User user = new User();
        user.setName(name);
        user.setPasswordHash(password);
        user.setEmail(email);

        return userService.save(user);
    }
}
