package com.odi.excptions;

import java.util.ArrayList;
import java.util.List;

public class InvalidDataException extends Exception {

	static final long serialVersionUID = -5387516993124229948L;
	private String fieldName;
	private List<String> errors;
	
	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public InvalidDataException(List<String> errors) {
		super(((errors == null) || errors.isEmpty())? "" : errors.get(0)) ;
		this.errors=errors;
	}
	public InvalidDataException() {
		super();
		this.errors=new ArrayList<String>();
	}

	public InvalidDataException(String message, Throwable arg1) {
		super(message, arg1);
		this.errors=new ArrayList<String>();
		errors.add(message);
	}

	public InvalidDataException(String message) {
		super(message);
		this.errors=new ArrayList<String>();
		errors.add(message);
	}

	public InvalidDataException(Throwable arg0) {
		super(arg0);
		this.errors=new ArrayList<String>();
	}
	
	public InvalidDataException(String message,String fieldName) {
		super(message);
		this.fieldName = fieldName;
	}
	
	@Override
	public String getMessage()
	{
		if ((errors != null) && !errors.isEmpty())
		{
			if (errors.size() == 1)
			{
				return errors.get(0) ;
			}
			return errors.toString() ;
		}
		else
		{
			return super.getMessage() ;
		}
	}

}

