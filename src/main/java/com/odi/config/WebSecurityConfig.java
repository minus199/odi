package com.odi.config;

import com.odi.service.users.domain.GuestUser;
import com.odi.service.users.impl.ApiUserAuthenticationProvider;
import com.odi.service.users.impl.GuestAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by minus on 4/3/17.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final GuestAuthenticationProvider guestAuthProvider;
	private final UserDetailsService userDetailsService;
	private final AuthenticationProvider authProvider;

	@Autowired
	public WebSecurityConfig(GuestAuthenticationProvider guestAuthProvider,
	                         ApiUserAuthenticationProvider authProvider,
	                         UserDetailsService userDetailsService) {

		this.guestAuthProvider = guestAuthProvider;
		this.userDetailsService = userDetailsService;
		this.authProvider = authProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().fullyAuthenticated()
				.expressionHandler(webSecurityExpressionHandler());

		http.authenticationProvider(authProvider)
				.userDetailsService(userDetailsService)
				.anonymous().authenticationFilter(
				new AnonymousAuthenticationFilter("odi") {
					@Override
					protected Authentication createAuthentication(HttpServletRequest request) {
						return GuestUser.take().asToken();
					}
				}).authenticationProvider(guestAuthProvider);

		http.csrf().disable().httpBasic();
	}

	@Bean
	public DefaultWebSecurityExpressionHandler webSecurityExpressionHandler() {
		return new DefaultWebSecurityExpressionHandler();
	}
}
