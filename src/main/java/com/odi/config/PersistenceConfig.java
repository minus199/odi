package com.odi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by minus on 4/2/17.
 */
@Configuration
@EnableTransactionManagement
public class PersistenceConfig {

}
