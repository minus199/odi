package com.odi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.session.MapSessionRepository;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.web.http.CookieHttpSessionStrategy;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionManager;
import org.springframework.session.web.http.MultiHttpSessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by minus on 4/2/17.
 */
@Configuration
@EnableSpringHttpSession
public class WebConfig extends WebMvcConfigurerAdapter {
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Bean
	SessionRepository sessionRepository() {
		return new MapSessionRepository();
	}

	@Component
	class Sess implements MultiHttpSessionStrategy, HttpSessionManager {
		private final HeaderHttpSessionStrategy headerHttpSessionStrategy = new HeaderHttpSessionStrategy();
		private final CookieHttpSessionStrategy cookieHttpSessionStrategy = new CookieHttpSessionStrategy();

		@Override
		public String getCurrentSessionAlias(HttpServletRequest request) {
			return cookieHttpSessionStrategy.getCurrentSessionAlias(request);
		}

		@Override
		public Map<String, String> getSessionIds(HttpServletRequest request) {
			return cookieHttpSessionStrategy.getSessionIds(request);
		}

		@Override
		public String encodeURL(String url, String sessionAlias) {
			return cookieHttpSessionStrategy.encodeURL(url, sessionAlias);
		}

		@Override
		public String getNewSessionAlias(HttpServletRequest request) {
			return cookieHttpSessionStrategy.getNewSessionAlias(request);
		}

		@Override
		public String getRequestedSessionId(HttpServletRequest request) {
			String sessionId = headerHttpSessionStrategy.getRequestedSessionId(request);
			if (sessionId != null && ! sessionId.isEmpty())
				return sessionId;

			// cookie part
			Map<String, String> sessionIds = getSessionIds(request);
			String sessionAlias = getCurrentSessionAlias(request);
			return sessionIds.get(sessionAlias);
		}

		@Override
		public void onNewSession(Session session, HttpServletRequest request, HttpServletResponse response) {
			headerHttpSessionStrategy.onNewSession(session, request, response);
			cookieHttpSessionStrategy.onNewSession(session, request, response);
		}

		@Override
		public void onInvalidateSession(HttpServletRequest request, HttpServletResponse response) {
			headerHttpSessionStrategy.onInvalidateSession(request, response);
			cookieHttpSessionStrategy.onInvalidateSession(request, response);
		}

		@Override
		public HttpServletRequest wrapRequest(HttpServletRequest request, HttpServletResponse response) {
			return cookieHttpSessionStrategy.wrapRequest(request, response);
		}

		@Override
		public HttpServletResponse wrapResponse(HttpServletRequest request, HttpServletResponse response) {
			return cookieHttpSessionStrategy.wrapResponse(request, response);
		}
	}
}
