package com.odi.config;

import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by minus on 3/29/17.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
	@Bean
	public JobRegistry jobRegistry() {
		return new MapJobRegistry();
	}

}