package com.odi.config;

import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.EnableVaadinNavigation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.vaadin.spring.http.HttpResponseFactory;
import org.vaadin.spring.http.HttpResponseFilter;
import org.vaadin.spring.http.HttpService;
import org.vaadin.spring.http.VaadinHttpService;
import org.vaadin.spring.security.annotation.EnableVaadinSecurity;

/**
 * Created by minus on 4/18/17.
 */
@Configuration
@EnableVaadin
@EnableVaadinSecurity
@EnableVaadinNavigation
public class VaadinConfig {
	@Bean
	public HttpResponseFactory httpResponseFactory() {
		return new HttpResponseFactory();
	}

	@Bean
	public HttpResponseFilter httpResponseFilter() {
		return new HttpResponseFilter();
	}

	@Bean

	public HttpService httpService() {
		return new VaadinHttpService();
	}
}
