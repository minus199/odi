package com.odi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
@ComponentScan("com.odi.*")
@EnableAutoConfiguration
public class OdiApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(OdiApplication.class, args);
	}

	// todo - why 2 contexts?

	//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(BusinessProfiler.class);
//		ctx.refresh();

//		AnnotationConfigEmbeddedWebApplicationContext

	// hibernate import sql
	//http://www.liquibase.org

	//10373  curl -vi main_admin:admin@localhost:8080/api/test1
//10374  xaut="x-auth-token: 0629337f-69b0-441d-b71f-bd4e03ac9c29"
//			10375  curl -H "${xaut}" http://localhost:8080/api/test2
//			10376  curl -H "${xaut}" http://localhost:8080/api/profile

//	curl user:main_admin@localhost:8080/api/test1

	/*
	 * Database access
	 * localhost:8080/h2-console
	 * database: jdbc:h2:mem:testdb
	 */
}





