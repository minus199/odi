package com.odi.view.components;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by minus on 4/18/17.
 */
public class ToggleButton extends Button {
	public ToggleButton(String caption, Component toggleableItem) {
		super(caption);
		addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
		addClickListener(event -> toggleableItem.setVisible(! toggleableItem.isVisible()));
		toggleableItem.setVisible(false);
	}

	public static ToggleButton make(String caption, ComponentContainer container, Component toggleableItem) {
		ToggleButton toggleButton = new ToggleButton(caption, toggleableItem);
		container.addComponent(toggleButton);
		return toggleButton;
	}
}
