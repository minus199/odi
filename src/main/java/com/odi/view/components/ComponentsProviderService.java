package com.odi.view.components;

import com.odi.model.AirLineCompany;
import com.odi.model.Airport;
import com.odi.model.Company;
import com.odi.model.product.PaymentMethod;
import com.odi.repository.IAirLineCompanyRepository;
import com.odi.repository.IAirportRepository;
import com.odi.service.ICompanyService;
import com.odi.view.Currency;
import com.vaadin.ui.ComboBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by minus on 4/18/17.
 */
@Service
public class ComponentsProviderService {
	final private static ComboBox.CaptionFilter defaultCaptionFilter = (itemCaption, filterText)
			-> itemCaption.toLowerCase().contains(filterText.toLowerCase());
	private final IAirLineCompanyRepository airLineCompanyRepository;
	private final IAirportRepository airportRepository;
	private final ICompanyService companyService;

	@Autowired
	public ComponentsProviderService(IAirLineCompanyRepository companyRepository,
	                                 IAirportRepository airportRepository,
	                                 ICompanyService companyService) {
		this.airLineCompanyRepository = companyRepository;
		this.airportRepository = airportRepository;
		this.companyService = companyService;
	}

	public <T> ComboBox<T> genericComboBox(Collection<T> items, String caption) {
		return this.genericComboBox(items, caption, false);
	}

	public <T> ComboBox<T> genericComboBox(Collection<T> items, String caption, boolean isPlaceholder) {
		ComboBox<T> genericComboBox = new ComboBox<>();

		genericComboBox.setItemCaptionGenerator(Object::toString);
		genericComboBox.setItems(defaultCaptionFilter, items);

		if (isPlaceholder)
			genericComboBox.setPlaceholder(caption);
		else
			genericComboBox.setCaption(caption);

		return genericComboBox;
	}

	public ComboBox<PaymentMethod> paymentMethod() {
		ComboBox<PaymentMethod> paymentMethod = genericComboBox(Arrays.asList(PaymentMethod.values()), "Payment Method");
		paymentMethod.setEmptySelectionAllowed(false);
		return paymentMethod;
	}

	public ComboBox<Currency> currency() {
		ComboBox<Currency> currencyComboBox = genericComboBox(Arrays.asList(Currency.values()), "Currency");
		currencyComboBox.setEmptySelectionAllowed(false);
		return currencyComboBox;
	}

	public ComboBox<Company> company() {
		return genericComboBox(companyService.getAll(), "Company");
	}

	public ComboBox<AirLineCompany> airline() {
		return genericComboBox(airLineCompanyRepository.findAll(), "AirLine Company");
	}

	public ComboBox<Airport> airport(String caption) {
		return genericComboBox(airportRepository.findAll(), caption);
	}
}
