package com.odi.view.components;

import com.odi.model.EntityBase;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.EditorSaveListener;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

/**
 * Created by minus on 4/16/17.
 * <p>
 * Editable implementation of Grid
 */
public class OdiGrid<T extends EntityBase> extends Grid<T> {
	private final AtomicReference<T> activeRow = new AtomicReference<>();

	public OdiGrid(Class<T> entityType, CallbackDataProvider<T, Predicate<List<T>>> dataProvider, EditorSaveListener<T> saveListener) {
		super(entityType);
		setCaptionAsHtml(true);
		setCaption("<h1>"+entityType.getSimpleName()+"</h1>");

		setSizeFull();
		setHeightMode(HeightMode.CSS);
		setHeight("100%");
		setSelectionMode(Grid.SelectionMode.MULTI);
		setDataProvider(dataProvider);
		getEditor().setEnabled(true);

		addItemClickListener(event -> activeRow.set(event.getItem()));
		getEditor().addSaveListener(saveListener);
	}

	public T getActiveRow() {
		return activeRow.get();
	}
}