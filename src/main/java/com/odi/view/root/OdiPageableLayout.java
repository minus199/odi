package com.odi.view.root;

import com.odi.model.EntityBase;
import com.odi.model.user.UserRole;
import com.odi.service.IServiceBase;
import com.odi.view.admin.LimitedAccessView;
import com.odi.view.components.OdiGrid;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.EditorSaveListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.vaadin.spring.security.VaadinSecurity;

import javax.annotation.PostConstruct;
import javax.util.streamex.StreamEx;
import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

/**
 * Created by minus on 4/16/17.
 */
//@SpringView
abstract public class OdiPageableLayout<T extends EntityBase> extends VerticalLayout implements LimitedAccessView {
	final private static AtomicInteger currentPage = new AtomicInteger(0);
	private final IServiceBase<T> service;
	private final Class<T> entityType;
	private VaadinSecurity vaadinSecurity;
	private OdiGrid<T> grid;
	private Map<String, Grid.Column<T, ?>> colByID;

	protected OdiPageableLayout(IServiceBase<T> service, Class<T> entityType,
	                            CallbackDataProvider.FetchCallback<T, Predicate<List<T>>> fetchCallback,
	                            CallbackDataProvider.CountCallback<T, Predicate<List<T>>> countCallback) {
		this.service = service;
		this.entityType = entityType;
		this.grid = new OdiGrid<>(entityType,
		                          new CallbackDataProvider<>(fetchCallback, countCallback),
		                          getSaveListener());
	}

	protected OdiPageableLayout(IServiceBase<T> service, Class<T> entityType) {
		this(service, entityType,
		     q -> {
			     currentPage.set(0);
			     return service.getAll(new PageRequest(currentPage.getAndIncrement(), 20)).getContent().stream();
		     },
		     q -> (int) service.count());
	}

	protected OdiPageableLayout(IServiceBase<T> service, Class<T> entityType,
	                            VaadinSecurity vaadinSecurity,
	                            CallbackDataProvider.FetchCallback<T, Predicate<List<T>>> fetchCallback,
	                            CallbackDataProvider.CountCallback<T, Predicate<List<T>>> countCallback) {
		this(service, entityType, fetchCallback, countCallback);
		this.vaadinSecurity = vaadinSecurity;
	}

	protected OdiPageableLayout(IServiceBase<T> service, Class<T> entityType, VaadinSecurity vaadinSecurity) {
		this(service, entityType);
		this.vaadinSecurity = vaadinSecurity;
	}

	protected void beforeInit() {

	}

	@PostConstruct
	private void _initGrid() throws AccessDeniedException {
		if (! Objects.isNull(this.vaadinSecurity)) {
			Optional
					.of(getClass().getAnnotation(PreAuthorize.class))
					.ifPresent(preAuthorize -> System.out.println());
		}

		beforeInit();
		addComponentsAndExpand(grid);
		afterInit();
	}

	protected void afterInit() {

	}

	final protected Grid.Column<T, ?> colByID(String name) {
		return colByID().get(name);
	}

	final protected Map<String, ? extends Grid.Column<T, ?>> colByID() {
		if (colByID == null) {
			this.colByID = StreamEx.of(getGrid().getColumns()).toMap(Grid.Column::getId, col -> col);
		}

		return colByID;
	}

	protected EditorSaveListener<T> getSaveListener() {
		return event -> {
			T activeRow = getGrid().getActiveRow();
			service.save(activeRow);
			Notification notification = new Notification(entityType.getSimpleName() + " "
			                                             + activeRow.toString() + " was saved!");
			notification.setDelayMsec(1500);
			notification.show(UI.getCurrent().getPage());
		};
	}

	final public OdiGrid<T> getGrid() {
		return grid;
	}

	final protected AtomicInteger getCurrentPage() {
		return currentPage;
	}

	final public IServiceBase<T> getService() {
		return service;
	}

	public Class<T> getEntityType() {
		return entityType;
	}

	@Override
	public List<UserRole> getRequiredRoles() {
		return null;
	}
}
