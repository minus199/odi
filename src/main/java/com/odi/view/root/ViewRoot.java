package com.odi.view.root;

import com.odi.service.users.domain.GuestUser;
import com.odi.view.root.login.LoginView;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Objects;

/**
 * Created by minus on 4/13/17.
 */

@Theme("valo")
@SpringUI()
@SpringViewDisplay
public class ViewRoot extends UI implements ViewDisplay {
	private final LoginView loginView;
	private final boolean debug;
	private final boolean loginEnabled;
	private final boolean horizontalNavEnabled;

	private Panel springViewDisplay = new Panel();

	@Autowired
	public ViewRoot(LoginView loginView,
	                @Value("${odi.system.vaadin.debug}") Integer debug,
	                @Value("${odi.system.vaadin.login_enabled}") Integer loginEnabled,
	                @Value("${odi.system.vaadin.horizontal_navigation_on}") Integer horizontalNavEnabled) {

		this.loginView = loginView;
		this.debug = debug > 0;
		this.loginEnabled = loginEnabled > 0;
		this.horizontalNavEnabled = horizontalNavEnabled > 0;
	}

	@Override
	public void showView(View view) {
		springViewDisplay.setContent((Component) view);
	}

	@Override
	protected void init(VaadinRequest request) {
//		Responsive.makeResponsive(this);
		doLogin();
	}

	/**
	 * disable login form and use Guest user by default with property odi.system.login_enabled
	 */
	private boolean doLogin() {
		if (! loginEnabled) {
			VaadinSession.getCurrent().setAttribute("user-details", GuestUser.take().asToken());
			return true;
		}

		try {
			if (! Objects.isNull(VaadinSession.getCurrent().getAttribute("user-details"))) return true;
		} catch (Exception ignored) { }

		loginView.build(userDetails -> {
			VaadinSession.getCurrent().setAttribute("user-details", userDetails);
			postLogin();
		});

		setContent(loginView);
		return true;
	}

	private void postLogin() {
		NavigationBar navigationBar = new NavigationBar(horizontalNavEnabled);
		final AbstractOrderedLayout root = horizontalNavEnabled ? new HorizontalLayout() : new VerticalLayout();
		root.setWidth(100, Unit.PERCENTAGE);
		root.setHeight(100, Unit.PERCENTAGE);

		root.addComponent(navigationBar);

		springViewDisplay.setSizeFull();
		root.addComponent(springViewDisplay);
		root.setExpandRatio(springViewDisplay, 1.0f);
		setContent(root);

		if (! debug) getNavigator().setErrorView(new ErrorView());
	}

	private class ErrorView extends VerticalLayout implements View {
		public final static String VIEW_NAME = "ErrorView ";

		@Override
		public void enter(ViewChangeListener.ViewChangeEvent event) {
			addComponent(new Label("This is the " + VIEW_NAME));
		}
	}
}
