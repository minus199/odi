package com.odi.view.root;

import com.odi.model.KV;
import com.odi.view.Products.ProductsView;
import com.odi.view.admin.UsersView;
import com.odi.view.company.CompanyView;
import com.odi.view.flight.FlightsView;
import com.odi.view.proposals.ProposalView;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.themes.ValoTheme;

import java.nio.file.AccessDeniedException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by minus on 4/14/17.
 */
public class NavigationBar extends CssLayout {
	public final static String ACTIVE_PAGE_BUTTON = "ACTIVE_PAGE_BUTTON";
	private final Map<String, Button> buttons = new HashMap<>();

	public NavigationBar(boolean horizontalNavEnabled) {
		super();
		setStyles(horizontalNavEnabled);
		makeButtons(horizontalNavEnabled);
	}

	private void makeButtons(boolean horizontalNavEnabled) {
		Stream.of(KV.of(UsersView.DISPLAY_NAME, UsersView.VIEW_NAME),
		          KV.of(CompanyView.DISPLAY_NAME, CompanyView.VIEW_NAME),
		          KV.of(FlightsView.DISPLAY_NAME, FlightsView.VIEW_NAME),
		          KV.of(ProposalView.DISPLAY_NAME, ProposalView.VIEW_NAME),
		          KV.of(ProductsView.DISPLAY_NAME, ProductsView.VIEW_NAME),
		          KV.of("Booking", "Booking"))
				.map(btn -> createNavigationButton(btn, horizontalNavEnabled)).filter(Objects::nonNull)
				.forEach(this::addComponent);

		Button logoutButton = new Button("Logout", event -> {

			VaadinSession.getCurrent().getService().fireSessionDestroy(VaadinSession.getCurrent());
		});
		if (horizontalNavEnabled) logoutButton.setPrimaryStyleName(ValoTheme.MENU_ITEM);
		logoutButton.addStyleName(ValoTheme.BUTTON_DANGER);

		addComponent(logoutButton);
	}

	private Button createNavigationButton(KV<String, String> viewDisplayNames, boolean horizontalNavEnabled) {
		Button button = new Button
				(viewDisplayNames.getK(), event -> updateSessionAfterClick(viewDisplayNames.getV(), event));

		try {
			authorizeButton(button);
		} catch (AccessDeniedException e) {
			return null;
		}

		button.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
		if (horizontalNavEnabled) button.setPrimaryStyleName(ValoTheme.MENU_ITEM);
		highlightBySession(button);

		buttons.put(button.getCaption(), button);
		return button;
	}

	private void authorizeButton(Button button) throws AccessDeniedException {
		// todo check authority for button		//					if (request.isUserInRole("ROLE_GUEST");)
	}

	private void updateSessionAfterClick(String displayName, Button.ClickEvent event) {
		// Remove previously selected button
		Optional.of(String.valueOf(VaadinSession.getCurrent().getAttribute(ACTIVE_PAGE_BUTTON)))
				.flatMap(sessBtnName -> buttons.entrySet().stream().filter(es -> es.getKey().equals(sessBtnName)).findFirst())
				.ifPresent(stringButtonEntry -> stringButtonEntry.getValue().removeStyleName("selected"));

		// Update session state
		VaadinSession.getCurrent().setAttribute(ACTIVE_PAGE_BUTTON, event.getButton().getCaption());
		event.getButton().addStyleName("selected");

		getUI().getNavigator().navigateTo(displayName);
	}

	private void highlightBySession(Button button) {
		try {
			// Keep navigation state in session -- load button from session and make current button same state
			Optional
					.of(String.valueOf(VaadinSession.getCurrent().getAttribute(ACTIVE_PAGE_BUTTON)))
					.filter(sessBtn -> sessBtn.equals(button.getCaption()))
					.ifPresent(sessBtn -> button.addStyleName("selected"));
		} catch (Exception ignored) {}
	}

	private void setStyles(boolean horizontalNavEnabled) {
		if (! horizontalNavEnabled) {
			addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		} else {
			setPrimaryStyleName(ValoTheme.MENU_ROOT);
			addStyleName(ValoTheme.UI_WITH_MENU);
		}

		Responsive.makeResponsive(this);
	}
}
