package com.odi.view.root.login;

/**
 * Created by minus on 4/17/17.
 */

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
public class LoginView extends VerticalLayout implements View {
	private final LoginGraphics loginGraphics;
	private final LoginForm loginForm;
	private final LoginCallback.LoginClickListener loginClickListener;

	@Autowired
	public LoginView(LoginCallback.LoginClickListener loginClickListener) {
		this.loginClickListener = loginClickListener;
		this.loginGraphics = new LoginGraphics();
		this.loginForm = new LoginForm();

		setSizeFull();
	}

	public void build(LoginCallback afterLogin) {
		if (getComponentCount() == 0) {
			loginClickListener.afterLoginCallbackRef.set(afterLogin);
			addComponents(loginGraphics, loginForm.build(loginClickListener));
			setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
		}
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}
}
