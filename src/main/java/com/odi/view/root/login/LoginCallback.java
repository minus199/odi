package com.odi.view.root.login;

import com.odi.service.users.impl.OdiUserDetailsService;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button;
import com.vaadin.util.ReflectTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by minus on 4/19/17.
 */
public interface LoginCallback {
	void run(UserDetails userDetails);

	@Component
	@Scope("prototype")
	final class LoginClickListener implements Serializable {
		static final Method BUTTON_CLICK_METHOD = ReflectTools
				.findMethod(LoginClickListener.class, "newLogin", LoginButtonClickEvent.class);

		final AtomicReference<LoginCallback> afterLoginCallbackRef = new AtomicReference<>();
		private final UserDetailsService userService;
		private final AuthenticationManager authenticationManager;

		@Autowired
		LoginClickListener(OdiUserDetailsService userService, AuthenticationManager authenticationManager) {
			this.userService = userService;
			this.authenticationManager = authenticationManager;
		}

		public void newLogin(LoginButtonClickEvent event) {
			try {
				UserDetails userDetails = userService
						.loadUserByUsername(String.valueOf(event.authenticationToken.getPrincipal()));

				authenticationManager.authenticate(event.authenticationToken);
				SecurityContextHolder.getContext().setAuthentication(event.authenticationToken);

				afterLoginCallbackRef.get().run(userDetails);
				afterLoginCallbackRef.set(ud -> {});
			} catch (AuthenticationException exception) {
				event.parentEvent.getButton().setComponentError(new UserError(exception.getMessage()));
			}
		}
	}

	static final class LoginButtonClickEvent extends com.vaadin.ui.Component.Event {
		private final UsernamePasswordAuthenticationToken authenticationToken;
		private final Button.ClickEvent parentEvent;

		LoginButtonClickEvent(Button.ClickEvent parentEvent, UsernamePasswordAuthenticationToken authenticationToken) {
			super(parentEvent.getButton());
			this.authenticationToken = authenticationToken;
			this.parentEvent = parentEvent;
		}
	}
}
