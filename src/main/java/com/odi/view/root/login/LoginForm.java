package com.odi.view.root.login;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static com.odi.view.root.login.LoginCallback.LoginButtonClickEvent;
import static com.odi.view.root.login.LoginCallback.LoginClickListener;

/**
 * Created by minus on 4/19/17.
 */
class LoginForm extends VerticalLayout {
	final private TextField username = new TextField("Username");
	final private PasswordField password = new PasswordField("Password");
	final private Button signIn = new SubmitLoginButton();

	LoginForm() {
		setSizeUndefined();
		setSpacing(true);
		addStyleName("login-panel");
	}

	LoginForm build(LoginClickListener loginCallback) {

		HorizontalLayout fields = new HorizontalLayout(userName(), password());
		fields.setSpacing(true);
		fields.addStyleName("fields");

		addComponents(fields, signIn(loginCallback), new CheckBox("Remember me", true));
		setComponentAlignment(signIn, Alignment.MIDDLE_RIGHT); // after adding to layout

		return this;
	}

	private TextField userName() {
		username.focus();
		username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		username.setErrorHandler(event -> {});

		return username;
	}

	private PasswordField password() {
		password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		password.setErrorHandler(event -> {});
		return password;
	}

	private Button signIn(LoginClickListener loginCallback) {
		signIn.addListener(LoginButtonClickEvent.class, loginCallback, LoginClickListener.BUTTON_CLICK_METHOD);
		return signIn;
	}

	private final class SubmitLoginButton extends Button {
		SubmitLoginButton() {
			super("Sign In");

			addStyleName(ValoTheme.BUTTON_PRIMARY);

			setClickShortcut(ShortcutAction.KeyCode.ENTER);
			addClickListener(this::fireSubmitLoginEvent);

			setErrorHandler(event -> {
				Notification notification = new Notification("Incorrect credentials.");
				notification.setDelayMsec(3000);
				notification.setStyleName(ValoTheme.NOTIFICATION_ERROR);

				notification.setDescription(event.getThrowable().getMessage());
				notification.show(UI.getCurrent().getPage());
			});
		}

		private void fireSubmitLoginEvent(Button.ClickEvent event) {
			UsernamePasswordAuthenticationToken authenticationToken
					= new UsernamePasswordAuthenticationToken(username.getValue(), password.getValue());
			fireEvent(new LoginButtonClickEvent(event, authenticationToken));
		}
	}

	String getUsername() {
		return username.getValue();
	}

	String getPassword() {
		return password.getValue();
	}
}
