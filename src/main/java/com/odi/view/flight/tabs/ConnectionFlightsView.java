package com.odi.view.flight.tabs;

import com.odi.model.ConnectedFlight;
import com.odi.service.IConnectedFlightService;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by minus on 4/13/17.
 */
@VaadinSessionScope()
@SpringComponent
public class ConnectionFlightsView extends OdiPageableLayout<ConnectedFlight> {
	@Autowired
	public ConnectionFlightsView(IConnectedFlightService connectedFlightService) {
		super(connectedFlightService, ConnectedFlight.class);
	}
}
