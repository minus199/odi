package com.odi.view.flight.tabs;

import com.odi.model.AirLineCompany;
import com.odi.model.Airport;
import com.odi.model.Flight;
import com.odi.model.Flight2;
import com.odi.view.Currency;
import com.odi.view.components.ComponentsProviderService;
import com.vaadin.data.*;
import com.vaadin.data.converter.LocalDateTimeToDateConverter;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

/**
 * Created by minus on 4/16/17.
 */

public class AddFlightForm extends VerticalLayout {

	final private DateTimeField takeoffDate = new DateTimeField();
	final private DateTimeField landingDate = new DateTimeField();
	final private TextField flightDuration = new TextField();

	private final Binder<Flight2> binder = new Binder<>(Flight2.class);

	public AddFlightForm(ComponentsProviderService componentsProvider) {

		binder.setBean(new Flight2());

		TextField flightID = new TextField("flightID");
		flightID.setVisible(false);

		ComboBox<AirLineCompany> airline = componentsProvider.airline();
		binder.forField(airline).bind(Flight2::getAirline, Flight2::setAirline);

		ComboBox<Currency> currencyComboBox = componentsProvider.currency();
		binder.forField(currencyComboBox).bind(Flight2::getCurrency, Flight2::setCurrency);

		ComboBox<Airport> originAirport = componentsProvider.airport("Origin");
		binder.forField(originAirport).bind(Flight2::getOriginAirport, Flight2::setOriginAirport);

		ComboBox<Airport> destinationAirport = new ComboBox<>("Destination");
		binder.forField(destinationAirport).bind(Flight2::getDestinationAirport, Flight2::setDestinationAirport);

		datesAndTime();
		flightDuration.setEnabled(false);

		addComponents(airline,
		              new HorizontalLayout(originAirport, destinationAirport),
		              new HorizontalLayout(takeoffDate, landingDate),
		              new HorizontalLayout(flightDuration, cost(), currencyComboBox),
		              submitButton());
	}

	private void datesAndTime() {
		HasValue.ValueChangeListener<LocalDateTime> timeValueChangeListener = (HasValue.ValueChangeListener<LocalDateTime>) event -> {
			if (! takeoffDate.isEmpty() && ! landingDate.isEmpty()) {
				String timeDiff = String
						.format("%02d:%02d:00",
						        takeoffDate.getValue().until(landingDate.getValue(), ChronoUnit.DAYS) * 24,
						        takeoffDate.getValue().until(landingDate.getValue(), ChronoUnit.MINUTES) % 60);

				flightDuration.clear();
				flightDuration.setValue(timeDiff);
			}
		};
		takeoffDate.setPlaceholder("Takeoff Date");

		takeoffDate.addValueChangeListener(timeValueChangeListener);
		takeoffDate.addValueChangeListener(event -> {
			landingDate.setRangeEnd(takeoffDate.getValue().plusHours(99));
			landingDate.setRangeStart(takeoffDate.getValue());
		});
		binder.forField(takeoffDate)
				.withConverter(new LocalDateTimeToDateConverter(ZoneOffset.UTC))
				.bind(Flight2::getTakeoffDate, Flight2::setTakeoffDate);

		landingDate.addValueChangeListener(timeValueChangeListener);
		landingDate.setPlaceholder("Landing Date");
		binder.forField(landingDate)
				.withConverter(new LocalDateTimeToDateConverter(ZoneOffset.UTC))
				.bind(Flight2::getLandingDate, Flight2::setLandingDate);

		flightDuration.setPlaceholder("Flight Duration");
		binder.forField(flightDuration).withConverter(new Converter<String, Time>() {
			@Override
			public Result<Time> convertToModel(String value, ValueContext context) {
				try {
					return Result.ok(Time.valueOf(value));
				} catch (IllegalArgumentException e) {
					return Result.error("Fight seems a bit too long; Are the dates correct?");
				}
			}

			@Override
			public String convertToPresentation(Time value, ValueContext context) {
				try { return value.toString(); } catch (Exception e) { return ""; }
			}
		}).bind(Flight2::getFlightDuration, Flight2::setFlightDuration);
	}

	private TextField cost() {
		TextField cost = new TextField();
		cost.setPlaceholder("Cost");
		Validator<String> stringValidator = (value, context) -> {
			try {
				return Double.valueOf(value) > 0
						? ValidationResult.ok()
						: ValidationResult.error("Flight cost must be specified.");
			} catch (NumberFormatException ex) {
				return ValidationResult.error("Flight cost must be specified.");
			}
		};

		binder.forField(cost)
				.withValidator(stringValidator)
				.withConverter(new StringToDoubleConverter("Flight cost must be specified."))
				.bind(Flight2::getCost, Flight2::setCost);
		return cost;
	}

	private Button submitButton() {
		Button submit = new Button("Save");
		submit.addClickListener(event -> {
			Flight2 flight2 = binder.getBean();
			Flight flight = new Flight();
			flight.setFlightTime(flight2.getTakeoffDate());
			flight.setFlightLanding(flight2.getLandingDate());
			flight.setAirlineId(flight2.getAirline().getId());
			flight.setFlightNumber(flight2.getFlightNumber());
			flight.setOriginAirportId(flight2.getOriginAirport().getCityId());
			flight.setDestinationAirportId(flight2.getDestinationAirport().getId());
			Calendar instance = Calendar.getInstance();
			instance.setTime(flight2.getTakeoffDate());
			flight.setDayOfWeek(DayOfWeek.of(instance.get(Calendar.DAY_OF_WEEK)));

			flight.setTempFlightCost(flight2.getCost().intValue());
			flight.setFlightDuration(flight2.getFlightDuration().toString());

			System.out.println();
		});
		return submit;
	}

	public Binder<Flight2> getBinder() {
		return binder;
	}
}
