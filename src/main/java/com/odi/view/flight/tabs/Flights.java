package com.odi.view.flight.tabs;

import com.odi.model.Flight;
import com.odi.model.Flight2;
import com.odi.repository.IAirLineCompanyRepository;
import com.odi.repository.IAirportRepository;
import com.odi.service.IEventService;
import com.odi.service.IFlightService;
import com.odi.view.Currency;
import com.odi.view.components.ToggleButton;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.LocalDateTimeToDateConverter;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by minus on 4/18/17.
 */
@VaadinSessionScope()
@SpringComponent
public class Flights extends OdiPageableLayout<Flight> {

	@Autowired
	protected Flights(IFlightService service,
	                  IAirLineCompanyRepository airlineCompanyRepo,
	                  IEventService eventService,
	                  IAirportRepository airportRepo) {

		super(service, Flight.class);
		dashboard(eventService);
		getGrid().addItemClickListener(event -> {
			// todo -- debuging, convert from previous flight pojo to newer one
			// when an item in the grid is clicked, the form will be populated with data from selected item for editing
			Flight flight = event.getItem();
			Flight2 flight2 = new Flight2();

			flight2.setAirline(airlineCompanyRepo.findOne(flight.getAirlineId()));
			flight2.setOriginAirport(airportRepo.findOne(flight.getOriginAirportId()));
			flight2.setDestinationAirport(airportRepo.findOne(flight.getDestinationAirportId()));

			flight2.setTakeoffDate(flight.getFlightTime());
			flight2.setLandingDate(flight.getFlightLanding());
			try {
				flight2.setFlightDuration(Time.valueOf(flight.getFlightDuration()));
			} catch (Exception e) {

			}

			flight2.setFlightNumber(flight.getFlightNumber());

			flight2.setCost((double) flight.getTempFlightCost());
			flight2.setCurrency(Currency.USD);

//			addFlight.getBinder().readBean(flight2);
		});
	}

	private HorizontalLayout dashboard(IEventService eventService) {
		Button delete = new Button("Delete");
		delete.addClickListener(event -> {
			getService().deleteInBatch(new ArrayList<>(getGrid().getSelectedItems()));
			successCallback();
		});

		Button cancelled = new Button("Cancelled");
		cancelled.addClickListener(event -> {
			getGrid().getSelectedItems().forEach(flight -> eventService.flightCanceled(flight.getId(), new Date()));
			successCallback();
		});

		Button landed = new Button("Landed");
		landed.addClickListener(event -> {
			getGrid().getSelectedItems().forEach(flight -> eventService.flightLanded(
					flight.getId(), new Date(),
					flight.getFlightTime(), flight.getFlightLanding()));

			successCallback();
		});

		CssLayout dashboard = new CssLayout(delete, cancelled, landed);
		dashboard.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

		HorizontalLayout dashboardContainer = new HorizontalLayout();
		ToggleButton.make("Flight dashboard", dashboardContainer, dashboard);
		dashboardContainer.addComponent(dashboard);
		addComponents(dashboardContainer);

		return dashboardContainer;
	}

	private void successCallback() {
		Notification notification = new Notification("Done.");
		notification.setDelayMsec(2000);
		notification.setStyleName(ValoTheme.NOTIFICATION_SUCCESS);
		notification.show(UI.getCurrent().getPage());

		getGrid().getDataProvider().refreshAll();
	}

	private void makeBindings() {
		Binder<Flight> binder = getGrid().getEditor().getBinder();

		DateTimeField flightTimeEditing = new DateTimeField();
		Binder.Binding<Flight, Date> flightTimeBind = binder.forField(flightTimeEditing)
				.withConverter(new LocalDateTimeToDateConverter(ZoneOffset.UTC))
				.bind(Flight::getFlightTime, Flight::setFlightTime);
		colByID("flightTime").setEditorBinding(flightTimeBind);

		DateTimeField flightLandingEditing = new DateTimeField();
		Binder.Binding<Flight, Date> flightLandingBind = binder.forField(flightLandingEditing)
				.withConverter(new LocalDateTimeToDateConverter(ZoneOffset.UTC))
				.bind(Flight::getFlightLanding, Flight::setFlightLanding);

		colByID("flightLanding").setEditorBinding(flightLandingBind);

		TextField airlineIdEditing = new TextField();
		Binder.Binding<Flight, String> airlineIdBind = binder
				.bind(airlineIdEditing, Flight::getAirlineId, Flight::setAirlineId);
		colByID("airlineId").setEditorBinding(airlineIdBind);

		TextField flightNumberEditing = new TextField();
		Binder.Binding<Flight, Integer> flightNumberBind = binder
				.forField(flightNumberEditing)
				.withConverter(new StringToIntegerConverter("failed"))
				.bind(Flight::getFlightNumber, Flight::setFlightNumber);
		colByID("flightNumber").setEditorBinding(flightNumberBind);

		TextField originAirportIdEditing = new TextField();
		Binder.Binding<Flight, String> originAirportIdBind = binder
				.bind(originAirportIdEditing, Flight::getOriginAirportId, Flight::setOriginAirportId);
		colByID("originAirportId").setEditorBinding(originAirportIdBind);

		TextField destinationAirportIdEditing = new TextField();
		Binder.Binding<Flight, String> destinationAirportIdBind = binder
				.bind(destinationAirportIdEditing, Flight::getDestinationAirportId, Flight::setDestinationAirportId);
		colByID("destinationAirportId").setEditorBinding(destinationAirportIdBind);

		ComboBox<DayOfWeek> dayOfWeekComboBox = new ComboBox<>();
		dayOfWeekComboBox.setItemCaptionGenerator(Enum::name);
		dayOfWeekComboBox.setEmptySelectionAllowed(false);
		dayOfWeekComboBox.setWidth(100.0f, Unit.PERCENTAGE);
		dayOfWeekComboBox.setItems(DayOfWeek.values());

		Binder.Binding<Flight, DayOfWeek> weekDayBinding = binder.forField(dayOfWeekComboBox)
				.bind(Flight::getDayOfWeek, Flight::setDayOfWeek);
		colByID("dayOfWeek").setEditorBinding(weekDayBinding);

		/*

		dayOfWeek
		tempFlightCost
		flightDuration
		flightDurationHours
		flightDurationMinutes*/
		/*TextField dayOfWeekEditing = new TextField();
		Binder.Binding<Flight, DayOfWeek> dayOfWeekBind = binder.bind(dayOfWeekEditing, Flight::getDayOfWeek, Flight::setDayOfWeek);
		TextField tempFlightCostEditing = new TextField();
		Binder.Binding<Flight, Integer> tempFlightCostBind = binder.bind(tempFlightCostEditing, Flight::getTempFlightCost, Flight::setTempFlightCost);
		TextField flightDurationEditing = new TextField();
		Binder.Binding<Flight, String> flightDurationBind = binder.bind(flightDurationEditing, Flight::getFlightDuration, Flight::setFlightDuration);
		TextField flightDurationHoursEditing = new TextField();
		Binder.Binding<Flight, int> flightDurationHoursBind = binder.bind(flightDurationHoursEditing, Flight::getFlightDurationHours, Flight::setFlightDurationHours);
		TextField flightDurationMinutesEditing = new TextField();
		Binder.Binding<Flight, int> flightDurationMinutesBind = binder.bind(flightDurationMinutesEditing, Flight::getFlightDurationMinutes, Flight::setFlightDurationMinutes);*/

	}

	@Override
	protected void afterInit() {
		makeBindings();
	}
}
