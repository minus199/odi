package com.odi.view.flight;

import com.odi.view.components.ComponentsProviderService;
import com.odi.view.flight.tabs.AddFlightForm;
import com.odi.view.flight.tabs.ConnectionFlightsView;
import com.odi.view.flight.tabs.Flights;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by minus on 4/13/17.
 */
@SpringView(name = FlightsView.VIEW_NAME)
//@PreAuthorize("hasRole('ROLE_ADMIN')")
public class FlightsView extends VerticalLayout implements View {
	public static final String DISPLAY_NAME = "Flights";
	public static final String VIEW_NAME = "FlightsView";

	@Autowired
	public FlightsView(ComponentsProviderService componentsProvider,
	                   Flights flightsTab,
	                   ConnectionFlightsView connectionFlightsView) {

		menu(connectionFlightsView, flightsTab, componentsProvider);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	private void menu(ConnectionFlightsView connectionFlightsView, Flights flightsTab, ComponentsProviderService componentsProvider) {
		AddFlightForm addFlight = new AddFlightForm(componentsProvider);

		TabSheet tabSheet = new TabSheet();
		tabSheet.addTab(addFlight, "Add a new Flight");
		tabSheet.addTab(flightsTab, "Flights");
		tabSheet.addTab(connectionFlightsView, "Connection Flights");

		setSpacing(true);
		setMargin(true);
		addComponentsAndExpand(tabSheet);
	}
}