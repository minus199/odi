package com.odi.view;

/**
 * Created by minus on 4/18/17.
 */
public enum Currency {
	USD('\u0024'), EURO('\u20AC'), NIS('\u20AA');

	private final char sign;

	Currency(char sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return String.format("%s %s", Character.toString(sign), name());
	}

	public char getSign() {
		return sign;
	}

}