package com.odi.view.company;

import com.odi.model.Company;
import com.odi.model.offer.Proposal;
import com.odi.model.product.Product;
import com.odi.repository.IProductRepository;
import com.odi.repository.IProposalRepository;
import com.odi.service.ICompanyService;
import com.odi.service.IFlightService;
import com.odi.service.model.FlightInfo;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.CallbackDataProvider;
import com.vaadin.server.Responsive;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.components.grid.ItemClickListener;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by minus on 4/18/17.
 */
public class Dashboard extends TabSheet implements ItemClickListener<Company> {
	private final IProductRepository productRepository;
	private final IFlightService flightService;
	private final IProposalRepository proposalRepository;
	private final Map<String, Grid> grids = new ConcurrentHashMap<>();
	private List<Tab> dashboardTabs = new ArrayList<>();
	private Company companyPojo;

	public Dashboard(ICompanyService service, IProductRepository productRepository, IFlightService flightService,
	                 IProposalRepository proposalRepository) {
		super();
		this.productRepository = productRepository;
		this.flightService = flightService;
		this.proposalRepository = proposalRepository;

		FormLayout components = new NewCompany(service).makeForm();
		addTab(components, "Add a new company");
		buildDashboard();
		Responsive.makeResponsive(this);
	}

	private <E, T> Grid<E> buildGrid(Class<E> entityType,
	                                 CallbackDataProvider.FetchCallback<E, T> fetchCallback, CallbackDataProvider.CountCallback<E, T> countCallback,
	                                 ValueProvider<E, T> idProvider) {

		Grid<E> currentGrid = new Grid<>(entityType);
		currentGrid.setSizeFull();

		CallbackDataProvider<E, T>/*phone home*/ productDataProvider = new CallbackDataProvider<>(fetchCallback, countCallback);

		currentGrid.setDataProvider(productDataProvider);
		Tab tab = addTab(currentGrid, entityType.getSimpleName());
		tab.setVisible(false);
		dashboardTabs.add(tab);

		grids.put(entityType.getSimpleName(), currentGrid);
		return currentGrid;
	}

	private void buildDashboard() {
		buildGrid(Product.class,
		          query -> productRepository.findProductByCompanyId(companyPojo.getId()).stream(),
		          query -> (int) productRepository.countProductByCompanyId(companyPojo.getId()),
		          Product::getId);

		buildGrid(Proposal.class,
		          query -> proposalRepository.findByCompanyId(companyPojo.getId()).stream(),
		          query -> (int) proposalRepository.countProposalsByCompanyId(companyPojo.getId()),
		          Proposal::getId);

		buildGrid(FlightInfo.class,
		          query -> {
			          AtomicInteger currentPage = new AtomicInteger(0);
			          return flightService
					          .getAll(new PageRequest(currentPage.getAndIncrement(), 20))
					          .getContent().stream().map(flightService::flight2FlightInfo);
		          }, query -> (int) flightService.count(),
		          FlightInfo::getId);
	}

	@Override
	public void itemClick(Grid.ItemClick<Company> event) {
		this.companyPojo = event.getItem();
//		refreshAll();
		dashboardTabs.forEach(tab -> tab.setVisible(companyPojo != null));
	}

	void refreshAll() {
		grids.forEach((name, grid) -> grid.getDataProvider().refreshAll());
	}
}
