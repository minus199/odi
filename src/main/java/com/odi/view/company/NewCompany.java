package com.odi.view.company;

import com.odi.model.Company;
import com.odi.service.IServiceBase;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * Created by minus on 4/16/17.
 */
class NewCompany extends FormLayout {
	private final IServiceBase<Company> serviceBase;

	NewCompany(IServiceBase<Company> serviceBase) {
		super();
		this.serviceBase = serviceBase;
		setSpacing(true);
		setCaption("Add a new company");
		setMargin(new MarginInfo(true, false));
	}

	NewCompany makeForm() {
		Binder<Company> binder = new Binder<>();
		TextField companyName = new TextField("Name");
		binder.forField(companyName)
				.asRequired("Name may not be empty")
				.bind(Company::getName, Company::setName);

		TextField companyAddress = new TextField("Address");
		binder.forField(companyAddress)
				.asRequired("Address may not be empty")
				.withValidator(new StringLengthValidator("Please enter a valid address", 10, null))
				.bind(Company::getAddress, Company::setAddress);

		TextField phoneNumber = new TextField("Phone#");
		binder.forField(phoneNumber)
				.withValidator(new StringLengthValidator("Please enter a valid phone number", 7, 15))
				.bind(Company::getTelNum, Company::setTelNum);

		TextField faxNum = new TextField("Fax#");
		binder.forField(faxNum)
				.bind(Company::getFaxNum, Company::setFaxNum);

		Label validationStatus = new Label();
		binder.setStatusLabel(validationStatus);

		binder.setBean(new Company());

		Button createBtn = new Button("Save");
		createBtn.setEnabled(false);
		createBtn.addClickListener(e -> serviceBase.save(binder.getBean()));
		binder.addStatusChangeListener(event -> createBtn.setEnabled(binder.isValid()));

		addComponents(companyName, companyAddress, phoneNumber, faxNum, createBtn, validationStatus);

		Responsive.makeResponsive(this);
		return this;
	}
}
