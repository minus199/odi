package com.odi.view.company;

import com.odi.model.Company;
import com.odi.repository.IProductRepository;
import com.odi.repository.IProposalRepository;
import com.odi.service.ICompanyService;
import com.odi.service.IFlightService;
import com.odi.view.components.ToggleButton;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;

/**
 * Created by minus on 4/18/17.
 */

@SpringView(name = CompanyView.VIEW_NAME)
public class CompanyView extends OdiPageableLayout<Company> implements View {
	public static final String DISPLAY_NAME = "Company";
	public static final String VIEW_NAME = "CompanyView";
	private final Dashboard dashboard;

	public CompanyView(ICompanyService service, IProposalRepository proposalRepository,
	                   IProductRepository productRepository, IFlightService flightService) {
		super(service, Company.class);

		dashboard = new Dashboard(service, productRepository, flightService, proposalRepository);
		ToggleButton.make("Dashboard", this, dashboard);
		getGrid().addItemClickListener(dashboard);
		addComponentsAndExpand(dashboard);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		getGrid().getDataProvider().refreshAll();
		dashboard.refreshAll();
	}
}
