package com.odi.view.admin;

import com.odi.model.user.UserRole;

import java.util.List;

/**
 * Created by minus on 4/17/17.
 */
public interface LimitedAccessView {
	List<UserRole> getRequiredRoles(); // comma delimited string like "X, Y, Z"
}
