package com.odi.view.admin;

import com.odi.model.user.User;
import com.odi.service.IUserService;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.vaadin.spring.security.VaadinSecurity;

/**
 * Created by minus on 4/13/17.
 */

@SpringView(name = UsersView.VIEW_NAME)
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class UsersView extends OdiPageableLayout<User> implements View, LimitedAccessView {
	public static final String VIEW_NAME = "UsersView";
	public static final String DISPLAY_NAME = "Users";

	@Autowired
	public UsersView(IUserService userService, VaadinSecurity security) {
		super(userService, User.class, security);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}
}