package com.odi.view.proposals;

import com.odi.model.offer.Proposal;
import com.odi.service.IFlightProposalService;
import com.odi.service.IProposalService;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by minus on 4/18/17.
 */
@SpringView(name = ProposalView.VIEW_NAME)
public class ProposalView extends OdiPageableLayout<Proposal> implements View {
	public static final String DISPLAY_NAME = "Proposal";
	public static final String VIEW_NAME = "ProposalView";

	@Autowired
	protected ProposalView(IProposalService service, IFlightProposalService flightProposalService) {
		super(service, Proposal.class);
		Button cancelBtn = new Button("Cancel selected");

		FormLayout components = new FormLayout();
		new TextField("");

		/*cancelBtn.addClickListener(event -> {
			FlightCanceledProposal fc = (FlightCanceledProposal) flightProposalService.getFlightCancelProposal(
					proposal.getFlight(),
					date,
					proposal.getCost()
			                                                                                                  );

		})*/
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	private void create(){
		FormLayout newProposal = new FormLayout();

	}
}
