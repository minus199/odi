package com.odi.view.Products;

import com.odi.model.Company;
import com.odi.model.product.PaymentMethod;
import com.odi.model.product.Product;
import com.odi.service.ICompanyService;
import com.odi.service.IProductService;
import com.odi.view.components.ComponentsProviderService;
import com.odi.view.root.OdiPageableLayout;
import com.vaadin.data.Binder;
import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by minus on 4/18/17.
 */
@SpringView(name = ProductsView.VIEW_NAME)
public class ProductsView extends OdiPageableLayout<Product> implements View {
	public static final String DISPLAY_NAME = "Product";
	public static final String VIEW_NAME = "ProductView";

	@Autowired
	protected ProductsView(IProductService service,
	                       ComponentsProviderService componentsProviderService,
	                       ICompanyService companyService) {
		super(service, Product.class);

		makeBinding(companyService, componentsProviderService);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	private void makeBinding(final ICompanyService companyService,
	                         final ComponentsProviderService componentsProviderService) {
		Binder<Product> binder = getGrid().getEditor().getBinder();

		TextField productName = new TextField("Product Name");
		Binder.Binding<Product, String> productNameBinding = binder.forField(productName)
				.withValidator(new StringLengthValidator(
						"Please enter a meaningful name which is not too long :)", 3, 20))
				.bind(Product::getName, Product::setName);
		colByID("name").setEditorBinding(productNameBinding);

		ComboBox<Company> companyComboBox = componentsProviderService.company();
		companyComboBox.setEmptySelectionAllowed(false);
		Binder.Binding<Product, String> companyBinding = binder.forField(companyComboBox)
				.withConverter(new Converter<Company, String>() {
					@Override
					public Result<String> convertToModel(Company value, ValueContext context) {
						return Result.ok(value.getId());
					}

					@Override
					public Company convertToPresentation(String value, ValueContext context) {
						return companyService.findOneByName(value).orElse(null);
					}
				})
				.bind(Product::getCompanyId, Product::setCompanyId);
		colByID("companyId").setEditorBinding(companyBinding);

		TextField defaultValue = new TextField("Default Value");
		Binder.Binding<Product, String> defaultValueBindings = binder.forField(defaultValue)
				.withValidator(new StringLengthValidator("Enter sometthing", 0, 20))
				.bind(Product::getDefaultValue, Product::setDefaultValue);
		colByID("defaultValue").setEditorBinding(defaultValueBindings);

		TextField description = new TextField();
		Binder.Binding<Product, String> descriptionBindings = binder
				.forField(description).bind(Product::getDescription, Product::setDescription);
		colByID("description").setEditorBinding(descriptionBindings);

		ComboBox<PaymentMethod> paymentMethodComboBox = componentsProviderService.paymentMethod();
		Binder.Binding<Product, PaymentMethod> pmBindings = binder.forField(paymentMethodComboBox)
				.bind(Product::getPaymentMethod, Product::setPaymentMethod);
		colByID("paymentMethod").setEditorBinding(pmBindings);

		colByID("paymentAmount");
		colByID("segment");
		colByID("templateId");
		colByID("thetemplate");
	}
}
