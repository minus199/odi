var FiltersLoader = function()
{
    var cache_key = 'ED_FILT_CACHE';

    var no_cache = lscache === null || typeof(lscache) != 'object';
    no_cache = true;

    var self = this;

    self.init = function(cb)
    {
        var cached = try_from_cache();

        if(!cached)
        {
            ODIApi.get('wapi/get_global_data', {}, function (data) {
                self.init_all(data);
                save_to_cache();
                self.invoke(cb,{data_object : self.data_object,all_data : self.all_data});
            });
        }
        else
        {
            self.all_data = cached.all_data;
            self.data_object = cached.data_object;

            self.invoke(cb,cached);
        }
    }

    self.all_data = null;
    self.data_object = {
        airports_data : null,
        airports_map : null
    };


    self.invoke = function(cb,data)
    {
        if(typeof(cb) == 'function')
            cb(data);
    }

    self.init_all = function(data) {
        var airports_data = [];
        var airports_map = {};
        var city_map = {};

        self.all_data = data;

        for (var country_id in self.all_data.countries) {
            var country = self.all_data.countries[country_id];
            var cities = self.all_data.cities[country_id];

            for (var i in cities) {
                var city = cities[i];
                var airports = self.all_data.airports[city.id];

                if (airports.length > 0) {
                    for (var j in airports) {
                        var airport = airports[j];

                        airport.city = city;
                        airport.country = country;

                        airport.value = city.name + ' (' + airport.code + ')';
                        airport.label = airport.value + ' - ' + airport.country.name;

                        airports_data.push(airport);

                        airports_map[airport.id] = airport;
                    }
                }
            }
        }

        delete self.all_data.airports;
        self.data_object.airports_map = airports_map;
        self.data_object.airports_data = airports_data;
    }

    function try_from_cache()
    {
        if(no_cache) return false;

        var data = lscache.get(cache_key);

        if(data === null)
        {
            return false;
        }

        return data;
    }

    var max_cache_minutes = 60;

    function save_to_cache()
    {
        if(no_cache) return;

        var data = {
            data_object : self.data_object,
            all_data : self.all_data
        };

        lscache.set(cache_key,data,max_cache_minutes);
    }


    self.GetAirportData = function(airportCode)
    {
        var airport = self.data_object.airports_map[airportCode];

        if(!airport) return false;

        return airport;
    }

    self.GetCityNameByAirport = function(airportCode)
    {
        var data = self.GetAirportData(airportCode);

        return data?data.city.name : false;
    }

    self.GetCountryNameByAirport = function(airportCode)
    {
        var data = self.GetAirportData(airportCode);

        return data?data.country.name : false;
    }
}

window.filtersLoader = new FiltersLoader();