
var EDFlightModel = function (data) 
{

	var d = {};
	
	for(var p in data)
	{
		if(Array.isArray(data[p]))
			data[p] = data[p][0];
	}
	
	
	var sFormat = 'YYYY-MM-DD';
    var self = this;

    self.priceSymbol = '€';
	self.fromCity = data.source_city;
	self.destCity = data.dest_city;

	self.from = data.source;
	self.dest = data.dest;

    self.passengers = data.totalpax;
    self.flightDate = data.godate;
    self.returnDate = data.returndate;
	self.adults = data.adults;
	self.infants = data.infants;
	self.children = data.children;

	self.search_params = {
		source     : data.source,
		dest     : data.dest,
		godate     : data.godate,
		returndate : data.returndate
	}

	self.formattedReturnDate = _formatDate(data.returndate);
	self.formattedGoDate = _formatDate(data.godate);

	self.formattedPassangers = get_passangers_title();

	self.step = ko.observable(1);
	self.selectedFlight = ko.observable();
	self.goProposal = ko.observable(null);
	self.returnProposal = ko.observable(null);
	self.proposals = ko.observableArray([]);

	self.body_class= ['results','details'];
	self.stepClass = ko.computed(function()
	{
		var step = self.step() - 1;

		return self.body_class[step];
	})

	var _getPriceComponent = function(c)
	{
		var nodec = parseInt(c);
		var dec = (c - nodec).toFixed(2).substring(1);
		var tot   = nodec+ dec;

		return {
			no_decimal : nodec,
			decimals   : dec,
			price      : tot,
			nice       : self.priceSymbol + tot
		}
	}

	self.finalPrice = ko.computed(function()
	{
		var flight = self.selectedFlight();
		var pax = self.passengers;
		var total = flight?flight.breakdown.total.price:0;

		total = parseFloat(total);

		var props = self.proposals();

		if(Array.isArray(props))
		{
			for(var i in props)
			{
				var prop = props[i];

				if(prop.accepted())
				{
					var ppt = pax * prop.cost;
					total += ppt;
				}
			}
		}


		var comp = _getPriceComponent(total);

		return comp.nice;
	},self);

	self.proposalsBreakDown = ko.computed(function()
	{
		var fl = self.selectedFlight();
		var props = self.proposals();
		var pax = self.passengers;

		if(!fl || !Array.isArray(props) || self.step() != 2) return null;

		var total = 0;
		var arr = [];


		for(var i in props)
		{
			var prop = props[i];

			if(prop.accepted())
			{
				var ppt = pax * prop.cost;
				total += ppt;

				arr.push({
					direction: prop.isDepart?'Departure':'Return',
					price : _getPriceComponent(prop.cost).nice,
					count : pax,
					total : _getPriceComponent(ppt).nice
				});
			}
		}

		if(arr.length == 0) return null;


		return {
			data: arr,
			total: _getPriceComponent(total).nice
		}

	},self);

	self.getProposalDirection = function($data)
	{
		return $data.isDepart?'(Departure)':'(Return)';
	}

	self.acceptProposal = function($data)
	{
		$data.accepted(true);
	}

	self.selectFlight = function($data,e)
	{
		var btn = $(e.target);

		btn.closest('li').find('input[type="radio"]').attr('checked',true).prop('checked',true);

		self.selectedFlight($data);

		self.go_to_step_2();

	};

	self.go_to_step_2 = function()
	{
		self.step(2);
		self.loading(true);

		self.load_proposals(function()
		{
			self.loading(false);
		});
	}

	self.load_proposals = function(cb)
	{
		var has_cb = typeof(cb) == 'function';

		self.returnProposal(null);
		self.goProposal(null);
		self.proposals([]);

		var flight = self.selectedFlight();


		self.checkProposal(flight.flight_data.go,self.flightDate,true,flight.is_oneway,function(proposal)
		{
			// ? handle proposal ?

			self.checkProposal(flight.flight_data.return,self.returnDate,false,flight.is_oneway,function(proposal)
			{
				// ? handle proposal ?

				if(has_cb) cb();
			});
		})

	}

	self.userName = ko.observable('');
	self.userPhone = ko.observable('');

	var validateFlight = function()
	{
		var unlen = self.userName().length > 0;
		var uplen = self.userPhone().length > 0;

		var valid = unlen && uplen;

		var err = [];

		$('.form_err').hide();


		if(!unlen)
		{
			$('.name_err').show();
		};
		if(!uplen)
		{
			$('.phone_err').show();
		}

		return valid;
	};

	self.submitFlight = function()
	{
		if(validateFlight())
		{
			self.sendProposals();
			self.goToFinish();
		}
	}

	self.goToFinish = function()
	{
		self.step(3);
	}

	self.sendProposals = function()
	{
		var props = self.proposals();
		var sent = 0;
		var failed = 0;
		var done = 0;

		for(var i in props)
		{
			var prop = props[i];

			if(prop.accepted())
			{
				sent++;

				var params = {
					proposalId : prop.id,
					user : self.userName(),
					userphone: self.userPhone()
				}

				ODIApi.post('proposal/accept',params,function(proid)
				{

					if(typeof(proid) == 'object' && proid.responseText)
						proid = proid.responseText;

					if(proid != prop.id) failed++;
					else done++;

					if(sent === (done + failed))
					{
						if(failed)
						{
							console.log('Proposal approval failed: '+prop.id);
						}

						$('html,body').animate({
							scrollTop: 0
						}, 100);
						self.step(3);
					}
				},function(err){failed++},true);
			}
		}
	}

	self.company_id = 'ODI';
	self.checkProposal = function(fldata,date,isDepart,is_one_way,cb)
	{
		var has_cb = typeof(cb) == 'function';

		if(fldata == null || !fldata.connection)
		{
			if(has_cb) cb(false);
			return;
		}


		var params = {
			template : 'MissedConnection',
			flight1id : fldata.id,
			flight2id : fldata.id2,
			date :date,
			companyid : self.company_id,
			productid : null
		};

		ODIApi.get('/proposal',params,function(data)
		{
			if(data)
			{
				self.add_proposal(data,isDepart,is_one_way);
				if(has_cb) cb(false);
			}
		},function()
		{
			if(has_cb) cb(false);
		});
	}

	self.fix_proposal = function(data,isDepart,is_one_way)
	{
		var prop =
		{
			proposal : data,
			id : data.id,
			cost: data.cost,
			accepted : ko.observable(false),
			isDepart : isDepart,
			from : isDepart?self.from:self.dest,
			dest : isDepart?self.dest:self.from,
			is_one_way : is_one_way,
		}

		return prop;
	}

	self.add_proposal = function(data,isDepart,is_one_way)
	{
		if(!isDepart) isDepart = false;
		var prop = self.fix_proposal(data,isDepart,is_one_way);

		self.proposals.push(prop);

		if(isDepart)
			self.goProposal(prop);
		else
			self.returnProposal(prop);

	}

	self.declineProposal = function($data)
	{
		$data.accepted(false);
	}

	self.removeProposal = function(isDepart)
	{
		if(!isDepart) isDepart = false;

		var props = self.proposals();
		var pr = false;

		for(var i in props)
		{
			if(isDepart == props[i].isDepart)
			{
				pr = props[i];
			}
		}

		if(isDepart)
			self.goProposal(null);
		else
			self.returnProposal(null);

		self.proposals.remove(pr);

		return pr;
	}

	function _getVerbForCount(c,v,t)
    {
        if(c > 1) v+='s';

        if(typeof(t) !== 'function')
        {
            return c + ' '+ v;
        }

        return t(c,v);
    }

	function get_passangers_title(){
		var adults = self.adults;
		var children = self.children;
		var infants= self.infants;
		var total = self.passengers;

		var text = _getVerbForCount(adults,'Adult');

		var prefix = ', ';

		if( total > adults )
        {
	        if(children > 0)
		        text += prefix + children + ' Children';

	        if(infants > 0)
		        text += prefix + infants + _getVerbForCount(infants,'Infants');
        }

        return text;
	}

	function _formatDate(d,dFormat)
    {
        if(!dFormat)
        {
	        dFormat = 'ddd DD MMM';
        }
	    return moment(d, sFormat).format(dFormat);
    }


    self.loading = ko.observable(true);
    self.data = data;

    self.creataDateString = function () {

        var dFormat = 'ddd. DD MMM, `YY';

        var returnText = _formatDate(self.flightDate,dFormat);

        if (self.returnDate && self.returnDate.length) {
            returnText += ' - ' + _formatDate(self.returnDate,dFormat);
        }

        return returnText;
    }

    self.getPassangersTitle = function () {
        var v = ' Passanger';

        if (self.passengers > 1) v += 's';

        return self.passengers + v;
    }

    self.init = function ()
    {
        self.load();
    }


    self.load = function () {
        self.loading(true);


        var params = self.data;
        ODIApi.get('/flight/options', params, self.handle_result);
    }

    self.flights = ko.observableArray([]);

    self.view_data = function($data)
    {
        console.log($data);
    }

    self.on_radio_click = function($data,e)
    {
        var id = 'it_'+$data.serial+'_'+$data.side;
        var other_side = $data.side == 1?2:1;

	    var id2 = 'it_'+$data.serial+'_'+other_side;

        var checked = $('#'+id).is(':checked');

        $('#'+id2).attr('checked',checked).prop('checked',checked);
    }

    self.handle_result = function (data)
    {
        self.loading(false);

        var items = [];
        
        var toRemove = [];
        
        for(var i = 0 ; i<data.length ;i++)
        {
            var obj = data[i];

            self.fill_airport_data(obj.goFirst);
            self.fill_airport_data(obj.goSecond);
            self.fill_airport_data(obj.returnFirst);
            self.fill_airport_data(obj.returnSecond);

            var total = obj.goCost + obj.returnCost;

            obj.price = _getPriceComponent(total);

	        obj.price.go = _getPriceComponent(obj.goCost);
	        obj.price.return = _getPriceComponent(obj.returnCost);

	        obj.breakdown = _getPriceBreakdown(total);

	        obj.flight_data = _getFlightData(obj);
	        
	        if(obj.flight_data != null && obj.flight_data !== false)
        	{
	        	items.push(data[i]);
        	}
        }
        
        
        if(items.length > 0)
    	{
        	items[0].is_smart = true;
    	}
        
        self.flights(items);
    }

    self.step = ko.observable(1);

	var _getFlightData = function(obj)
    {
    	if(obj.returnFirst != null && typeof(obj.returnFirst) == 'object') obj.oneway = false;

        var dobj = {
            is_oneway : obj.oneway,
            go : _getSingleFlightData([obj.goFirst,obj.goSecond],obj.tripGoDuration,obj.goLayover,obj.goConnected,self.flightDate),
	        return : obj.oneway?null:_getSingleFlightData([obj.returnFirst,obj.returnSecond],obj.tripReturnDuration,obj.returnLayover,obj.returnConnected,self.returnDate)
        }
        
        if(!dobj.go) return false;
        
        if(!dobj.return && dobj.go && !dobj.is_oneway) return false;
        

	    dobj.go.side = 1;
	    dobj.return.side = 2;

	    flight_serial++;

	    return dobj;

    }

	var flight_serial = 192;
    var _getSingleFlightData = function(flights,duration,layover,isConnected,date)
    {
	    var flight = flights[0];

	    if(!flight.destAirport || !flight.srcAirport) return false;

	    var obj =
		{
			id   : flight.id,
			id2  : null,
			date : _formatDate(date,'ddd, DD-MMM-YY'),
		    depart: flight.flightTime,
		    landing: flight.flightArrivalTime,
		    from : flight.srcAirport.name + ', ' + flight.srcAirport.city.name,
			fromCityCode : flight.srcAirport.city.name + '('+flight.srcAirport.id+')',
		    fromFull : flight.flightTime + ' ' +flight.srcAirport.name + ', ' + flight.srcAirport.city.name + '('+flight.srcAirport.id+')',
		    to : flight.destAirport.name + ', ' + flight.destAirport.city.name,
			toCityCode : flight.destAirport.city.name + '('+flight.destAirport.id+')',
		    toFull : flight.flightArrivalTime + ' ' +flight.destAirport.name + ', ' + flight.destAirport.city.name + '('+flight.destAirport.id+')',
		    duration : _getDurationString(duration),
		    connection : isConnected,
            stop_text  : 'direct',
            airline : {
		        name : flight.airline,
                code : flight.airlineCode,
                flight : flight.flightNumber
            },
            serial: flight_serial,
			short_connection : false
	    };

	    if(isConnected)
        {
            var flight2 = flights[1];

            obj.id2  = flight2.id;
            obj.landing = flight2.flightArrivalTime;
            obj.to = flight2.destAirport.name + ', ' + flight2.destAirport.city.name;
            obj.toCityCode  = flight2.destAirport.city.name + '('+flight2.destAirport.id+')';
            obj.toFull = flight2.flightArrivalTime + ' ' +flight2.destAirport.name + ', ' + flight2.destAirport.city.name + '('+flight2.destAirport.id+')',
            obj.stop_text = '1 stop';
	        obj.itinerary = _getConnectionItinerary(flight,flight2,layover,date,obj.duration);

	        obj.short_connection = obj.itinerary.short_connection;
        }
        else
        {
            obj.itinerary = _getSingleItinerary(flight,date,obj.duration);
        }

        return obj;

    }

    function _getDurationString(duration)
    {
        return duration.hours+'h '+pad(duration.minutes,2) + '\'';
    }

    function _getConnectionItinerary(flight,flight2,layover,date,duration)
    {
	    var first = _getSingleItinerary(flight,date,duration);
	    var second = _getSingleItinerary(flight2,date,duration);

	    var layaway_duration = _getDurationString(layover);

	    first.second = second;
	    first.layaway = {
		    duration: layaway_duration,
		    start : first.arrival.datetime,
		    end   : second.depart.datetime,
		    is_short : self.is_short_connection(layover)
	    }

	    first.short_connection = first.layaway.is_short;


	    return first;
    }

	self.is_short_connection = function(layover)
	{
		if(layover.hours < 4) return true;

		//if(layover.hours < 4 && layover.minutes <= 30) return true;
	}

    function _getSingleItinerary(flight,date,duration)
    {
        var fmtdate = _formatDate(date,'dddd, DD MMMM');

        return {
            airline : {
	            name : flight.airline,
                code : flight.airlineCode,
                flight : flight.flightNumber
            },
	        depart: {
		        date : fmtdate,
		        time : flight.flightTime,
                datetime : flight.flightTime+' '+fmtdate,
		        airport :
			        {
				        name : flight.srcAirport.name,
				        code : '('+flight.srcAirport.id+')',
				        geo  : flight.srcAirport.city.name + ' - ' +flight.srcAirport.country.name
			        }
	        },
	        arrival: {
		        date : fmtdate,
		        time : flight.flightArrivalTime,
		        datetime : flight.flightArrivalTime+' '+fmtdate,
		        airport :
			        {
				        name : flight.destAirport.name,
				        code : '('+flight.destAirport.id+')',
				        geo  : flight.destAirport.city.name + ' - ' +flight.destAirport.country.name
			        }
	        },
		    duration: duration,
	    }
    }

	function pad(num, size) {
		var s = num+"";
		while (s.length < size) s = "0" + s;
		return s;
	}

    var _getPriceBreakdown = function(total)
    {
        var component = _getPriceComponent(total);

        var total_price = self.adults * total;

        var res = [
	        {
		        name : 'Adults',
		        count : self.adults,
		        price : component.nice,
		        total : _getPriceComponent(total_price).nice
	        }
        ];


	    if(self.children > 0)
	    {
		    total_price += self.children * total;
		    res.push({
			    name : 'Children',
			    count : self.children,
			    price : component.nice,
			    total : _getPriceComponent(self.children * total).nice
		    });
	    }

	    if(self.infants > 0)
	    {
		    total_price = self.infants * total;
		    res.push({
			    name : 'Infants',
			    count : self.infants,
			    price : component.nice,
			    total : _getPriceComponent(self.infants * total).nice
		    });
	    }

	    return {
	        data: res,
            total: _getPriceComponent(total_price),
            service: _getPriceComponent(self.passengers *  13.44).nice
        }

    }


    self.getPrice = function(c,p)
    {
        var t = _getPriceComponent(c*p);

        return self.priceSymbol + t;
    }

    self.fill_airport_data = function(obj)
    {
    	if(obj == null) return null;

        if(obj.srcAirport)
        {
            var airportData = window.filtersLoader.GetAirportData(obj.srcAirport.code);

            if(airportData)
            {
                obj.srcAirport = airportData;
            }
        }

        if(obj.destAirport)
        {
            var airportData = window.filtersLoader.GetAirportData(obj.destAirport.code);

            if(airportData)
            {
                obj.destAirport = airportData;
            }
        }
    }

	window.filtersLoader.init(function()
	{
		flightModel.init();
	});
}

$(document).ready(function()
{
	$(document).on('click','.hint_trigger_button',function(e)
	{
		e.stopPropagation();

		var cont = $(this).parent().find('.od-hint-container');
		var isopened = cont.hasClass('opened');

		$('.hint_container').removeClass('opened');

		if(!isopened) cont.addClass('opened');
		/*
        {
            $('html').on('click','body',_close_all);
	        cont.addClass('opened');
        }
        else
        {
	        $('html').off('click','body',_close_all);
        }*/
	});

	function _close_all()
    {
	    $('.hint_container').removeClass('opened');
    }

	$(document).on('click','.od-hint-close',function()
	{
		$('.hint_container').removeClass('opened');
	});

	$(document).on('click','.toggle_next',function()
	{
		$(this).next().slideToggle(100);
	});

    /*
	$('.od-hint-container,.od-hint-container *').on('click', function(e) {
		e.stopPropagation();
	});*/

})