!function () {
    "use strict";
    var a = document.getElementsByTagName("body")[0], b = a.getAttribute("data-app") || "flights", c = a.getAttribute("data-language") || "en", d = a.getAttribute("data-locale") || "en_US", e = a.getAttribute("data-brand") || "ED", f = a.getAttribute("data-env") || "prod", g = /secure/.test(document.location.pathname), h = {
        ED: "edreams",
        GV: "govoyage",
        OP: "opodo",
        TL: "travellink"
    }, i = function (a) {
        if (Array.prototype.reduce)return a.split("").reduce(function (a, b) {
            return a = (a << 5) - a + b.charCodeAt(0), a & a
        }, 0);
        var b = 0;
        if (0 === a.length)return b;
        for (var c = 0; c < a.length; c++) {
            var d = a.charCodeAt(c);
            b = (b << 5) - b + d, b &= b
        }
        return b
    }, j = function () {
        for (var a = document.getElementsByTagName("meta") || [], b = [], c = 0; c < a.length; c++)"odigeo-static-domain" === a[c].getAttribute("name") && b.push("//" + a[c].getAttribute("content") + "/travel/");
        return b
    }(), k = function (a) {
        if (j.length > 0) {
            var b = Math.abs(i(a)) % j.length;
            return j[b] + a
        }
        return "/travel/" + a
    }, l = function (a) {
        return k("static-content/versioned/" + a)
    }, m = function (a) {
        return k("static-content/versioned_YiDEY4crIO/" + a)
    }, n = function (a, c, d) {
        var e = "js/" + (a ? a : "");
        return e += "/" + c, e += "." + (g ? "secure" : b) + "." + d, m(e)
    };
    window.___gcfg = {lang: c};
    var o = {
        waitSeconds: 0,
        paths: {
            font: l("require/require.font.min.4f23996b357c6c40db69235066b904b2"),
            propertyParser: l("require/require.propertyParser.min.5d9e09ca8eed74acd59dc28bde595b69"),
            async: l("require/require.async.odigeo.min.2a1e3eac8dfa67d9c5a4ae75189f1e53"),
            jQuery: ["//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min", l("jquery/jquery-1.11.2.min.9aecea3830b65ecad103ee84bd5fe294")],
            thirdParty: l("thirdParty.desktop.min.0a878ca38a900cefa836c70259bcd9f8"),
            underscore: l("underscore.1.7.min.8866e84bc6081be27cd3cef1178288dc"),
            backbone: l("backbone.min.73c50cc1d02efb4e4a868d88c9b39e74"),
            html5shiv: l("html5shiv.c5776ab906652add91f9d827138cddf5"),
            Odigeo: m("js/desktop.odigeo.all"),
            setupjs: "//" + location.host + "/travel/setup.js/index.jsp?noext=1" + (location.search ? "&" + location.search.slice(1) : ""),
            desktop: n("", "desktop", "bundle"),

        },
        modules: [],
        shim: {
            jQuery: {exports: "$"},
            underscore: {exports: "_"},
            backbone: {exports: "Backbone", deps: ["jQuery", "underscore"]},
            tv4: {exports: "tv4"},
            markerclusterer: {exports: "MarkerClusterer", deps: ["googleMaps"]},
            GenesysWebChat: {exports: "GenesysWebChat"},
            thirdParty: {deps: ["jQuery"]},
            Odigeo: {exports: "Odigeo", deps: ["underscore", "backbone", "jQuery", "thirdParty"]},
            desktop: {deps: ["tv4", "Odigeo"]},
            facebook: {exports: "FB"},
            twitter: {exports: "twttr"},
            googlePlus: {exports: "gapi"},
            mailcheck: {exports: "mailcheck", deps: ["jQuery"]},
            "google-analytics": {exports: "ga"},
            avuxi: {exports: "AVUXI", deps: ["googleMaps"]}
        }
    };
    require.config(o), define("googleMaps", ["async!maps"], function () {
        return window.google.maps
    });
    var p = /msie/i.test((navigator.userAgent || navigator.vendor || window.opera).toLowerCase());
    p && (window.require(["html5shiv"]), "function" != typeof String.prototype.trim && (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    }), "function" != typeof Date.now && (Date.now = function () {
        return +new Date
    }), "function" != typeof Array.prototype.indexOf && (Array.prototype.indexOf = function (a) {
        for (var b = 0, c = this.length; b < c; b++)if (this[b] === a)return b;
        return -1
    })), window.require(["Odigeo"], function () {
        window.require(["setupjs"], function () {
            Odigeo.require = window.require, Odigeo.define = window.define, !g && Odigeo.SessionScope.cosearchLibUrl && Odigeo.require([Odigeo.SessionScope.cosearchLibUrl]), $(function () {
                Odigeo.require(["desktop"])
            })
        })
    })
}();
