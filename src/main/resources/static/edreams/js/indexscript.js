$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function() {
    var dateToday = new Date();
    $('.fromdateinput').datepicker({
        dayNamesMin: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 2,
        minDate: dateToday,
        onSelect: function (theDate,object) {
            var pTag = $('<p class="datetag"></p>')
            pTag.html(theDate);
            $('.displayfromdate').empty().append(pTag);
            $( ".todateinput" ).datepicker("option", "minDate", theDate );
            $('this').addClass('godatepicked');
            setTimeout(function(){
                if(!$('.returnFlight').hasClass('visibility')) {
                    $('.todateinput').datepicker('show');
                    $('.ui-datepicker-days-cell-over').addClass('selectedfromdate');
                    $('.ui-datepicker').css({'top': '400px!important'});
                    $('.ui-datepicker').addClass('secoundpicker')
                    $('.ui-datepicker').removeClass('firstpicker')

                }}, 16);
            var selectedgo = $('.selectedfromdate').prev()
            selectedgo.addClass('godatepicked');

            }

    });
    $('.fromdate').click(function() {
        $('.fromdateinput').show().focus().hide();
        $('.ui-datepicker').css({'top' : '400px!important'});
        $('.ui-datepicker').addClass('firstpicker')
        $('.ui-datepicker').removeClass('secoundpicker')
        $('.ui-datepicker').addClass('firstpicker')
        $('.ui-state-active').addClass('godatepicked')
        $('.ui-datepicker').removeClass('ui-state-active');


    });

    $('.todateinput').datepicker({
        dayNamesMin: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 2,

        onSelect: function (theDate,object) {
            var pTag = $('<p class="datetag"></p>')
            pTag.html(theDate);
            $('.displaytodate').empty().append(pTag);
            var selectedgo = $($('.ui-state-default')[0]).prev()
            selectedgo.addClass('godatepicked');
            $('.ui-state-active').addClass('godatepicked')
            $('.ui-datepicker').removeClass('ui-state-active');

        }
    });



    $('.todate').click(function() {
        $('.todateinput').show().focus().hide();
        $('.ui-datepicker').css({'top' : '400px!important'});
        $('.ui-datepicker').addClass('secoundpicker')
        $('.ui-datepicker').removeClass('firstpicker')

    });


    var oneWay = $('.oneWayTrip');
    var twoWay = $('.returnFlightButton')
    oneWay.on('click', function()
    {
       $('.returnFlight').addClass('visibility');
        oneWay.addClass('switcher_tab active');
        twoWay.removeClass('switcher_tab active');

    });
    twoWay.on('click', function(){
        $('.returnFlight').removeClass('visibility');
        twoWay.addClass('switcher_tab active');
        oneWay.removeClass('switcher_tab active');
    });




    var airports_data = null;
    var airports_map = null;
    var all_data = null;

    $(document).ready(function()
    {
        window.filtersLoader.init(function(filt_data)
        {
            airports_data = filt_data.data_object.airports_data;
            airports_map  = filt_data.data_object.airports_map;
            all_data      = filt_data.all_data;

            init_airportpickers();
        })
    });



    function init_airportpickers() {


        eac_options_default.data = airports_data;

        $(".fromcity").easyAutocomplete(eac_options_default);

        var eac_options_to= $.extend(true,{},eac_options_default,{list:{onSelectItemEvent: function()
        {
            var selectedItemValue = $(".tocity").getSelectedItemData();

            $('#form_dest').val(selectedItemValue.code);
            $('#form_dest_city').val(selectedItemValue.city.name);
        }}})

        $(".tocity").easyAutocomplete(eac_options_to);
    }

    var eac_options_default = {
        highlightPhrase:false,
        getValue: "label",
        list: {
            onSelectItemEvent: function() {
                var selectedItemValue = $(".fromcity").getSelectedItemData();
                $('#form_source').val(selectedItemValue.code);
                $('#form_source_city').val(selectedItemValue.city.name);
            },
            match: {
                enabled: true
            }
        },
        template : {
            type : 'custom',

            method : function(countryName, airportObj)
            {
                //airportObj = {code : 'TLV',name : 'Ben-Gurion International',city: 'Tel Aviv'};

                return '<div class="item od-airportselector-suggestions-item od-airportselector-subitems-airports no-border"'+
                    'title="'+airportObj.name+'">'+
                    '<div class="od-airportselector-suggestions-icon"><span> ( </span></div>'+
                    '<div class="od-airportselector-suggestions-text">'+
                    airportObj.name+'<span class="od-airportselector-relatedDistance"> - '+airportObj.city.name+'</span>'+
                    '</div>'+
                    '<div class="od-airportselector-suggestions-iata-wrapper"> <span class="od-airportselector-suggestions-iata">'+airportObj.code+'</span></div>'+
                    '</div>'+
                    '</li>';
            }
        },
        theme: "square"
    };





    $('#flight_search').submit(function()
    {
        var params = $(this).serializeObject();

        console.log(JSON.stringify(params));

        var validated = true;
        $('.err_msg').addClass('hiddens');

        if(!params.source || params.source.length == 0){
            $('.sourceinvalid').removeClass('hiddens');
            validated = false;
        }
        if(!params.dest || params.dest.length == 0){
            $('.destinvalid').removeClass('hiddens')
            validated = false;
        }
        if(!params.godate || params.godate.length == 0){
            $('.dateinvalidfrom').removeClass('hiddens')
            validated = false;
        }
        if(!params.returndate || params.returndate.length == 0){
            $('.dateinvalidto').removeClass('hiddens')
            validated = false;
        }


        return validated;
    })



    $('.openthing').on('click', function(){
        $('.od-moreoptions-content').toggle();
        if($('.od-moreoptions-arrow').html() === '.'){
            $('.od-moreoptions-arrow').html(',');
            $('.bluecolor').removeClass('open');
        }else{
            $('.od-moreoptions-arrow').html('.')
            $('.bluecolor').addClass('open');
        }
        $('.od-flightsManager-close-button').on('click', function(){
            $('.od-moreoptions-content').hide();
            $('.od-moreoptions-arrow').html(',');
            $('.bluecolor').removeClass('open');
        })
    });
});


var MyModel = function () {
    var self = this;

    self.adults = ko.observable();
    self.children = ko.observable();
    self.infents = ko.observable();


    self.adults.subscribe(function()
    {
       var ad = self.adults();

        $('.reduceadults').toggleClass('disabled',ad <= 1);
    });

    self.addAdults = function(){
        self.adults(self.adults() + 1);
    };

    self.reduceAdults = function(){
        var ad = self.adults() - 1;

        if(ad < 1) ad = 1;

        self.adults(ad);
    };

    self.addChildren = function(){
        var ad = self.children();
        $('.reducechildren').removeClass('disabled')
        ad++;
        self.children(ad);
    };

    self.reduceChildren = function(){
        var ad = self.children();
        if (ad > 0){
            $('.reducechildren').addClass('disabled')
            ad--;
            self.children(ad);
        }else{
            $('.reducechildren').addClass('disabled')
            self.children(ad);
        }
    };

    self.addInfents = function(){
        var ad = self.infents();
        $('.reduceinfents').removeClass('disabled')
        ad++;
        self.infents(ad);
    };

    self.reduceInfents = function(){
        var ad = self.infents();
        if (ad > 0){
            $('.reduceinfents').addClass('disabled')
            ad--;
            self.infents(ad);
        }else{
            $('.reduceinfents').addClass('disabled')
            self.infents(ad);
        }
    }

    self.checkAdults = ko.computed(function(){
        var adults = self.adults();
        var first = $('span.firstoption');
        var secound = $('span.secoundoption');
        var third = $('span.thirdoption');

        if (adults === 2){
            $('od-pax-selector-quick-access').show();
            first.removeClass('selected');
            third.removeClass('selected');
            secound.addClass('selected');
        }else if(adults === 3){
            $('od-pax-selector-quick-access').show();
            secound.removeClass('selected');
            first.removeClass('selected')
            third.addClass('selected');
        }else if(adults === 1){
            $('od-pax-selector-quick-access').show();
            secound.removeClass('selected');
            third.removeClass('selected')
            first.addClass('selected');
        }else{
            secound.removeClass('selected');
            third.removeClass('selected')
            first.removeClass('selected');
            $('pax_quick_access').hide();
        }
    })


    self.showResultsInput = ko.computed(function(){
        var adults = self.adults();
        var children = self.children();
        var infents= self.infents();
        var text = adults + ' Adults';
        if( children > 0 && infents == 0 ){
            $('.od-pax-selector-quick-access').hide();
            return text + ' & ' +children + ' Children';
        }else if( children == 0 && infents > 0){
            $('.od-pax-selector-quick-access').hide();
            return text + ' & ' +infents + ' infents';
        }else if(children > 0 && infents > 0){
            $('.od-pax-selector-quick-access').hide();
            return text + ' & ' +children + ' Children' + ' & '  +infents + ' infents'
        } else if (children == 0 && infents == 0 ){
            $('.od-pax-selector-quick-access').show();
            return text
        }
    });



    self.calculateTotalPax = ko.computed(function(){

        var total = self.adults() + self.children() + self.infents();


        return total;
    })

    self.init = function () {


        self.adults(1);
        self.children(0);
        self.infents(0);


    }

    self.bind_adults = function(n)
    {
        return function($data,event)
        {
            self.adults(n);
        }
    }

}

var model = new MyModel();
model.init();



ko.applyBindings(model, $('#kowrap')[0]);






