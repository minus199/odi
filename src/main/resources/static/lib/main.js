$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

(function($)
{
	var countries = false;
	var cities = false;
	var airports = false;
	$(document).ready(function()
	{
		ODIApi.get('wapi/get_global_data',{},function(data)
		{
			init_all(data);
		})
	});

	var global = null;
	var airports_data= [];
	var airports_map= {};

	function init_all(data)
	{
		global = data;

		for(var country_id in global.countries)
		{
			var country = global.countries[country_id];
			var cities = global.cities[country_id];

			for(var i in cities)
			{
				var city = cities[i];
				var airports = global.airports[city.id];

				if(airports.length > 0)
				{
					for(var j in airports)
					{
						var airport = airports[j];

						airport.city = city;
						airport.country = country;

						airport.value = city.name + ' ('+airport.code+')';
						airport.label = airport.value+ ' - ' + airport.country.name;

						airports_data.push(airport);

						airports_map[airport.id] = airport;
					}
				}
			}
		}

		$('.passenger-button-remove').click(function()
		{
			var target = $(this).parent().find('.passenger-amount');

			var value = parseInt(target.val()) - 1;

			if(value < 0) value = 0;

			target.val(value);
		});

		$('.passenger-button-add').click(function()
		{
			var target = $(this).parent().find('.passenger-amount');

			var value = parseInt(target.val()) + 1;

			if(value > 10) value = 10;

			target.val(value);
		});

		$('#depart_date').datepicker({ minDate : new Date(),
			onSelect: function(dateText,inst)
			{
				var d = $('#depart_date').datepicker('getDate');
				$('#return_date').datepicker('option','minDate',d);
			}
		});
		$('#return_date').datepicker({ minDate : new Date()});


		$('#searchPodForm').submit(function()
		{
			var params = $(this).serializeObject();

			var is_one = params.oneway == '1';

			params.oneway = is_one;

			if(!params.origin || !params.origin.length)
			{
				alert('Please select an origin');
			}
			else if(!params.destination || !params.destination.length)
			{
				alert('Please select a destination');
			}
			else if(!params.departing || !params.departing.length)
			{
				alert('Please choose a departure date');
			}
			else if(!is_one && (!params.return || !params.return.length))
			{
				alert('Please choose a return date');
			}
			else{

				submit_search_data(params);
			}


			return false;
		});


		$('#one-way').change(function()
		{
			var checked = $(this).is(':checked');

			$(this).parent().toggleClass('checked',checked);
			$('#return-date-picker').toggle(!checked);
		});

		$( ".airport-picker" ).each(function()
		{
			$(this).autocomplete({
				minLength: 0,
				source: airports_data,
				focus: function( event, ui )
				{
					$( this ).val( ui.item.value );
					return false;
				},
				select: function( event, ui )
				{
					$(this).val( ui.item.value );

					var target = $('#'+$(this).attr('data-target'));

					if(target.length > 0)
					{
						target.val(ui.item.code);
					}
					return false;
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
				return $( "<li>" )
				.append( "<div>" + item.value + "<br><span>" + item.country.name + "</span></div>" )
				.appendTo( ul );
			};
		})

		function submit_search_data(params)
		{
			if(!params.one)
				ODIApi.post('wapi/search_flights',params, function(data)
				{
					/*data.goingFlights = data.goingFlights.sort(flight_sort_price);

					 if(!data.oneWay)
					 {
					 data.returningFlights = data.returningFlights.sort(flight_sort_price);
					 }*/

					data.Adults = params.Adults;
					data.Children = params.Children;

					data.originCity = airports_map[data.originAirportId].city;
					data.destCity = airports_map[data.destAirportId].city;
					data.originCity.airports = clean_data(global.airports[data.originCity.id]);
					data.destCity.airports = clean_data(global.airports[data.destCity.id]);

					var js = JSON.stringify(data);
					$('#book_form_data').val(js);
					$('#submit_to_book').submit();
				},false,true);
		}


		function clean_data(arr)
		{
			for(var i in arr)
			{
				delete arr[i].city;
				delete arr[i].country;
			}

			return arr;
		}

	}
})(jQuery);