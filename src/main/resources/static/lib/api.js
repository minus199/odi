window.ODIApi = new (function ODIApi() {
    var self = {};

    self.Methods = {
        POST: 'post',
        GET: 'get',
        DELETE: 'delete',
        PUT: 'put',
    };

    self.get_headers = function () {
        var h = {};

        return h;
    }

    self.base = '/';

    self.get = function (url, data, success, error,disallow_json) { return self.request("GET", url, data, success, error,disallow_json); }
    self.post = function (url, data, success, error,disallow_json) { return self.request("POST", url, data, success, error,disallow_json); }
    self.put = function (url, data, success, error,disallow_json) { return self.request("PUT", url, data, success, error,disallow_json); }
    self.delete = function (url, data, success, error,disallow_json) { return self.request("DELETE", url, data, success, error,disallow_json); }


    self.request = function (method, url, data, success, error,disallow_json) {

        var params = {
            type: method,
            url: (self.base+url).replace(/\/\//g, '\/'),
            data: data,
            success: success,
            headers: self.get_headers(),
            error: function (xhr, err) {
                if (err = "parsererror" && xhr.status == 200) {
                    if (typeof success === 'function')
                        success(xhr, err);
                }
                else if (typeof error === 'function')
                    error(xhr, err);
                else {
                    var message = "An error has occurred";

                    if (xhr.responseJSON) {
                        var response = xhr.responseJSON;
                        message = response.ExceptionMessage;
                        console.log(xhr.status, response.ExceptionMessage);
                    }
                    else {
                        console.log(xhr);
                    }
                }
            },
            dataType: 'json'
        }


        if (!disallow_json && self.is_body_method(method))
        {
            params.contentType = 'application/json';
            params.data = JSON.stringify(data);
        }


        return $.ajax(params);
    }


    self.is_body_method = function(method)
    {
        return (method === 'POST' || method === 'PUT');
    }

    return self;
})();