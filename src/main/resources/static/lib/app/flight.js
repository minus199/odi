var data1 = {"originAirportId":"TLV","destAirportId":"CDG","departingDate":"02/23/2017","returnDate":"",

	"goingFlights":{
		"Wed 22 Feb":{"id":"c995d1e7-4a96-41fa-a971-193801203152","name":null,"createdDate":1487515334381,"lastModifiedDate":1487515334381,"flightTime":11700000,"flightDuration":null,"flightLanding":29700000,"airlineId":"LY","flightNumber":319,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"THURSDAY","tempFlightCost":300,"instances":[]},
		"Thu 23 Feb":{
			"id":"111870ea-740a-4ca4-a96e-167ce9638d24","name":null,"createdDate":1487515334381,"lastModifiedDate":1487515334381,"flightTime":11700000,"flightDuration":null,"flightLanding":29700000,"airlineId":"LY","flightNumber":319,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"FRIDAY","tempFlightCost":270,"instances":[]},
		"Fri 24 Feb":{"id":"9bf5b203-6c04-4d93-b09a-cbffd6469195","name":null,"createdDate":1487515334381,"lastModifiedDate":1487515334381,"flightTime":11700000,"flightDuration":null,"flightLanding":29700000,"airlineId":"LY","flightNumber":319,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"SATURDAY","tempFlightCost":250,"instances":[]},

	}
	,"returningFlights":null,"oneWay":true,"Adults":"1","Children":"1","originCity":{"id":"e04d7816-1163-484a-b538-58c88ca1754f","name":"Tel Aviv","createdDate":1487515334334,"lastModifiedDate":1487515334334,"countryId":"17caf1ff-68a2-49ed-a500-828a8b19e8f5","uniquename":"telaviv","airports":[{"id":"TLV","name":"Ben Guryon","createdDate":1487515334350,"lastModifiedDate":1487515334350,"cityId":"e04d7816-1163-484a-b538-58c88ca1754f","code":"TLV","value":"Tel Aviv (TLV)","label":"Tel Aviv (TLV) - Israel"}]},"destCity":{"id":"eea1a87c-bf39-4d62-9bdd-23c94863a38f","name":"Paris","createdDate":1487515334334,"lastModifiedDate":1487515334334,"countryId":"ab441a1b-4863-4ec0-a13f-c366314c1447","uniquename":"paris","airports":[{"id":"CDG","name":"Charles De Gaulle","createdDate":1487515334365,"lastModifiedDate":1487515334365,"cityId":"eea1a87c-bf39-4d62-9bdd-23c94863a38f","code":"CDG","value":"Paris (CDG)","label":"Paris (CDG) - Frace"},{"id":"ORY","name":"Orly","createdDate":1487515334365,"lastModifiedDate":1487515334365,"cityId":"eea1a87c-bf39-4d62-9bdd-23c94863a38f","code":"ORY","value":"Paris (ORY)","label":"Paris (ORY) - Frace"}]}};



var data2 = {"originAirportId":"TLV","destAirportId":"CDG","departingDate":"02/20/2017","returnDate":"02/23/2017","goingFlights":{"Sun 19 Feb":{"id":"65b2df87-a290-48ff-bbf3-d4709af701c6","name":null,"createdDate":1487515922119,"lastModifiedDate":1487515922119,"flightTime":-6900000,"flightDuration":null,"flightLanding":10800000,"airlineId":"AF","flightNumber":1121,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"MONDAY","tempFlightCost":430,"instances":[]},"Mon 20 Feb":{"id":"0a945f1d-47cc-4de7-8461-e1880de1d6a7","name":null,"createdDate":1487515922150,"lastModifiedDate":1487515922150,"flightTime":64200000,"flightDuration":null,"flightLanding":77100000,"airlineId":"EZY","flightNumber":8818,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"SUNDAY","tempFlightCost":430,"instances":[{"id":"c394247c-1a62-46bc-84bd-e9ee98173ddf","day":"SUNDAY","time":"19:50","flightDuration":null,"flightLandingTime":"23:25","cost":450},{"id":"2a8b999e-6d24-4f28-a88c-6f91266701d5","day":"TUESDAY","time":"19:50","flightDuration":null,"flightLandingTime":"23:25","cost":310},{"id":"c87aa036-99ff-4124-89cd-4955af494b4d","day":"THURSDAY","time":"19:50","flightDuration":null,"flightLandingTime":"23:25","cost":420}]},"Tue 21 Feb":{"id":"c5d54537-8904-4032-af99-50a5ed836394","name":null,"createdDate":1487515922072,"lastModifiedDate":1487515922072,"flightTime":50700000,"flightDuration":null,"flightLanding":68700000,"airlineId":"LY","flightNumber":325,"originAirportId":"TLV","destinationAirportId":"CDG","dayOfWeek":"WEDNESDAY","tempFlightCost":330,"instances":[]}},"returningFlights":{"Wed 22 Feb":{"id":"2269061a-75a3-477e-9a06-df5b82871fff","name":null,"createdDate":1487515922025,"lastModifiedDate":1487515922025,"flightTime":35400000,"flightDuration":null,"flightLanding":53700000,"airlineId":"LY","flightNumber":320,"originAirportId":"CDG","destinationAirportId":"TLV","dayOfWeek":"THURSDAY","tempFlightCost":280,"instances":[]},"Thu 23 Feb":{"id":"c6e1ad75-2c92-4aa6-92f9-c025033bd232","name":null,"createdDate":1487515922088,"lastModifiedDate":1487515922088,"flightTime":-6900000,"flightDuration":null,"flightLanding":7800000,"airlineId":"LY","flightNumber":326,"originAirportId":"CDG","destinationAirportId":"TLV","dayOfWeek":"FRIDAY","tempFlightCost":450,"instances":[]},"Fri 24 Feb":{"id":"4f299841-5cb3-4d0a-b4fd-e35d99917a91","name":null,"createdDate":1487515922025,"lastModifiedDate":1487515922025,"flightTime":35400000,"flightDuration":null,"flightLanding":53700000,"airlineId":"LY","flightNumber":320,"originAirportId":"CDG","destinationAirportId":"TLV","dayOfWeek":"SATURDAY","tempFlightCost":220,"instances":[]}},"oneWay":false,"Adults":"2","Children":"0","originCity":{"id":"ab7025b4-1e61-4ea5-9f18-14fc35ff7d29","name":"Tel Aviv","createdDate":1487515919943,"lastModifiedDate":1487515919943,"countryId":"587a69ab-b3ac-4ae5-86b7-ed4c7f0fc7f8","uniquename":"telaviv","airports":[{"id":"TLV","name":"Ben Guryon","createdDate":1487515920036,"lastModifiedDate":1487515920036,"cityId":"ab7025b4-1e61-4ea5-9f18-14fc35ff7d29","code":"TLV","value":"Tel Aviv (TLV)","label":"Tel Aviv (TLV) - Israel"}]},"destCity":{"id":"c9949207-dcb8-4c07-905f-4234ce8c70ff","name":"Paris","createdDate":1487515919974,"lastModifiedDate":1487515919974,"countryId":"c573a56b-6edf-4e1f-bda7-11dc0eb60244","uniquename":"paris","airports":[{"id":"CDG","name":"Charles De Gaulle","createdDate":1487515920036,"lastModifiedDate":1487515920036,"cityId":"c9949207-dcb8-4c07-905f-4234ce8c70ff","code":"CDG","value":"Paris (CDG)","label":"Paris (CDG) - Frace"},{"id":"ORY","name":"Orly","createdDate":1487515920052,"lastModifiedDate":1487515920052,"cityId":"c9949207-dcb8-4c07-905f-4234ce8c70ff","code":"ORY","value":"Paris (ORY)","label":"Paris (ORY) - Frace"}]}};

var PriceComponent = function(item,name,details,price,amount, amountText, icon,index)
{
	if(typeof(details) == 'string') details = function(){return details;}
	amount = parseInt(amount);
	price = parseFloat(price);

	if(isNaN(amount) || amount <= 0) amount = 1;
	if(isNaN(price) || price < 0) price = 0;

	this.item = item;
	this.details = details;
	this.price  = price;
	this.amount = amount;
	this.amountText = amountText;
	this.icon = icon;
	this.index = index;
	this.totalCost = amount * price;
}

var FlightsModel = function(options)
{
	var self = this;
	var data = options.data;
	var global = data;

	self.proposals = ko.observableArray([]);

	self.isOneWay = ko.observable(data.oneWay);
	self.departFlight = ko.observable(false);
	self.returnFlight = ko.observable(false);
	self.priceComponents = ko.observableArray([]);
	self.totalPrice = ko.computed(function()
	{
		var comps = self.priceComponents();
		var total = 0;

		for(var i in comps)
		{
			total += comps[i].totalCost;
		}

		var totalTickets = parseInt(global.Adults) + parseInt(global.Children);

		var going = self.departFlight();
		if(going)
		{
			total+= (totalTickets * going.tempFlightCost);
		}

		if(!self.isOneWay())
		{
			var retur = self.returnFlight();
			if(retur)
			{
				total+= (totalTickets * retur.tempFlightCost);
			}
		}

		var props = self.proposals();

		for(var i in props)
		{
			if(props[i].accepted())
			{
				total += parseFloat(totalTickets * props[i].cost);
			}
		}

		return parseFloat(total);
	},self);

	self.is_valid = ko.observable(false);
	self.loading = ko.observable(false);

	self.get_nice_price = function(price)
	{
		var priceInt = parseInt(price);
		var prDec = parseInt((price - priceInt) * 100);

		return '$'+priceInt+'<div>.</div><span>'+prDec+'</span>';
	}

	self.totalPriceFormatted = ko.computed(function()
	{
		var price = self.totalPrice().toFixed(2);

		return self.get_nice_price(price);
	},self);

	self.goingFlights = ko.observableArray([]);
	self.returningFlights = ko.observableArray([]);

	self.flight_sort_price = function(a,b)
	{
		return a.tempFlightCost > b.tempFlightCost;
	}

	self.max_view_flights=3;
	self.init = function()
	{
		self.flightDetails = ko.observable({
			srcCode  : global.originAirportId,
			srcCity  : global.originCity.name,
			destCode : global.destAirportId,
			destCity : global.destCity.name,
			originRoute : self.get_route(),
			destRoute : self.get_route(true),
			altOriginRoute : self.get_route(false,self.alt_rout),
			altDestRoute : self.get_route(true,self.alt_rout),
		});


		self.goingFlights(self.fix_flight_info(data.goingFlights));
		if(!data.oneWay)
			self.returningFlights(self.fix_flight_info(data.returningFlights));

		/*var maxFl = Math.min(self.max_view_flights,data.goingFlights.length);

		 data.goingFlights = data.goingFlights.sort(self.flight_sort_price);
		 data.goingFlights[0].is_lowest = true;


		 ;

		 if(!data.oneWay)
		 {
		 data.returningFlights = data.returningFlights.sort(self.flight_sort_price);
		 data.returningFlights[0].is_lowest = true;

		 maxFl = Math.min(self.max_view_flights,data.goingFlights.length);
		 self.returningFlights(self.fix_flight_info(data.returningFlights.splice(0,maxFl)));
		 }*/
	}

	self.removeDepart = function(data,e)
	{
		self.departFlight(false);
		self.is_valid(false);
		$('.OutboundDaySlider').find('a.selected').removeClass('selected');
		self.removeProposal(true);
	}

	self.removeReturn = function(data,e)
	{
		self.returnFlight(false);

		self.removeProposal(false);

		if(!self.isOneWay()) self.is_valid(false);
		$('.ReturnDaySlider').find('a.selected').removeClass('selected');
	}

	self.addDepart = function(data,e)
	{
		self.departFlight(data);
		var elm = $(e.currentTarget);

		elm.parents('.OutboundDaySlider').find('a.selected').removeClass('selected');
		elm.addClass('selected');

		self.checkProposal(data,true);
	}

	self.addReturn = function(data,e)
	{
		self.returnFlight(data);
		var elm = $(e.currentTarget);

		elm.parents('.ReturnDaySlider').find('a.selected').removeClass('selected');
		elm.addClass('selected');

		self.checkProposal(data);
	}

	self.step = ko.observable(1);

	self.is_valid = ko.computed(function()
	{
		var valid = self.departFlight();

		if(!valid || self.isOneWay())
			return valid;
		else
			valid = self.returnFlight() != false;

		return valid;
	},self);

	self.userName = ko.observable('');
	self.userPhone = ko.observable('');

	self.is_valid_two = ko.computed(function()
	{
		var valid = self.userName().length > 0 && self.userPhone().length > 0;

		return valid;
	},self);

	self.loadingCheckout = ko.observable(false);
	self.buyFlight = function()
	{
		self.loadingCheckout(true);

		var done = 0;
		var sent = 0;
		var failed = 0;

		var props = self.proposals();


		for(var i in props)
		{
			var prop = props[i];

			if(prop.accepted())
			{
				sent++;

				var params = {
					proposalId : prop.id,
					user : self.userName(),
					userphone: self.userPhone()
				}

				ODIApi.post('proposal/accept',params,function(proid)
				{
					if(proid != prop.id) failed++;
					else done++;

					if(sent === (done + failed))
					{
						if(failed)
						{
							console.log('Proposal approval failed: '+prop.id);
						}

						self.loadingCheckout(false);
						self.step(3);
					}
				},function(err){failed++},true);
			}
		}

		if(sent == 0)
		{
			self.loadingCheckout(false);
			self.step(3);
		}
	}

	self.moveToCheckout = function()
	{
		self.step(2);
	}


	self.alt_rout = function(from,to)
	{
		return '<span>'+from+'<br>to '+to+'</span>';
	}
	self.get_route = function(rev,renderer)
	{
		var srcCode  = global.originAirportId;
		var srcCity  = global.originCity;

		var destCode = global.destAirportId;
		var destCity = global.destCity;

		var srcName = self.get_airport_name(srcCode,srcCity);
		var destName = self.get_airport_name(destCode,destCity);

		if(rev)
		{
			var tmp = srcName;
			srcName = destName;
			destName = tmp;

		}

		if(typeof(renderer) === 'function'){return renderer(srcName,destName);}

		return srcName + ' <span>to '+destName+'</span>';
	}


	self.get_airport_name = function(code,city)
	{
		if(city.airports.length == 1) return city.name;


		var airport = false;

		for(var i in city.airports)
		{
			airport = city.airports[i]
			if(airport.id == code)
			{
				return city.name + ' ' + airport.name;
			}
		}
	}

	self.company_id = 'Easy Jet';
	self.checkProposal = function(data,isDepart)
	{
		self.removeProposal(isDepart);
		self.loading(true);

		var year = new Date().getFullYear();
		var d = data.date_index + ' '+ year +' ' + data.timeString;

		var flightDate = self.get_formatted_date(d,'Y-MM-D','ddd DD MMM Y HH:mm');


		//yyyy-MM-dd
		var params = {
			flightid : data.id,
			companyid : self.company_id,
			productid : '',
			flightDate : flightDate,
			numberOfTickets : parseInt(global.Adults) + parseInt(global.Children),

			date: flightDate,
			airline : data.airlineId
		}
		ODIApi.get('/proposal/get',params,function(data)
		{
			self.loading(false);

			if(data)
			{
				self.add_proposal(data,isDepart);
			}
		},function()
		{
			self.loading(false);
		});
	}

	self.fix_proposal = function(data,isDepart)
	{
		var prop = {
			proposal : data,
			id : data.id,
			displayText: self.get_proposal_display_text(data),
			displayTitle: 'Flight delay insurance',
			cost: data.cost,
			accepted : ko.observable(false),
			isDepart : isDepart
		}

		return prop;
	}

	self.get_proposal_display_text = function(data)
	{
		return 'Get <strong>$'+data.compensation+'</strong> back if your flight was delayed for more than '+data.delayHours+' hours';
	}

	self.add_proposal = function(data,isDepart)
	{
		if(!isDepart) isDepart = false;
		var prop = self.fix_proposal(data,isDepart);

		self.proposals.push(prop);
	}

	self.removeProposal = function(isDepart)
	{
		if(!isDepart) isDepart = false;

		var props = self.proposals();
		var pr = false;

		for(var i in props)
		{
			if(isDepart == props[i].isDepart)
			{
				pr = props[i];
			}
		}

		self.proposals.remove(pr);

		return pr;
	}


	self.fix_flight_info = function(flights)
	{
		var lowest_key = false;
		var lowest_rate = 99999999999;
		var year = new Date().getFullYear();

		for(var i in flights)
		{
			var flight = flights[i];

			if(flight == null) continue;

			var d = new Date(flight.flightTime);
			
			
			
			flight.timeString = moment(d).format('HH:mm');

			d = new Date(flight.flightLanding);
			flight.landingTimeString = moment(d).format('HH:mm');

			if(lowest_rate > flight.tempFlightCost)
			{
				lowest_rate = flight.tempFlightCost;
				lowest_key  = i;
			}

			flight.date_index = i;
			flight.is_lowest = false;
			flight.formattedPrice = self.get_nice_price(flight.tempFlightCost);

			flight.formattedTimes = 'Dep '+flight.timeString+ ' - Arr ' + flight.landingTimeString;
			flight.dateFormatted = self.get_formatted_date(flight.date_index + ' '+ year +' ' + flight.timeString,false,'ddd DD MMM Y HH:mm');

			flight.priceDescription = self.get_price_description(flight.tempFlightCost);

			flights[i] = flight;
		}

		flights[lowest_key].is_lowest = true;

		return flights;
	}

	self.get_price_description = function(price)
	{
		var adults = global.Adults;
		var child = global.Children;
		var html = '';

		if(adults > 0)
		{
			html = '<div class="basket-passenger-grouping">' + adults +' Adult <span class="basket-passenger-total"> ' + adults +' x $' + price +'</span></div>';
		}

		if(child > 0)
		{
			html += '<div class="basket-passenger-grouping">' + child +' Child <span class="basket-passenger-total"> ' + child +' x $' + price +'</span></div>';
		}

		return html;
	}


	self.get_formatted_date = function(timeStr,format,srcFormat)
	{
		if(!format) format = 'ddd DD MMMM Y';
		
		if(srcFormat)
			return moment(timeStr,srcFormat).format(format);
		
		return moment(timeStr).format(format);
	}

	self.init();
}

ko.bindingHandlers.foreachprop = {
	transformObject: function (obj) {
		var properties = [];
		ko.utils.objectForEach(obj, function (key, value) {
			properties.push({ key: key, value: value });
		});
		return properties;
	},
	init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var properties = ko.pureComputed(function () {
			var obj = ko.utils.unwrapObservable(valueAccessor());
			return ko.bindingHandlers.foreachprop.transformObject(obj);
		});
		ko.applyBindingsToNode(element, { foreach: properties }, bindingContext);
		return { controlsDescendantBindings: true };
	}
};

