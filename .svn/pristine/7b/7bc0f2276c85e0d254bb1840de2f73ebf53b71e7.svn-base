package com.odi.model.product;

import com.odi.model.BaseBase;
import com.odi.model.event.Event;
import org.hibernate.annotations.Proxy;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Proxy(lazy = false)
@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(name = "Product.findByCompany", query = "SELECT p FROM Product p WHERE p.companyId = ?1")
})
public class Product extends BaseBase {

    private String templateId = null;
    private String companyId = null;
    private String defaultValue = null;
    
    private String description = "";

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "thetemplate")
    private Template template;

    private String segment = "";

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Product() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void validate() {
        super.validate();
    }

    @Override
    public String toString() {
        return super.toString();
    }


    public boolean isRelevant(Event event) {
        return getTemplate().isRelevant(event);
    }

    public boolean isIn(Event event) {
        return getTemplate().isIn(event);
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }
}
