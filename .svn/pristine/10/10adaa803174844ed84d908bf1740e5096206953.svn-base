package com.odi.controller;

import com.odi.model.*;
import com.odi.service.ResponseFactory;
import com.odi.service.batch.IBatchProcessor;
import com.odi.service.batch.exceptions.ProcessingException;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by minus on 3/30/17.
 */
@RestController
public class BatchDataImportController {
	private final IBatchProcessor batchProcessor;
	private final ResponseFactory responseFactory;

	@Autowired
	public BatchDataImportController(IBatchProcessor batchProcessor, ResponseFactory responseFactory) {
		this.batchProcessor = batchProcessor;
		this.responseFactory = responseFactory;
	}

	@PostMapping(value = "/batch/csv", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file, AllowedType type) {
		Optional<Throwable> throwableOptional = Optional.empty();
		try {
			JobExecution process = batchProcessor.process(type.getCompanyClass(), new ByteArrayResource(file.getBytes()));
			ExitStatus exitStatus = process.getExitStatus();
			if (exitStatus.equals(ExitStatus.COMPLETED)) {
				return responseFactory.make();
			}

			if (Objects.equals(ExitStatus.FAILED.getExitCode(), exitStatus.getExitCode())) {
				throwableOptional = Optional.ofNullable(process.getAllFailureExceptions().get(0));
			}
		} catch (IOException | ProcessingException e) {
			throwableOptional = Optional.of(e);
		}

		return responseFactory.make(throwableOptional, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	enum AllowedType {
		AIRLINE(AirLineCompany.class),
		AIRPORT(Airport.class),
		COMPANY(Company.class),
		FLIGHT(Flight.class);

		private final Class<? extends EntityBase> companyClass;

		<T extends EntityBase> AllowedType(Class<T> companyClass) {

			this.companyClass = companyClass;
		}

		public Class<? extends EntityBase> getCompanyClass() {
			return companyClass;
		}
	}
}