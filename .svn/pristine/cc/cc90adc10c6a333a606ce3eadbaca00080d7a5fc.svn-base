package com.odi;

import com.odi.domain.ConnectedFlight;
import com.odi.domain.Flight;
import com.odi.domain.event.FlightCanceledEvent;
import com.odi.domain.event.FlightLndedEvent;
import com.odi.domain.offer.MissedConnectionProposal;
import com.odi.domain.product.MissedConnectionProductTemplate;
import com.odi.domain.product.Product;
import com.odi.service.IConnectedFlightService;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.DayOfWeek;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MissedConnectionProposalTest {

    @Autowired
    IConnectedFlightService connectedFlightService;

    private final Logger logger = LoggerFactory.getLogger("test");

    @Test
    public void contextLoads() {
    }


    @Test
    public void newTest() {
        MissedConnectionProposal proposal = new MissedConnectionProposal();

        assertNotNull(proposal);
    }

    @Test
    public void isIn() {
        MissedConnectionProposal proposal = new MissedConnectionProposal();
        Flight flight1 = new Flight("06:00", "08:00", "TA", 111, "BG", "TAV", DayOfWeek.SUNDAY, 230);
        Flight flight2 = new Flight("09:00", "12:00", "TA", 222, "TAV", "CDG", DayOfWeek.SUNDAY, 370);
        Flight flightNotRelevant = new Flight("09:00", "12:00", "AA", 222, "BB", "CC", DayOfWeek.SUNDAY, 370);

        Product product = new Product();
        MissedConnectionProductTemplate template = new MissedConnectionProductTemplate();

        FlightLndedEvent landed = new FlightLndedEvent();
        FlightCanceledEvent canceled = new FlightCanceledEvent();

        String travelDate = "10/10/2010";
        DateTimeFormatter dfDate = DateTimeFormat.forPattern("dd/MM/YYYY");
        DateTimeFormatter dfTime = DateTimeFormat.forPattern("kk:mm");
        DateTimeFormatter dfDateTime = DateTimeFormat.forPattern("dd/MM/YYYY kk:mm");


        product.setName("Missed Connection");
        product.setCompanyId("ODI");
        product.setTemplate(template);

        proposal.setFirstFlight(flight1);
        proposal.setSecondFlight(flight2);
        proposal.setCost(36);
        proposal.setCompensation(600);
        proposal.setCompanyId("ODI");
        proposal.setTravelDate(dfDate.parseDateTime(travelDate).toDate());
        proposal.setProduct(product);

        /*
        Check for Cancel event
        First flight, second flight, and non relevant flight
         */
        canceled.setAirline(flight1.getAirlineId());
        canceled.setAirline(flight1.getAirlineId());
        canceled.setDepartureAirportId(flight1.getOriginAirportId());
        canceled.setArrivalAirportId(flight1.getDestinationAirportId());
        canceled.setFlightNumber(flight1.getFlightNumber());
        canceled.setScheduledDate(dfDate.parseDateTime(travelDate).toDate());
        assertTrue(proposal.isIn(canceled));

        canceled.setAirline(flight2.getAirlineId());
        canceled.setAirline(flight2.getAirlineId());
        canceled.setDepartureAirportId(flight2.getOriginAirportId());
        canceled.setArrivalAirportId(flight2.getDestinationAirportId());
        canceled.setFlightNumber(flight2.getFlightNumber());
        canceled.setScheduledDate(dfDate.parseDateTime(travelDate).toDate());
        assertTrue(proposal.isIn(canceled));

        // Not the same date
        canceled.setAirline(flight2.getAirlineId());
        canceled.setAirline(flight2.getAirlineId());
        canceled.setDepartureAirportId(flight2.getOriginAirportId());
        canceled.setArrivalAirportId(flight2.getDestinationAirportId());
        canceled.setFlightNumber(flight2.getFlightNumber());
        canceled.setScheduledDate(dfDate.parseDateTime("1/1/2017").toDate());
        assertTrue(proposal.isIn(canceled) == false);

        // Not relevant flight
        canceled.setAirline(flightNotRelevant.getAirlineId());
        canceled.setAirline(flightNotRelevant.getAirlineId());
        canceled.setDepartureAirportId(flightNotRelevant.getOriginAirportId());
        canceled.setArrivalAirportId(flightNotRelevant.getDestinationAirportId());
        canceled.setFlightNumber(flightNotRelevant.getFlightNumber());
        canceled.setScheduledDate(dfDate.parseDateTime(travelDate).toDate());
        assertTrue(proposal.isIn(canceled) == false);


        /*
        Check for landed event
        1st flight late in 15 minutes, but on time
         */
        landed.setAirline(flight1.getAirlineId());
        landed.setAirline(flight1.getAirlineId());
        landed.setDepartureAirportId(flight1.getOriginAirportId());
        landed.setArrivalAirportId(flight1.getDestinationAirportId());
        landed.setFlightNumber(flight1.getFlightNumber());
        landed.setScheduledDate(dfDate.parseDateTime(travelDate).toDate());
        landed.setDepartureTime(dfDateTime.parseDateTime(travelDate + " 06:15").toDate());
        landed.setArrivalTime(dfDateTime.parseDateTime(travelDate + " 08:15").toDate());
        assertTrue(proposal.isIn(landed) == false);


        /*
        Check for landed event
        1st flight late in 15 minutes, but on time
         */
        landed.setAirline(flight1.getAirlineId());
        landed.setAirline(flight1.getAirlineId());
        landed.setDepartureAirportId(flight1.getOriginAirportId());
        landed.setArrivalAirportId(flight1.getDestinationAirportId());
        landed.setFlightNumber(flight1.getFlightNumber());
        landed.setScheduledDate(dfDate.parseDateTime(travelDate).toDate());
        landed.setDepartureTime(dfDateTime.parseDateTime(travelDate + " 06:40").toDate());
        landed.setArrivalTime(dfDateTime.parseDateTime(travelDate + " 08:45").toDate());
        assertTrue(proposal.isIn(landed));
    }

    @Test
    public void saveTest() {
        String travelDate = "10/10/2010";
        DateTimeFormatter dfDate = DateTimeFormat.forPattern("dd/MM/YYYY");

        Flight flight1 = new Flight("06:00", "08:00", "TA", 111, "BG", "TAV", DayOfWeek.SUNDAY, 230);
        Flight flight2 = new Flight("09:00", "12:00", "TA", 222, "TAV", "CDG", DayOfWeek.SUNDAY, 370);
        ConnectedFlight flight = new ConnectedFlight(flight1, flight2);

        connectedFlightService.save(flight);
    }


}
