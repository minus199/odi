package com.odi.service.users.domain;

import com.odi.model.user.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;

/**
 * Created by asafb on 4/01/17
 */
public class APIUser implements ISystemUser {
	private User user;

	public APIUser(User user) {
		this.user = user;
	}

	@Override
	public UsernamePasswordAuthenticationToken asDetails() {
		return new UsernamePasswordAuthenticationToken(this, getPassword(), getAuthorities());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return user.getRole().grantedAuthorities();
	}

	@Override
	public String getPassword() {
		return user.getPasswordHash();
	}

	@Override
	public String getUsername() {
		return user.getName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true; // should implement user.isActive(); --> Unactive user will not be authenticated
	}

	@Override
	public boolean isAccountNonLocked() {
		return true; // should implement user.isLocked(); --> Locked user will not be authenticated
	}

	@Override
	public boolean isCredentialsNonExpired() {
		if (user == null) {
			return false;
		}

		Timestamp original = new Timestamp(System.currentTimeMillis());
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(original.getTime());
		cal.add(Calendar.MINUTE, 10);

		return true; // should implement user.expirationTime(); --> User with expired creds will not be authenticated
//		return user.getAccessTokenExpTimestamp().after(new Timestamp(cal.getTime().getTime()));
	}

	@Override
	public boolean isEnabled() {
		return true; // disabled will not be authed
		// for example
//		return user != null && user.isActive() && ! user.isLocked();
	}
}
