package com.odi.domain.offer;

import com.odi.domain.Flight;
import com.odi.domain.event.*;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.annotations.Proxy;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQuery(name = "MissedConnectionProposal.findAcceptedState", query = "SELECT p FROM MissedConnectionProposal p WHERE p.state = 3")
@Proxy(lazy=false) // Dont know why, but it helped...
@DiscriminatorValue("MissedConnectionProposal")
@Table(name="MissedConnectionProposal")
public class MissedConnectionProposal extends Proposal {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "firstFlightId")
    Flight firstFlight = null;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "secondFlightId")
    Flight secondFlight = null;

    // The date of the flight
    private Date travelDate = null;



    public Flight getFirstFlight() {
        return firstFlight;
    }

    public void setFirstFlight(Flight firstFlight) {
        this.firstFlight = firstFlight;
    }

    public Flight getSecondFlight() {
        return secondFlight;
    }

    public void setSecondFlight(Flight secondFlight) {
        this.secondFlight = secondFlight;
    }

    @Override
    public boolean isRelevant(Event event) {
        return super.isRelevant(event);
    }

    public Date getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(Date travelDate) {
        this.travelDate = travelDate;
    }


    @Override
    public boolean isIn(Event event) {
        String eventAirlineId = "";
        int eventFlightNumber = 0;
        Date eventFlightDate = null;

        switch (Events.fromClass(event.getClass())) {
            case FlightCanceledEvent:
                eventAirlineId = ((FlightCanceledEvent)event).getAirline();
                eventFlightNumber = ((FlightCanceledEvent)event).getFlightNumber();
                eventFlightDate = ((FlightCanceledEvent)event).getScheduledDate();
                break;
            case FlightDeparturedEvent:
                eventAirlineId = ((FlightDeparturedEvent)event).getAirline();
                eventFlightNumber = ((FlightDeparturedEvent)event).getFlightNumber();
                eventFlightDate = ((FlightDeparturedEvent)event).getScheduledDate();
                break;
            case FlightLndedEvent:
                eventAirlineId = ((FlightLndedEvent)event).getAirline();
                eventFlightNumber = ((FlightLndedEvent)event).getFlightNumber();
                eventFlightDate = ((FlightLndedEvent)event).getScheduledDate();

                /*
                * The 2nd flight on that same date should leave at:
                * proposalDate and the secondFlight time
                 */
                DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd kk:mm");

                // This is only the date
                DateTime secondFlightDateTime = new DateTime(this.getTravelDate());
                 // This is the flight time of day (the relevant part is the hh:mm)
                DateTime flightTime = new DateTime(this.getSecondFlight().getFlightTime());
                // Build the full date and time of the second flight
                secondFlightDateTime = secondFlightDateTime.withHourOfDay(flightTime.getHourOfDay());
                secondFlightDateTime = secondFlightDateTime.withMinuteOfHour(flightTime.getMinuteOfHour());

                /*
                * Latest possible time is 30 minutes before the 2nd flight
                 */
                DateTime latestPossibleTime = secondFlightDateTime.minusMinutes(30);

                DateTime eventTime = new DateTime(((FlightLndedEvent)event).getArrivalTime());

                Seconds seconds = Seconds.secondsBetween(eventTime, latestPossibleTime);

                System.out.println("----- Missed Connection Proposal ----");
                System.out.println("Second Flight time: " + secondFlightDateTime.toString(dateTimeFormat));
                System.out.println("Latest time without delay: " + latestPossibleTime.toString(dateTimeFormat));
                System.out.println("Actual Flight time: " + eventTime.toString(dateTimeFormat));
                System.out.println("Number of seconds: " + String.valueOf(seconds.getSeconds()));

                if (seconds.getSeconds() > 0)
                    return false;

                break;
            case UNKNOWN:
                return false;
        }

        if (eventAirlineId.equals(firstFlight.getAirlineId()) == false &&
                eventAirlineId.equals(secondFlight.getAirlineId()) == false)
            return false;

        if (eventFlightNumber != (firstFlight.getFlightNumber())  &&
                eventFlightNumber != (secondFlight.getFlightNumber()))
            return false;

        if (DateUtils.isSameDay(getTravelDate(), eventFlightDate) == false)
            return false;

        return super.isIn(event);
    }

    @Override
    public String claimMessage() {
        return String.format("We are sorry you missed your connection flight (%s%d).",
                this.getSecondFlight().getAirlineId(),
                this.getSecondFlight().getFlightNumber()
        );
    }

    @Override
    public String noClaimMessage() {
    	DateTime date = new DateTime(this.getTravelDate());
        return String.format("Flight %s%d on %s arrived on agreed schedule",
                this.getFirstFlight().getAirlineId(),
                this.getFirstFlight().getFlightNumber(),
                date.toString(DateTimeFormat.forPattern("yyyy-MM-dd"))
        );
    }



    @Override
    public String toString() {
        return String.format("(%s) Missed Connection %s->%s->%s on %s, payment %.2f$ to get %.2f$",
                this.getState().toString(),
                this.getFirstFlight().getOriginAirportId(),
                this.getSecondFlight().getOriginAirportId(),
                this.getFirstFlight().getDestinationAirportId(),
                this.getTravelDate().toString(),
                this.getCost(),
                this.getCompensation()
        );
    }

}
