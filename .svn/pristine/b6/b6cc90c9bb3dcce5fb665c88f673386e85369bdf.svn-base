package com.odi.domain;

import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.DayOfWeek;
import java.util.UUID;


@Entity
@Proxy(lazy = false)
public class FlightInstance {
    @Id
    @Column(name="id")
    private String id = UUID.randomUUID().toString();


    private DayOfWeek day;
    private String time;
    private String flightDuration = null;
    private String flightLandingTime = null;
    private double cost;

    public FlightInstance() {}

    public FlightInstance(DayOfWeek day, String time, String landingTime, double cost) {
        this.day = day;
        this.time = time;
        this.cost = cost;
        this.flightLandingTime = landingTime;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "FlightInstance{" +
                "day=" + day +
                ", time='" + time + '\'' +
                ", cost=" + cost +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(String flightDuration) {
        this.flightDuration = flightDuration;
    }

    public String getFlightLandingTime() {
        return flightLandingTime;
    }

    public void setFlightLandingTime(String flightLandingTime) {
        this.flightLandingTime = flightLandingTime;
    }
}
