package com.odi.service.batch;

import com.odi.model.EntityBase;
import com.odi.service.batch.exceptions.ProcessingException;
import com.odi.service.batch.readers.IReaderFactory;
import com.odi.service.batch.writeres.IBaseWriter;
import com.odi.service.batch.writeres.IWriterFactory;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

/**
 * Created by minus on 3/29/17.
 */
@Service
public class BatchProcessor implements IBatchProcessor {

	private final JobLauncher jobLauncher;
	private final StepBuilderFactory stepBuilderFactory;
	private final JobBuilderFactory jobBuilderFactory;
	private final JobRepository jobRepository;
	private final IWriterFactory writerFactory;
	private final IReaderFactory readerFactory;

	@Autowired
	public BatchProcessor(JobLauncher jobLauncher, JobBuilderFactory jobBuilderFactory,
	                      StepBuilderFactory stepBuilderFactory, JobRepository jobRepository,
	                      IWriterFactory writerFactory, IReaderFactory readerFactory) {
		this.jobLauncher = jobLauncher;

		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.jobRepository = jobRepository;
		this.writerFactory = writerFactory;
		this.readerFactory = readerFactory;
	}

	private String generateCSV(int numRows, int numColumns) {
		char delimiter = '|';
		char newLine = '\n';

		DataFactory df = new DataFactory();
		StringBuilder stringBuffer = new StringBuilder();
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				stringBuffer.append(df.getRandomChars(5)).append(j == numColumns - 1 ? "" : delimiter);
			}

			stringBuffer.append(newLine);
		}

		return stringBuffer.toString();
	}

	@Override
	public JobExecution process(Class<? extends EntityBase> targetClass, Resource resource) throws ProcessingException {
//		ByteArrayResource csvBytesResource = new ByteArrayResource(generateCSV(3000, 3).getBytes());
		Job job = csvJob(targetClass, resource);
		try {
			return jobLauncher.run(job, new JobParameters());
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			throw new ProcessingException();
		}
	}

	private Job csvJob(Class<? extends EntityBase> targetClass, Resource csvBytesResource) {
		return jobBuilderFactory.get(UUID.randomUUID().toString())
				.incrementer(new RunIdIncrementer())
				.listener(new JobExecutionListener() {
					@Override
					public void beforeJob(JobExecution jobExecution) {
						System.out.println("Doing job " + jobExecution.getJobId());
					}

					@Override
					public void afterJob(JobExecution jobExecution) {
						if (jobExecution.getExitStatus().getExitCode().equals(ExitStatus.FAILED.getExitCode())) {
							List<Throwable> allFailureExceptions = jobExecution.getAllFailureExceptions();
							if (! allFailureExceptions.isEmpty()) {
								Throwable throwable = allFailureExceptions.get(0);
								if (throwable instanceof FlatFileParseException) {
									FlatFileParseException parseEx = (FlatFileParseException) throwable;
									String message = parseEx.getCause().getMessage() +
									                 " at line " + String.valueOf(parseEx.getLineNumber()) +
									                 ':' + parseEx.getInput();

									jobExecution.setExitStatus(jobExecution.getExitStatus().addExitDescription(message));
									return;
								}

								jobExecution.getExitStatus()
										.addExitDescription("Error processing the file: " + throwable.getMessage());

								return;
							}
						}

						System.out.println("Done job " + jobExecution.getJobId());
					}
				})
				.start(step(targetClass, csvBytesResource)).build();
	}

	private Step step(Class<? extends EntityBase> targetClass, Resource csvBytesResource) {
		IBaseWriter writer = writerFactory.get(targetClass);
		ItemStreamReader reader = readerFactory.get(targetClass, csvBytesResource);

		ConcurrentTaskExecutor executor = new ConcurrentTaskExecutor();
		executor.setConcurrentExecutor(Executors.newCachedThreadPool());

		return stepBuilderFactory.get("s1")
				.chunk(500)
				.reader(reader)
				.writer(writer)
				.taskExecutor(executor).build();
	}

	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}
}
